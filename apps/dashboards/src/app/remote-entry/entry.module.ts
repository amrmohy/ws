import {NgModule} from '@angular/core';
import {AnalyticsDashboardComponent} from './analytics/analytics.component';
import {ChartsModule} from 'ng2-charts';
import {AgmCoreModule} from '@agm/core';
import {AdminAnalyticsDashboardService} from './analytics/analytics.service';
import {SalesDashboardComponent} from './sales-dashboard/sales-dashboard.component';
import {SalesDashboardService} from './sales-dashboard/sales-dashboard.service';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {PurchasingDashboardService} from './purchasing-dashboard/purchasing-dashboard.service';
import {PurchasingDashboardComponent} from './purchasing-dashboard/purchasing-dashboard.component';
import {TaxsDashboardComponent} from './taxs-dashboard/taxs-dashboard.component';
import {TaxsDashboardService} from './taxs-dashboard/taxs-dashboard.service';
import {TaxDetailsComponent} from './taxs-dashboard/tax-details/tax-details.component';
import {FinanceDashboardComponent} from './finance-dashboard/finance-dashboard.component';
import {FinanceDashboardService} from './finance-dashboard/finance-dashboard.service';
import {RemoteEntryComponent} from "./entry.component";
import {SharedModule} from "@khdma-frontend-nx/shared";
import {EntryRoutingModule} from "./entry-routing.module";
import {
  MultiselectAutocompleteComponent
} from "./components/multiselect-autocomplete/multiselect-autocomplete.component";
import {
  MultiselectAutocompleteBrandComponent
} from "./components/multiselect-autocomplete-brand/multiselect-autocomplete-brand.component";
import { MultiselectAutocompleteSalesPersonComponent } from './components/multiselect-autocomplete-sales-person/multiselect-autocomplete-sales-person.component';
import { MultiselectAutocompleteConsumerComponent } from './components/multiselect-autocomplete-consumer/multiselect-autocomplete-consumer.component';
import { MultiselectAutocompleteSupplierComponent } from './components/multiselect-autocomplete-supplier/multiselect-autocomplete-supplier.component';

@NgModule({
  declarations: [
    RemoteEntryComponent,
    AnalyticsDashboardComponent,
    SalesDashboardComponent,
    MultiselectAutocompleteComponent,
    MultiselectAutocompleteBrandComponent,
    MultiselectAutocompleteSalesPersonComponent,
    MultiselectAutocompleteConsumerComponent,
    PurchasingDashboardComponent,
    MultiselectAutocompleteSupplierComponent,
    TaxsDashboardComponent,
    TaxDetailsComponent,
    FinanceDashboardComponent
  ],
  imports: [
    SharedModule,
    EntryRoutingModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8', }),
    ChartsModule,
    NgxMatSelectSearchModule
  ],
  providers: [AdminAnalyticsDashboardService, SalesDashboardService, PurchasingDashboardService, TaxsDashboardService, FinanceDashboardService],
})
export class RemoteEntryModule {}
