import {BaseService} from '@khdma-frontend-nx/shared';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot,} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class PurchasingDashboardService implements Resolve<any> {
  widgets: any[];
  baseUrl = 'api/v1/dashboard';

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _base: BaseService) {
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([]).then((res) => {
        resolve(res);
      }, reject);
    });
  }



  getCategoriesCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/categories-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getTopConsumersChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-consumers-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(null))
    );
  }

  prepareQuery(params: any) {
    if (params) {
      let paramsQuery = Object.keys(params).map(key => params[key] && params[key] != 'null' ? key + '=' + params[key] : '').filter(item => item != '');
      return paramsQuery.length > 1 ? paramsQuery.join('&') : paramsQuery.join('');
    }
    return '';
  }


  getUserBranches(): Observable<any> {
    return this._base.get(`api/v1/customer-branch/customer`);
  }

  // searchName
  findBrandsByBranchIds(branchIds: any, pageNumber, pageSize, searchName?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let name = searchName ? `&searchName=${searchName}` : '';
    return this._base.get(`api/v1/brand/filter/branchs?${q + name}`);
  }

  // api/v1/item/branchs/filter
  findItemsByBranchIds(branchIds: any, categoryIds, subCategoryIds, pageNumber, pageSize, name?: null): Observable<any> {
    let q = `branchIds=${branchIds}&categoryIds=${categoryIds}&subCategoryIds=${subCategoryIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let search = name ? `&name=${name}` : '';
    return this._base.get(`api/v1/item/filter/branchs-categories-subCategories?${q + search}`);
  }

  // api/v1/category/branchs/filter
  findCategoriesByBranchIds(branchIds: any): Observable<any> {
    let q = `branchIds=${branchIds}`;
    return this._base.get(`api/v1/category/branchs/filter?${q}`);
  }


  findSubCategoriesByIds(branchIds, categoryIds): Observable<any> {
    let q = `branchIds=${branchIds}&categoryIds=${categoryIds}`;
    return this._base.get(`api/v1/subcategory/filter/categories-branchs?${q}`);
  }


  // findSupplier
  findSupplierByBranchIds(branchIds: any, pageNumber, pageSize, name?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let searchName = name ? `&name=${name}` : '';
    return this._base.get(`api/v1/appuser/filter/supplier/branchs?${q + searchName}`);
  }
  getSuppliersCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/suppliers-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getSubcategoriesCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/sub-categories-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getItemsCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/items-count?${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getDraftItemsCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-item-count?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getDraftCategoriesCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-category-count?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getDraftSubcategoriesCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-sub-category-count?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }
}
