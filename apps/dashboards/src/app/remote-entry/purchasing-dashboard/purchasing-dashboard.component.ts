import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UntypedFormBuilder} from '@angular/forms';
import {AuthService, Constant, BaseService, PreviewDownloadPdfDialogComponent} from '@khdma-frontend-nx/shared';
import { chartOption } from '../modal/dashboard/chartOption';
import {chartDataSet} from "../modal/dashboard/chartDataSet";
import { chartDataSetVale } from '../modal/dashboard/datasetValue';
import {chart} from "../modal/dashboard/chart";
import {formatDate} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {PurchasingDashboardService} from './purchasing-dashboard.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'purchasing-dashboard',
  templateUrl: './purchasing-dashboard.component.html',
  styleUrls: ['./purchasing-dashboard.component.scss'],
})
export class PurchasingDashboardComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  constant = new Constant();
  periods = this.constant.PERIODS;
  orderTypes = this.constant.ORDER_TYPES;
  reportTypes = this.constant.PURCHASING_REPORT_TYPES;
  reportPeriodTypes = this.constant.REPORT_PERIOD_TYPES;
  actualExpectedKeys = this.constant.ACTUAL_EXPECTED;
  // REPORTS TYPES
  PURCHASE = this.constant.PURCHASING_REPORT.PURCHASE;
  INVENTORY = this.constant.PURCHASING_REPORT.INVENTORY;
  SUPPLIER_STATEMENT = this.constant.PURCHASING_REPORT.SUPPLIER_STATEMENT;
  SUPPLIER_DUE_PAYMENTS = this.constant.PURCHASING_REPORT.SUPPLIER_DUE_PAYMENTS;
  SUPPLIER_AGING = this.constant.PURCHASING_REPORT.SUPPLIER_AGING;
  config = JSON.parse(localStorage.getItem('CONFIG'));
  currentUser = JSON.parse(localStorage.getItem('CURRENT_USER'));
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  branchId = this.branchConfig.id;
  services = this.config?.services;
  countries = this.config?.countries;
  userTypes = this.config?.userTypes;
  paymentMethods = this.config?.paymentMethods;
  deliveryMethods = this.config?.deliveryMethods;
  years = this.config?.fisicalYears;
  months = this.config?.months;
  widgets: any;

  //----------------------------
  orderPeriod: string = 'WEEK';
  orderType: string;

  draftItemsCount: number;
  draftCategoriesCount: number;
  draftSubcategoriesCount: number;

  suppliersCount: number;
  itemsCount: number;
  categoriesCount: number;
  subCategoriesCount: number;

  topConsumersChart: any = this.prepareChart();

  maxDate = new Date();
  // toggle filter
  toggleFilterBranch: boolean = true;
  toggleFilterPeriodMonth: boolean = false;
  toggleFilterPeriodYear: boolean = false;
  toggleFilterOrderType: boolean = false;
  toggleFilterBrand: boolean = false;
  toggleFilterItem: boolean = false;
  toggleFilterCategory: boolean = false;
  toggleFilterSubCategory: boolean = false;
  toggleFilterSupplier: boolean = false;

  branches: any[] = [];
  brands: any[] = [];
  items: any[] = [];
  categories: any[] = [];
  subCategories: any[] = [];
  suppliers: any[] = [];
  branchIds: any[] = [];
  categoriesIds: any[] = [];
  subCategoriesIds: any[] = [];
  brandsIds: any[] = [];
  itemsIds: any[] = [];
  suppliersIds: any[] = [];

  itemPageNumber = 0;
  itemTotalPages = 0;
  itemLastPage = false;
  isPagePageable = false;
  isPageIsItem = false;
  isPageIsBrand = false;


  reportType = this.constant.PURCHASING_REPORT.PURCHASE;
  periodType = this.constant.REPORT_PERIOD.WEEKLY;
  pdfUrl: any;

  /**
   * Constructor
   *
   * @param {SalesDashboardService} _purchasingDashboardService
   */
  constructor(
    private _purchasingDashboardService: PurchasingDashboardService,
    private authService: AuthService,
    private _router: Router,
    private formBuilder: UntypedFormBuilder,
    private _base: BaseService,
    public _translate: TranslateService,
    private sanitizer: DomSanitizer,
    public bottomSheet: MatBottomSheet,
  ) {
    console.log(this.config);
    // Register the custom chart.js plugin
    this._registerCustomChartJSPlugin();
    this.setConfig();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  setConfig() {
    this.subscriptions.add(
      this.authService.config.subscribe(config => {
        if (config) {
          console.log(config);
          this.services = config.services;
          this.countries = config.countries;
          this.userTypes = config.userTypes;
          this.deliveryMethods = config.deliveryMethods;
          this.paymentMethods = config.paymentMethods;
          this.deliveryMethods = config.deliveryMethods;
          this.years = config.fisicalYears;
          this.months = config?.months;
        }
      })
    );
  }

  ngOnInit(): void {
    console.log(this.config);
    this.load();
  }

  load() {
    if (this.currentUser) {
      if (this.constant.userIsAdmin()) {

        this.getDraftItemsCount();
        this.getDraftCategoriesCount();
        this.getDraftSubcategoriesCount();
        //Top Customers chart
        this.prepareTopConsumerChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
      }
      //User Type is Customer
      else if (this.constant.userIsCustomer() || this.constant.userIsSupervisor()) {
        this.getSuppliersCount();
        this.getCategoriesCount();
        this.getItemsCount();
        this.getSubcategoriesCount();
        //Top 20 Consumers
        this.prepareTopConsumerChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
      }
    }
    this.showFilterBranch();
  }


  prepareTopConsumerChart(params: any) {
    //Top consumers chart
    this._purchasingDashboardService.getTopConsumersChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.find(item => item.elementIndex == tooltipItem[0].xLabel).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        });
        this.topConsumersChart = this.prepareChart(res.map(item => item.elementIndex), res.map(item => item.elementCount), 'To Customers', option);
      }
    });
  }

  prepareChart(labelsArr?: string[], data?: any[], chartTitle?: string, options?: chartOption, type?: string) {
    return new chart(new chartDataSet([new chartDataSetVale(data, chartTitle)], labelsArr), options, type);
  }

  onTopConsumerPeriodChanged(event: any) {
    this.prepareTopConsumerChart({branchId: this.branchId == 0 ? null : this.branchId, period: event.value});
  }


  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Register a custom plugin
   */
  private _registerCustomChartJSPlugin(): void {
    (window as any).Chart.plugins.register({
      afterDatasetsDraw: function(chart, easing): any {
        // Only activate the plugin if it's made available
        // in the options
        if (
          !chart.options.plugins.xLabelsOnTop ||
          (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
        ) {
          return;
        }

        // To only draw at the end of animation, check for easing === 1
        const ctx = chart.ctx;

        chart.data.datasets.forEach(function(dataset, i): any {
          const meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function(element, index): any {

              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
              const fontSize = 13;
              const fontStyle = 'normal';
              const fontFamily = 'Roboto, Helvetica Neue, Arial';
              ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              const dataString = dataset.data[index].toString() + 'k';

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              const padding = 15;
              const startY = 24;
              const position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, startY);

              ctx.save();

              ctx.beginPath();
              ctx.setLineDash([5, 3]);
              ctx.moveTo(position.x, startY + padding);
              ctx.lineTo(position.x, position.y - padding);
              ctx.strokeStyle = 'rgba(255,255,255,0.12)';
              ctx.stroke();

              ctx.restore();
            });
          }
        });
      }
    });
  }


  filterForm = this.formBuilder.group({});


  showFilterPeriodMonth() {
    this.toggleFilterPeriodMonth = true;
    this.filterForm.addControl('month', this.formBuilder.control('', []));
  }

  removeFilterPeriodMonth() {
    this.toggleFilterPeriodMonth = false;
    this.filterForm.removeControl('month');
  }

  showFilterPeriodYear() {
    this.toggleFilterPeriodYear = true;
    this.filterForm.addControl('year', this.formBuilder.control('', []));
  }

  removeFilterPeriodYear() {
    this.toggleFilterPeriodYear = false;
    this.filterForm.removeControl('year');
  }


  convertToBlob(blob) {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = function() {
      const base64data = reader.result;
    };
  }


  // orderType
  showFilterOrderType() {
    this.toggleFilterOrderType = true;
    this.filterForm.addControl('orderType', this.formBuilder.control('', []));
  }

  removeFilterOrderType() {
    this.toggleFilterOrderType = false;
    this.filterForm.removeControl('orderType');
  }

  // baranch
  getUserBranches() {
    this._purchasingDashboardService.getUserBranches().subscribe(res => {
      this.branches = res.data;
    });
  }

  showFilterBranch() {
    this.getUserBranches();
    this.toggleFilterBranch = true;
    this.filterForm.addControl('branch', this.formBuilder.control('', []));
    if (this.currentUser.branchId) {
      this.filterForm.patchValue({
        branch: [this.currentUser.branchId]
      });
    }
  }

  removeFilterBranch() {
    this.toggleFilterBranch = false;
    this.filterForm.removeControl('branch');
    this.branchIds = [];
    // load brand without branchids
    if (this.toggleFilterBrand) {
      this.filterForm.patchValue({
        brand: ''
      });
      this.findBrandsByBranchIds();
    }
    // load category without branchids
    if (this.toggleFilterCategory) {
      this.filterForm.patchValue({
        category: ''
      });
      this.findCategoriesByBranchIds();
    }
    // load subcategory without branchids
    if (this.toggleFilterSubCategory) {
      this.filterForm.patchValue({
        subCategory: ''
      });
      this.findSubCategoriesByIds();
    }
    // load items without branchids
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }

    if (this.toggleFilterSupplier) {
      this.filterForm.patchValue({
        supplier: ''
      });
      this.findSupplierByBranchIds();
    }
  }

  branchChanged(value) {
    this.branchIds = value;
    if (this.toggleFilterBrand) {
      this.findBrandsByBranchIds();
    }
    if (this.toggleFilterCategory) {
      this.findCategoriesByBranchIds();
    }
    if (this.toggleFilterSubCategory) {
      this.findSubCategoriesByIds();
    }
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }

    if (this.toggleFilterSupplier) {
      this.findSupplierByBranchIds();
    }
  }

  // brand
  findBrandsByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._purchasingDashboardService.findBrandsByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.brands = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
        this.isPageIsBrand = true;
      }
    });
  }

  showFilterBrand() {
    this.findBrandsByBranchIds();
    this.toggleFilterBrand = true;
    this.filterForm.addControl('brand', this.formBuilder.control('', []));
  }

  removeFilterBrand() {
    this.toggleFilterBrand = false;
    this.brands = [];
    this.brandsIds = [];
    this.filterForm.removeControl('brand');
  }

  brandChanged(event) {
    this.brandsIds = event.value;
  }

  // category
  findCategoriesByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._purchasingDashboardService.findCategoriesByBranchIds(ids).subscribe(res => {
      if (res.success) {
        this.categories = res.data;
      }
    });
  }

  showFilterCategory() {
    this.findCategoriesByBranchIds();
    this.toggleFilterCategory = true;
    this.filterForm.addControl('category', this.formBuilder.control('', []));
  }

  removeFilterCategory() {
    this.toggleFilterCategory = false;
    this.filterForm.removeControl('category');
    this.categories = [];
    this.categoriesIds = [];
    // load subcategoryout with catids
    if (this.toggleFilterSubCategory) {
      this.filterForm.patchValue({
        subCategory: ''
      });
      this.findSubCategoriesByIds();
    }
    // load item without catids
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }
  }

  categoryChanged(event) {
    this.categoriesIds = event.value;
    // load subcategory
    if (this.toggleFilterSubCategory) {
      this.findSubCategoriesByIds();
    }
    // load items
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
  }

  //subCategories
  findSubCategoriesByIds() {
    let branchIds = this.branchIds.length > 0 ? this.branchIds : [this.currentUser.branchId];
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    this._purchasingDashboardService.findSubCategoriesByIds(branchIds, categoryIds).subscribe(res => {
      if (res.success) {
        this.subCategories = res.data;
      }
    });
  }

  showFilterSubCategory() {
    this.findSubCategoriesByIds();
    this.toggleFilterSubCategory = true;
    this.filterForm.addControl('subCategory', this.formBuilder.control('', []));
  }

  removeFilterSubCategory() {
    this.toggleFilterSubCategory = false;
    this.filterForm.removeControl('subCategory');
    this.subCategories = [];
    this.subCategoriesIds = [];
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }
  }

  subCategoryChanged(event) {
    this.subCategoriesIds = event.value;
    // load items
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
  }

  // item
  findItemsByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    let subCategoriesIds = this.subCategoriesIds.length > 0 ? this.subCategoriesIds : [];
    this._purchasingDashboardService.findItemsByBranchIds(ids, categoryIds, subCategoriesIds, 0, 10).subscribe(res => {
      if (res.success) {
        this.items = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
        this.isPageIsItem = true;
      }
    });
  }

  showFilterItem() {
    this.findItemsByBranchIds();
    this.toggleFilterItem = true;
    this.filterForm.addControl('item', this.formBuilder.control('', []));
  }

  removeFilterItem() {
    this.toggleFilterItem = false;
    this.filterForm.removeControl('item');
    this.items = [];
    this.itemsIds = [];
  }

  itemChanged(event) {
    this.itemsIds = event.value;
  }


  // supplier
  findSupplierByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._purchasingDashboardService.findSupplierByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.suppliers = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
      }
    });
  }

  showFilterSupplier() {
    this.findSupplierByBranchIds();
    this.toggleFilterSupplier = true;
    this.filterForm.addControl('supplier', this.formBuilder.control('', []));
  }

  removeFilterSupplier() {
    this.toggleFilterSupplier = false;
    this.suppliers = [];
    this.suppliersIds = [];
    this.filterForm.removeControl('supplier');
  }

  supplierChanged(event) {
    this.suppliersIds = event.value;
  }


  // report
  getReportType(value) {
    this.reportType = value;
    this.inventoryReport();
    this.supplierReports();
  }

  getReportPeriodType(value) {
    this.periodType = value;
  }

  inventoryReport() {
    if (this.reportType === this.INVENTORY) {
      if (this.toggleFilterPeriodMonth) {
        this.removeFilterPeriodMonth();
      }
      if (this.toggleFilterPeriodYear) {
        this.removeFilterPeriodYear();
      }
      if (this.toggleFilterSupplier) {
        this.removeFilterSupplier();
      }
    }
  }

  supplierReports() {
    if (this.reportType === this.SUPPLIER_DUE_PAYMENTS || this.reportType === this.SUPPLIER_AGING){
      if (this.toggleFilterBranch === false) {
        this.showFilterBranch();
      }
    }
    if (this.reportType === this.SUPPLIER_STATEMENT || this.reportType === this.SUPPLIER_DUE_PAYMENTS || this.reportType === this.SUPPLIER_AGING){
      if (this.toggleFilterPeriodMonth) {
        this.removeFilterPeriodMonth();
      }
      if (this.toggleFilterPeriodYear) {
        this.removeFilterPeriodYear();
      }
      if (this.toggleFilterBrand) {
        this.removeFilterBrand();
      }
      if (this.toggleFilterItem) {
        this.removeFilterItem();
      }
      if (this.toggleFilterCategory) {
        this.removeFilterCategory();
      }
      if (this.toggleFilterSubCategory) {
        this.removeFilterSubCategory();
      }
      if (this.toggleFilterOrderType) {
        this.removeFilterOrderType();
      }
      if (this.toggleFilterSupplier === false){
        this.showFilterSupplier();
      }
    }
  }


  selectedReportType(value: string) {
    return this.reportType === value;
  }

  selectedReportPeriodType(value: string) {
    return this.periodType === value;
  }

  preview(formValue) {
    let reportObj = {
      branchIds: this.branchIds.length > 0 ? this.branchIds : [this.currentUser.branchId],
      brandIds: this.brandsIds.length > 0 ? this.brandsIds : null,
      categoryIds: this.categoriesIds.length > 0 ? this.categoriesIds : null,
      createdMonths: formValue?.month ? formValue?.month : null,
      createdYears: formValue?.year ? formValue?.year : null,
      orderTypeId: formValue?.orderType ? formValue?.orderType?.toString() : '',
      itemIds: this.itemsIds.length > 0 ? this.itemsIds : null,
      subCategoryIds: this.subCategoriesIds.length > 0 ? this.subCategoriesIds : null,
      supplierIds: this.suppliersIds.length > 0 ? this.suppliersIds : null,
      Period: this.periodType ? this.periodType : null,
      currentDate: formatDate(new Date(), this.constant.DATE_TIME_FORMAT, 'en')
    };

    let reportDetailObj = {
      reportName: this.reportType + '_' + this._translate.currentLang,
      fileName: this.reportType + '_' + this._translate.currentLang,
      paramters: this.clean(reportObj),
    };


    if (this.reportType === this.SUPPLIER_STATEMENT){
      delete reportDetailObj.paramters.Period;
    }

    if (this.reportType === this.INVENTORY || this.reportType === this.PURCHASE){
      delete reportDetailObj.paramters.Period;
    }

    this.printReport(reportDetailObj, this.reportType);
  }

  printReport(reportObj, reportType) {
    this._base.downLoad(reportObj, `api/v1/report/pdf`)
      .subscribe(res => {
        const blob = new Blob([res], {type: 'application/pdf'});
        this.previewDownloadPdf(blob, `${reportType}.pdf`);
      });
  }

  previewDownloadPdf(blobData, filename) {
    const dialogRef = this.bottomSheet.open(PreviewDownloadPdfDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {blob: blobData, filename: `${this.reportType}.pdf`}
    });
    dialogRef.afterDismissed().subscribe((result) => {

    });
  }

  getDraftItemsCount() {
    this._purchasingDashboardService
      .getDraftItemsCount(null)
      .subscribe(res => this.draftItemsCount = res);
  }

  getDraftCategoriesCount() {
    this._purchasingDashboardService
      .getDraftCategoriesCount(null)
      .subscribe(res => this.draftCategoriesCount = res);
  }

  getDraftSubcategoriesCount() {
    this._purchasingDashboardService
      .getDraftSubcategoriesCount(null)
      .subscribe(res => this.draftSubcategoriesCount = res);
  }

  navigateToDraftCategories() {
    this._router.navigate(['pages/inventory/category'],
      {
        queryParams: {
          name: null,
          serviceCategoryId: null,
          statusCode: 1
        },
      });
  }

  navigateToDraftItems() {
    this._router.navigate(['pages/inventory/item'],
      {
        queryParams: {
          statusCode: 1
        }
      });
  }

  getSuppliersCount() {
    this._purchasingDashboardService.getSuppliersCount(this.branchId).subscribe(res => this.suppliersCount = res);
  }

  getItemsCount() {
    this._purchasingDashboardService.getItemsCount(this.branchId).subscribe(res => this.itemsCount = res);
  }

  getCategoriesCount() {
    this._purchasingDashboardService.getCategoriesCount(this.branchId).subscribe(res => this.categoriesCount = res);
  }

  getSubcategoriesCount() {
    this._purchasingDashboardService.getSubcategoriesCount(this.branchId).subscribe(res => this.subCategoriesCount = res);
  }

  clean(obj) {
    for (let propName in obj) {
      if (obj[propName] === null || obj[propName] === '') {
        delete obj[propName];
      }
    }
    return obj;
  }
}
