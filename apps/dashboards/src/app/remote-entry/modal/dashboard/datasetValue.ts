export class chartDataSetVale {
    label: string;
    backgroundColor: string = '#e44891';
    fillColor: string = '#e44891';
    strokeColor: string = "#e44891";
    borderColor: string[] = [
        "#e44891"
    ];
    borderWidth: number = 2;
    data: any[] = [];
    fill: boolean = false;

    constructor(
        data: any[],
        label?: string,
        backgroundColor?: string,
        borderColors?: string[],
        borderWidth?: number,
        fill?: boolean,
        fillColor?: string) {
        if (data) this.data = data;
        if (label) this.label = label;
        if (backgroundColor) this.backgroundColor = backgroundColor;
        if (borderColors) this.borderColor = borderColors;
        if (borderWidth) this.borderWidth = borderWidth;
        if (fill) this.fill = fill;
        if (fillColor) this.fillColor = fillColor;
    }
}