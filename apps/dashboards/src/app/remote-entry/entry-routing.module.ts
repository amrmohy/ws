import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AnalyticsDashboardComponent} from './analytics/analytics.component';
import {AdminAnalyticsDashboardService} from './analytics/analytics.service';
import {SalesDashboardService} from './sales-dashboard/sales-dashboard.service';
import {SalesDashboardComponent} from './sales-dashboard/sales-dashboard.component';
import {PurchasingDashboardService} from './purchasing-dashboard/purchasing-dashboard.service';
import {PurchasingDashboardComponent} from './purchasing-dashboard/purchasing-dashboard.component';
import {TaxsDashboardService} from './taxs-dashboard/taxs-dashboard.service';
import {TaxsDashboardComponent} from './taxs-dashboard/taxs-dashboard.component';
import {TaxDetailsComponent} from './taxs-dashboard/tax-details/tax-details.component';
import {FinanceDashboardComponent} from './finance-dashboard/finance-dashboard.component';
import {FinanceDashboardService} from './finance-dashboard/finance-dashboard.service';
import {RemoteEntryComponent} from "./entry.component";
import {AuthGuard} from "@khdma-frontend-nx/shared";

const routes: Routes = [
  {
    path: '',
    component: RemoteEntryComponent,
    canActivate: [AuthGuard],

    children: [
      {
        path: 'analytics',
        component: AnalyticsDashboardComponent,
        resolve: {
          data: AdminAnalyticsDashboardService,
        },
      },
      {
        path: 'sales',
        component: SalesDashboardComponent,
        resolve: {
          data: SalesDashboardService,
        },
      },
      {
        path: 'purchasing',
        component: PurchasingDashboardComponent,
        resolve: {
          data: PurchasingDashboardService,
        },
      },
      {
        path:'finance',
        component:FinanceDashboardComponent,
        resolve:{
          data:FinanceDashboardService
        }
      },
      {
        path: 'taxs',
        component: TaxsDashboardComponent,
        resolve: {
          data: TaxsDashboardService,
        },
      },
      {
        path: 'taxs/details/:id',
        component: TaxDetailsComponent,
      },
      {
        path: '',
        redirectTo: 'analytics',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntryRoutingModule {
}
