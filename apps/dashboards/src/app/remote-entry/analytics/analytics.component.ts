import {Component, OnDestroy, OnInit} from '@angular/core';
import {AdminAnalyticsDashboardService} from './analytics.service';
import {Router} from '@angular/router';
// import {ChartOfAccountsService} from '../../finance/services/chart-of-accounts.service';
import {Subscription} from 'rxjs';
import {AuthService, Constant, UpdateBranchService } from '@khdma-frontend-nx/shared';
import { chartOption } from '../modal/dashboard/chartOption';
import {chartDataSet} from "../modal/dashboard/chartDataSet";
import { chartDataSetVale } from '../modal/dashboard/datasetValue';
import {chart} from "../modal/dashboard/chart";


@Component({
  selector: 'khdma-frontend-nx-dashboards-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss'],
})
export class AnalyticsDashboardComponent implements OnInit, OnDestroy {
  constant = new Constant();
  periods = this.constant.PERIODS;
  orderTypes = this.constant.ORDER_TYPES;
  actualExpectedKeys = this.constant.ACTUAL_EXPECTED;
  config = JSON.parse(localStorage.getItem('CONFIG'));
  currentUser = JSON.parse(localStorage.getItem('CURRENT_USER'));
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  branchId = this.branchConfig.id;
  services = this.config?.services;
  countries = this.config?.countries;
  userTypes = this.config?.userTypes;
  paymentMethods = this.config?.paymentMethods;
  deliveryMethods = this.config?.deliveryMethods;
  widgets: any;
  orderPeriod: string = 'WEEK';
  orderType: string;
  orderPaymentMethodId: number;
  orderDeliveryMethodId: number;
  approvedOrdersCountPeriod: string = 'WEEK';
  approvedOrdersCountOrderType: string;
  customerCountServiceId: number;
  customerSalesCountryId: number;
  customerSalesPeriod: string = 'WEEK';
  customerCountPeriod: string = 'WEEK';
  employeesCount: number;
  draftOrdersCount: number;
  draftItemsCount: number;
  draftItemRequestCount: number;
  draftCategoriesCount: number;
  draftCategoriesRequestCount: number;
  draftSubcategoriesCount: number;
  draftSubcategoriesRequestCount: number;
  draftBrandsCount: number;
  draftBrandRequestCount: number;
  approvedOrdersCount: number;
  branchsCount: number;
  suppliersCount: number;
  itemsCount: number;
  categoriesCount: number;
  subCategoriesCount: number;
  customersCount: number;
  consumersCount: number;
  usersCount: number;
  accountManagersCount: number;
  topCustomersChart: any = this.prepareChart();
  topConsumersChart: any = this.prepareChart();
  topSalesItemsChart: any = this.prepareChart();
  topCustomerSalesChart: any = this.prepareChart();
  ordersChart: any = this.prepareChart();
  branchRevenueChart: any = this.prepareChart();
  branchProfitChart: any = this.prepareChart();
  branchCostChart: any = this.prepareChart();
  approvedRevenueSum: number;
  approvedRevenuePeriod: string = 'WEEK';
  revenueTypeChange: string = 'EXPECTED';
  branchRevenueChartPeriod: string = 'WEEK';
  branchRevenueChartTypeChange: string = 'EXPECTED';
  approvedProfitSum: number;
  approvedProfitPeriod: string = 'WEEK';
  profitTypeChange: string = 'EXPECTED';
  branchProfitChartPeriod: string = 'WEEK';
  branchProfitChartTypeChange: string = 'EXPECTED';
  approvedCostSum: number;
  approvedCostPeriod: string = 'WEEK';

  draftAccountsCount: any;
  private subscriptions = new Subscription();

  /**
   * Constructor
   *
   * @param {AdminAnalyticsDashboardService} _AdminAnalyticsDashboardService
   */
  constructor(
    private _adminAnalyticsDashboardService: AdminAnalyticsDashboardService,
    private authService: AuthService,
    private _router: Router,
    private _updateBranchService: UpdateBranchService,
    // private _chartOfAccountsService: ChartOfAccountsService
  ) {

    this._registerCustomChartJSPlugin();
    authService.config.subscribe(config => {
      if (config) {
        this.services = config.services;
        this.countries = config.countries;
        this.userTypes = config.userTypes;
        this.deliveryMethods = config.deliveryMethods;
        this.paymentMethods = config.paymentMethods;
      }
    });

  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this._updateBranchService.subject.subscribe((branch) => {
      if (branch.hasOwnProperty('id')) {
        this.branchId = branch.id;
        this.load();
      }else {
        this.load();
      }
    }));
  }

  load() {
    if (this.currentUser) {
      if (this.constant.userIsAdmin()) {
        this.getExpectedRevenueSum(this.periods[0]?.value);
        this.getExpectedProfitSum(this.periods[0]?.value);
        this.getCostOfGoods(this.periods[0]?.value);
        this._adminAnalyticsDashboardService.getUsersCount(null).subscribe(res => this.usersCount = res);
        this._adminAnalyticsDashboardService.getCsutomersCount(null).subscribe(res => this.customersCount = res);
        this._adminAnalyticsDashboardService.getApprovedOrdersCount(null).subscribe(res => this.approvedOrdersCount = res);
        this._adminAnalyticsDashboardService.getDraftOrdersCount(null).subscribe(res => this.draftOrdersCount = res);
        this._adminAnalyticsDashboardService.getAccountManagersCount(null).subscribe(res => this.accountManagersCount = res);
        //Top Customers chart
        this.prepareTopCustomerChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
        //Top customer sales
        this.prepareTopCustomerSalesChart({period: 'WEEK', countryId: null});
        //orders
        this.prepareOrdersChart({period: 'WEEK'});

        // draft
        this.getDraftItemsCount();
        this.getDraftAccountsCount();
        // this.getDraftItemRequestCount();
        this.getDraftCategoriesCount();
        this.getDraftSubcategoriesCount();
        this.getDraftBrandsCount();
      }
      //User Type is Customer
      else if (this.constant.userIsCustomer() || this.constant.userIsSupervisor()) {
        this.getExpectedRevenueSum(this.periods[0]?.value);
        this.getExpectedProfitSum(this.periods[0]?.value);
        this.getCostOfGoods(this.periods[0]?.value);
        this.getExpectedRevenueChart(this.periods[0]?.value);
        this.getExpectedProfitChart(this.periods[0]?.value);
        this.getCostOfGoodsChart(this.periods[0]?.value);
        this._adminAnalyticsDashboardService.getApprovedOrdersCount({branchId: null, period: this.periods[0]?.value, orderType: this.orderTypes[0]?.value}).subscribe(res => this.approvedOrdersCount = res);
        this._adminAnalyticsDashboardService.getBranchesCount(this.branchId).subscribe(res => this.branchsCount = res);
        this._adminAnalyticsDashboardService.getSuppliersCount(this.branchId).subscribe(res => this.suppliersCount = res);
        this._adminAnalyticsDashboardService.getItemsCount(this.branchId).subscribe(res => this.itemsCount = res);
        this._adminAnalyticsDashboardService.getCategoriesCount(this.branchId).subscribe(res => this.categoriesCount = res);
        this._adminAnalyticsDashboardService.getSubcategoriesCount(this.branchId).subscribe(res => this.subCategoriesCount = res);
        this._adminAnalyticsDashboardService.getEmployeesCount(this.branchId).subscribe(res => this.employeesCount = res);
        this._adminAnalyticsDashboardService.getConsumersCount(this.branchId).subscribe(res => this.consumersCount = res);
        //start charts ------------------------------------------------------------------------------------------------------------------
        //Top 20 Consumers
        this.prepareTopConsumerChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
        //Top Sales Items chart
        this.prepareTopSalesItemsChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
        //orders
        this.prepareOrdersChart({period: 'WEEK'});
      } else if (this.constant.userIsAccManager()) {
        this._adminAnalyticsDashboardService.getCsutomersCountAM(null).subscribe(res => this.customersCount = res);
        this._adminAnalyticsDashboardService.getApprovedOrdersCountAM(null).subscribe(res => this.approvedOrdersCount = res);
        //orders
        this.prepareOrdersChartAM({period: 'WEEK'});
      } else if (this.constant.userIsBackOffice()) {
        // draft
        this.getDraftItemsCount();

        this.getDraftCategoriesCount();
        this.getDraftSubcategoriesCount();
        this.getDraftBrandsCount();
      }
    }
  }

  prepareTopCustomerChart(params: any) {
    this._adminAnalyticsDashboardService.getTopCustomersChart(params).subscribe(
      res => {
        if (res) {
          let option = new chartOption(null, null, null, {
            callbacks: {
              title: function(tooltipItem, data) {
                return res.find(item => item.elementIndex == tooltipItem[0].xLabel).elementString;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          });
          this.topCustomersChart = this.prepareChart(res.map(item => item.elementIndex), res.map(item => item.elementCount), 'To Customers', option);
        }
      }
    );
  }

  prepareTopSalesItemsChart(params: any) {
    this._adminAnalyticsDashboardService.getTopSalesItemsChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.find(item => item.elementIndex == tooltipItem[0].xLabel).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        });
        this.topSalesItemsChart = this.prepareChart(res.map(item => item.elementIndex), res.map(item => item.elementCount), 'Top Sales Items Chart', option);
      }
    });
  }

  prepareTopConsumerChart(params: any) {
    //Top consumers chart
    this._adminAnalyticsDashboardService.getTopConsumersChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.find(item => item.elementIndex == tooltipItem[0].xLabel).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        });
        this.topConsumersChart = this.prepareChart(res.map(item => item.elementIndex), res.map(item => item.elementCount), 'To Customers', option);
      }
    });
  }

  prepareTopCustomerSalesChart(params: any) {
    this._adminAnalyticsDashboardService.getCustomerSalesChart(params).subscribe(res => {
      let option = new chartOption(null, null, null,
        {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.find(item => item.elementString.includes(tooltipItem[0].xLabel)).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        }, {
          xAxes: [{
            ticks: {
              fontColor: '#FFF'
            },
            gridLines: {
              display: false
            }
          }],
          yAxes: [{
            gridLines: {
              display: false
            }, ticks: {
              beginAtZero: true,
              fontColor: '#FFF'
            }
          }]
        }
      );
      this.topCustomerSalesChart = this.prepareChart(res.map(item => item.elementString.split('-')[1]),
        res.map(item => item.elementCount), 'Top Customer Sales', option, 'line');
    });
  }

  prepareOrdersChart(params: any) {
    //Orders
    this._adminAnalyticsDashboardService.getOrders(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          {
            callbacks: {
              title: function(tooltipItem, data) {
                return tooltipItem[0].xLabel;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          },
          {
            xAxes: [{
              ticks: {
                fontColor: '#FFF'
              },
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }, ticks: {
                beginAtZero: true,
                fontColor: '#FFF'
              }
            }]
          }
        );
        this.ordersChart = this.prepareChart(res.map(item => item.elementDate), res.map(item => item.elementCount), 'Orders', option, 'line');
      }
    });
  }

  prepareOrdersChartAM(params: any) {
    //Orders
    this._adminAnalyticsDashboardService.getOrdersAM(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          {
            callbacks: {
              title: function(tooltipItem, data) {
                return tooltipItem[0].xLabel;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          },
          {
            xAxes: [{
              ticks: {
                fontColor: '#FFF'
              },
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }, ticks: {
                beginAtZero: true,
                fontColor: '#FFF'
              }
            }]
          }
        );
        this.ordersChart = this.prepareChart(res.map(item => item.elementDate), res.map(item => item.elementCount), 'Orders', option, 'line');
      }
    });
  }


  prepareChart(labelsArr?: string[], data?: any[], chartTitle?: string, options?: chartOption, type?: string) {
    return new chart(new chartDataSet([new chartDataSetVale(data, chartTitle)], labelsArr), options, type);
  }

  customerCountServiceChange(serviceId: number) {
    this.customerCountServiceId = serviceId;
    let params = {branchId: null, period: this.customerCountPeriod, serviceId: serviceId};
    this._adminAnalyticsDashboardService.getCustomersCount(params).subscribe(res => this.customersCount = res);
  }

  customerCountServiceChangeAM(serviceId: number) {
    this.customerCountServiceId = serviceId;
    let params = {branchId: null, period: this.customerCountPeriod, serviceId: serviceId};
    this._adminAnalyticsDashboardService.getCsutomersCountAM(params).subscribe(res => this.customersCount = res);
  }

  customerCountPeriodChange(period: any) {
    this.customerCountPeriod = period;
    let params = {branchId: null, period: period, serviceId: this.customerCountServiceId};
    this._adminAnalyticsDashboardService.getCustomersCount(params).subscribe(res => this.customersCount = res);
  }

  customerCountPeriodChangeAM(period: any) {
    this.customerCountPeriod = period;
    let params = {branchId: null, period: period, serviceId: this.customerCountServiceId};
    this._adminAnalyticsDashboardService.getCsutomersCountAM(params).subscribe(res => this.customersCount = res);
  }

  draftOrdersCountPeriodChange(period: any) {
    let params = {branchId: null, period: period, orderType: null};
    this._adminAnalyticsDashboardService.getDraftOrdersCount(params).subscribe(res => this.draftOrdersCount = res);
  }

  approvedOrdersCountPeriodChange(period: any) {
    this.approvedOrdersCountPeriod = period;
    let params = {branchId: null, period: period, orderType: this.approvedOrdersCountOrderType};
    this._adminAnalyticsDashboardService.getApprovedOrdersCount(params).subscribe(res => this.approvedOrdersCount = res);
  }

  approvedOrdersCountPeriodChangeAM(period: any) {
    this.approvedOrdersCountPeriod = period;
    let params = {branchId: null, period: period, orderType: this.approvedOrdersCountOrderType};
    this._adminAnalyticsDashboardService.getApprovedOrdersCountAM(params).subscribe(res => this.approvedOrdersCount = res);
  }

  approvedOrdersCountTypeChange(orderType: string) {
    this.approvedOrdersCountOrderType = orderType;
    let params = {branchId: null, period: this.approvedOrdersCountPeriod, orderType: orderType};
    this._adminAnalyticsDashboardService.getApprovedOrdersCount(params)
      .subscribe(res => this.approvedOrdersCount = res);
  }

  approvedOrdersCountTypeChangeAM(orderType: string) {
    this.approvedOrdersCountOrderType = orderType;
    let params = {branchId: null, period: this.approvedOrdersCountPeriod, orderType: orderType};
    this._adminAnalyticsDashboardService.getApprovedOrdersCountAM(params).subscribe(res => this.approvedOrdersCount = res);
  }


  accountManagersCountCountryChange(countryId: any) {
    this._adminAnalyticsDashboardService.getAccountManagersCount(countryId && countryId != 'null' ? countryId : null).subscribe(res =>
      this.accountManagersCount = res
    );
  }

  onUserTypesChange(userType: number) {
    this._adminAnalyticsDashboardService.getUsersCount(userType + '' != 'null' ? userType : null).subscribe(res => this.usersCount = res);
  }

  onTopConsumerPeriodChanged(event: any) {
    this.prepareTopConsumerChart({branchId: this.branchId == 0 ? null : this.branchId, period: event.value});
  }

  onTopCustomerPeriodChanged(event: any) {
    this.prepareTopCustomerChart({branchId: this.branchId == 0 ? null : this.branchId, period: event.value});
  }

  onTopSalesItemChanged(event: any) {
    this.prepareTopSalesItemsChart({branchId: this.branchId == 0 ? null : this.branchId, period: event.value});
  }

  onTopCustomerSalesChanged(event: any) {
    this.customerSalesPeriod = event.value;
    this.prepareTopCustomerSalesChart({countryId: this.customerSalesCountryId, period: event.value});
  }

  orderTypeChange(orderType: string) {
    this.orderType = orderType;
    let params = {branchId: null, period: this.orderPeriod, orderType: orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChart(params);
  }

  orderPaymentMethodChange(paymentMethodId: number) {
    this.orderPaymentMethodId = paymentMethodId;
    let params = {branchId: null, period: this.orderPeriod, orderType: this.orderType, paymentMethod: paymentMethodId, deliveryMethod: this.orderDeliveryMethodId};
    this.prepareOrdersChart(params);
  }

  orderDeliveryMethodChange(deliveryMethodId: number) {
    this.orderDeliveryMethodId = deliveryMethodId;
    let params = {branchId: null, period: this.orderPeriod, orderType: this.orderType, paymentMethod: this.orderPaymentMethodId, deliveryMethod: deliveryMethodId};
    this.prepareOrdersChart(params);
  }

  ordersPeriodChange(period: any) {
    this.orderPeriod = period;
    let params = {branchId: null, period: period, orderType: this.orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChart(params);
  }

  ordersPeriodChangeAM(period: any) {
    this.orderPeriod = period;
    let params = {branchId: null, period: period, orderType: this.orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChartAM(params);
  }

  topCustomerSalesCountryChange(countryId: any) {
    this.customerSalesCountryId = countryId;
    this.prepareTopCustomerSalesChart({countryId: countryId, period: this.customerSalesPeriod});
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Register a custom plugin
   */
  private _registerCustomChartJSPlugin(): void {
    (window as any).Chart.plugins.register({
      afterDatasetsDraw: function(chart, easing): any {
        // Only activate the plugin if it's made available
        // in the options
        if (
          !chart.options.plugins.xLabelsOnTop ||
          (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
        ) {
          return;
        }

        // To only draw at the end of animation, check for easing === 1
        const ctx = chart.ctx;

        chart.data.datasets.forEach(function(dataset, i): any {
          const meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function(element, index): any {

              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
              const fontSize = 13;
              const fontStyle = 'normal';
              const fontFamily = 'Roboto, Helvetica Neue, Arial';
              ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              const dataString = dataset.data[index].toString() + 'k';

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              const padding = 15;
              const startY = 24;
              const position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, startY);

              ctx.save();

              ctx.beginPath();
              ctx.setLineDash([5, 3]);
              ctx.moveTo(position.x, startY + padding);
              ctx.lineTo(position.x, position.y - padding);
              ctx.strokeStyle = 'rgba(255,255,255,0.12)';
              ctx.stroke();

              ctx.restore();
            });
          }
        });
      }
    });
  }

  // Revenue
  getExpectedRevenueSum(period) {
    this._adminAnalyticsDashboardService.getExpectedRevenueSum({period: period})
      .subscribe(res => {
        this.approvedRevenueSum = res;
      });
  }

  getActualRevenueSum(period) {
    this._adminAnalyticsDashboardService.getActualRevenueSum({period: period})
      .subscribe(res => this.approvedRevenueSum = res);
  }

  approvedRevenueCountTypeChange(orderType: string) {
    this.revenueTypeChange = orderType;
    let period = this.approvedRevenuePeriod;
    if (orderType === 'EXPECTED') {
      this.getExpectedRevenueSum(period);
    } else {
      this.getActualRevenueSum(period);
    }
  }

  approvedRevenueCountPeriodChange(period: any) {
    this.approvedRevenuePeriod = period;

    if (this.revenueTypeChange === 'EXPECTED') {
      this.getExpectedRevenueSum(period);
    } else {
      this.getActualRevenueSum(period);
    }
  }

  // Revenue chart
  revenueBranchChartTypeChange(type: string) {
    this.branchRevenueChartTypeChange = type;
    let period = this.branchRevenueChartPeriod;
    if (type === 'EXPECTED') {
      this.getExpectedRevenueChart(period);
    } else {
      this.getActualRevenueChart(period);
    }
  }

  branchRevenueChartPeriodChange(period: any) {
    this.branchRevenueChartPeriod = period;
    if (this.branchRevenueChartTypeChange === 'EXPECTED') {
      this.getExpectedRevenueChart(period);
    } else {
      this.getActualRevenueChart(period);
    }

  }

  getExpectedRevenueChart(period) {
    this._adminAnalyticsDashboardService.getExpectedRevenueChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          null, null);
        this.branchRevenueChart = this.prepareChart(res.map(item => item.elementString),
          res.map(item => item.elementSum),
          'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  getActualRevenueChart(period) {
    this._adminAnalyticsDashboardService.getActualRevenueChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          null, null);
        this.branchRevenueChart = this.prepareChart(res.map(item => item.elementString),
          res.map(item => item.elementSum),
          'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  //profit
  approvedProfitCountTypeChange(orderType: string) {
    this.profitTypeChange = orderType;
    let period = this.approvedProfitPeriod;
    if (orderType === 'EXPECTED') {
      this.getExpectedProfitSum(period);
    } else {
      this.getActualProfitSum(period);
    }
  }

  approvedProfitCountPeriodChange(period: any) {
    this.approvedProfitPeriod = period;
    if (this.profitTypeChange === 'EXPECTED') {
      this.getExpectedProfitSum(period);
    } else {
      this.getActualProfitSum(period);
    }
  }

  getExpectedProfitSum(period) {
    this._adminAnalyticsDashboardService.getExpectedProfitSum({period: period})
      .subscribe(res => this.approvedProfitSum = res);
  }

  getActualProfitSum(period) {
    this._adminAnalyticsDashboardService.getActualProfitSum({period: period})
      .subscribe(res => this.approvedProfitSum = res);
  }

  //branch profit chart
  profitBranchChartTypeChange(type: string) {
    this.branchProfitChartTypeChange = type;
    let period = this.branchProfitChartPeriod;
    if (type === 'EXPECTED') {
      this.getExpectedProfitChart(period);
    } else {
      this.getActualProfitChart(period);
    }
  }

  branchProfitChartPeriodChange(period: any) {
    this.branchProfitChartPeriod = period;
    if (this.branchProfitChartTypeChange === 'EXPECTED') {
      this.getExpectedProfitChart(period);
    } else {
      this.getActualProfitChart(period);
    }
  }

  getExpectedProfitChart(period) {
    this._adminAnalyticsDashboardService.getExpectedProfitChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, null, null);
        this.branchProfitChart = this.prepareChart(res.map(item => item.elementString), res.map(item => item.elementSum), 'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  getActualProfitChart(period) {
    this._adminAnalyticsDashboardService.getActualProfitChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, null, null);
        this.branchProfitChart = this.prepareChart(res.map(item => item.elementString),
          res.map(item => item.elementSum), 'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  // cost of goods
  getCostOfGoods(period) {
    this._adminAnalyticsDashboardService.getCostOfGoods({period: period})
      .subscribe(res => this.approvedCostSum = res);
  }

  approvedCostOfGoodsPeriodChange(period: any) {
    this.approvedCostPeriod = period;
    this.getCostOfGoods(period);
  }

  // branch cost chart

  branchCostPeriodChange(period: any) {
    this.getCostOfGoodsChart(period);
  }

  getCostOfGoodsChart(period) {
    this._adminAnalyticsDashboardService
      .getCostOfGoodsChart({period: period})
      .subscribe(res => {
        if (res) {
          let option = new chartOption(null, null, null, null, null);
          this.branchCostChart = this.prepareChart(res.map(item => item.elementString),
            res.map(item => item.elementSum), 'branchRevenue',
            option, 'doughnut');
        }
      });
  }

  getDraftItemsCount() {
    this._adminAnalyticsDashboardService
      .getDraftItemsCount(null)
      .subscribe(res => {
        this.draftItemsCount = res.elementCount;
        this.draftItemRequestCount = res.elementRequestCount;
      });
  }

  getDraftAccountsCount() {
    // this._chartOfAccountsService.getDraftAccountsCount().subscribe((result) => {
    //   this.draftAccountsCount = result.data;
    // });
  }

  getDraftCategoriesCount() {
    this._adminAnalyticsDashboardService
      .getDraftCategoriesCount(null)
      .subscribe(res => {
        this.draftCategoriesCount = res.elementCount;
        this.draftCategoriesRequestCount = res.elementRequestCount;
      });
  }

  getDraftBrandsCount() {
    this._adminAnalyticsDashboardService
      .getDraftBrandsCount(null)
      .subscribe(res => {
        this.draftBrandsCount = res.elementCount;
        this.draftBrandRequestCount = res.elementRequestCount;
      });
  }

  getDraftSubcategoriesCount() {
    this._adminAnalyticsDashboardService
      .getDraftSubcategoriesCount(null)
      .subscribe(res => {
        this.draftSubcategoriesCount = res.elementCount;
        this.draftSubcategoriesRequestCount = res.elementRequestCount;
      });
  }

  navigateToDraftCategories() {
    this._router.navigate(['pages/inventory/category'],
      {
        queryParams: {
          name: null,
          serviceCategoryId: null,
          statusCode: 1
        },
      });
  }

  navigateToDraftCategoriesRequest() {
    this._router.navigate(['pages/inventory/category'],
      {
        queryParams: {
          name: null,
          serviceCategoryId: null,
          categoryRequestStatus: 1
        },
      });
  }

  navigateToDraftItems() {
    this._router.navigate(['pages/inventory/item'],
      {
        queryParams: {
          statusCode: 1
        }
      });
  }

  navigateToDraftItemsRequest() {
    this._router.navigate(['pages/inventory/item'],
      {
        queryParams: {
          itemRequestStatus: 1
        }
      });
  }

  navigateToDraftBrands() {
    this._router.navigate(['pages/inventory/brand'],
      {
        queryParams: {
          statusCode: 1
        }
      });
  }

  navigateToDraftAccounts() {
    this._router.navigate(['pages/finance/chart-of-account']);
  }

  navigateToDraftBrandsRequest() {
    this._router.navigate(['pages/inventory/brand'],
      {
        queryParams: {
          brandRequestStatus: 1
        }
      });
  }
}

