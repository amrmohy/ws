import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot,} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {BaseService} from "@khdma-frontend-nx/shared";

@Injectable()
export class AdminAnalyticsDashboardService implements Resolve<any> {
  widgets: any[];
  baseUrl = 'api/v1/dashboard';

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _base: BaseService) {
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([]).then((res) => {
        resolve(res);
      }, reject);
    });
  }


  getEmployeesCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/employees-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getConsumersCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/consumers-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getCsutomersCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/customers-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getBranchesCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/branches-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getSuppliersCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/suppliers-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getCategoriesCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/categories-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getSubcategoriesCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/sub-categories-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getItemsCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/items-count?${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getTopConsumersChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-consumers-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(null))
    );
  }

  getTopCustomersChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-customers-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(null))
    );
  }

  getTopSalesItemsChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-sales-items-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(null))
    );
  }

  getCustomersCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/customers-count?${this.prepareQuery(params)}`)
      .pipe(
        map(res => res.elementCount),
        catchError(error => of(null))
      );
  }

  getCsutomersCountAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/customers-count-am?${this.prepareQuery(params)}`)
      .pipe(
        map(res => res.elementCount),
        catchError(error => of(null))
      );
  }

  getUsersCount(userType: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/users-count?${userType ? 'userType=' + userType : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getAccountManagersCount(countryId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/account-managers-count${countryId ? '?countryId=' + countryId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getOrders(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/orders-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getOrdersAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/orders-chart-am?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getApprovedOrdersCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/approved-orders-count?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getActualRevenueSum(params) {
    return this._base.get(`${this.baseUrl}/actual-revenue?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getExpectedRevenueSum(params) {
    return this._base.get(`${this.baseUrl}/expected-revenue?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getActualRevenueChart(params) {
    return this._base.get(`${this.baseUrl}/actual-revenue-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getExpectedRevenueChart(params) {
    return this._base.get(`${this.baseUrl}/expected-revenue-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

// profit
  getActualProfitSum(params) {
    return this._base.get(`${this.baseUrl}/actual-profit?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getExpectedProfitSum(params) {
    return this._base.get(`${this.baseUrl}/expected-profit?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getActualProfitChart(params) {
    return this._base.get(`${this.baseUrl}/actual-profit-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getExpectedProfitChart(params) {
    return this._base.get(`${this.baseUrl}/expected-profit-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }


  // cost of goods
  getCostOfGoods(params) {
    return this._base.get(`${this.baseUrl}/cost-of-goods?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getCostOfGoodsChart(params) {
    return this._base.get(`${this.baseUrl}/cost-of-goods-chart?${this.prepareQuery(params)}`)
      .pipe(
        catchError(error => of(0))
      );
  }

  getApprovedOrdersCountAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/approved-orders-count-am?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getDraftOrdersCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-orders-count?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getCustomerSalesChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-customer-sales-chart?${this.prepareQuery(params)}`)
      .pipe(
        catchError(error => of(0))
      );
  }

  prepareQuery(params: any) {
    if (params) {
      let paramsQuery = Object.keys(params).map(key => params[key] && params[key] != 'null' ? key + '=' + params[key] : '').filter(item => item != '');
      return paramsQuery.length > 1 ? paramsQuery.join('&') : paramsQuery.join('');
    }
    return '';
  }

  getDraftItemsCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-item-count?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getDraftCategoriesCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-category-count?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getDraftBrandsCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-brand-count?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }


  getDraftSubcategoriesCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/draft-sub-category-count?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }
}
