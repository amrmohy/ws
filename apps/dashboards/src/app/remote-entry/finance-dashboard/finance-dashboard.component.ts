import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UntypedFormBuilder} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {FinanceDashboardService} from './finance-dashboard.service';
import {Subscription} from 'rxjs';
import {formatDate} from "@angular/common";
import {AuthService, Constant, BaseService, PreviewDownloadPdfDialogComponent} from '@khdma-frontend-nx/shared';
import { chartOption } from '../modal/dashboard/chartOption';
import {chartDataSet} from "../modal/dashboard/chartDataSet";
import { chartDataSetVale } from '../modal/dashboard/datasetValue';
import {chart} from "../modal/dashboard/chart";
@Component({
  selector: 'finance-dashboard',
  templateUrl: './finance-dashboard.component.html',
  styleUrls: ['./finance-dashboard.component.scss'],
})
export class FinanceDashboardComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  constant = new Constant();
  periods = this.constant.PERIODS;
  orderTypes = this.constant.ORDER_TYPES;
  reportTypes = this.constant.FINANCE_REPORT_TYPES;
  actualExpectedKeys = this.constant.ACTUAL_EXPECTED;
  REVENUE = this.constant.FINANCE_REPORT.REVENUE;
  EXPENSES = this.constant.FINANCE_REPORT.EXPENSES;
  TRIAL_BALANCE = this.constant.FINANCE_REPORT.TRIAL_BALANCE;
  INCOME_STATEMNT = this.constant.FINANCE_REPORT.INCOME_STATEMNT;
  BALANCE_SHEET = this.constant.FINANCE_REPORT.BALANCE_SHEET;
  config = JSON.parse(localStorage.getItem('CONFIG'));
  currentUser = JSON.parse(localStorage.getItem('CURRENT_USER'));
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  branchId = this.branchConfig.id;
  services = this.config?.services;
  countries = this.config?.countries;
  userTypes = this.config?.userTypes;
  paymentMethods = this.config?.paymentMethods;
  deliveryMethods = this.config?.deliveryMethods;
  years = this.config?.fisicalYears;
  months = this.config?.months;
  accountTypes = this.config?.accountTypes;
  widgets: any;

  orderPeriod: string = 'WEEK';
  orderType: string;
  orderPaymentMethodId: number;
  orderDeliveryMethodId: number;
  approvedOrdersCount: number;


  ordersChart: any = this.prepareChart();
  maxDate = new Date();
  // toggle filter
  toggleFilterBranch: boolean = true;
  toggleFilterPeriodMonth: boolean = false;
  toggleFilterPeriodYear: boolean = false;
  toggleFilterPaymentMethod: boolean = false;
  toggleFilterOrderType: boolean = false;
  toggleFilterBrand: boolean = false;
  toggleFilterItem: boolean = false;
  toggleFilterCategory: boolean = false;
  toggleFilterSubCategory: boolean = false;
  toggleFilterSalesPerson: boolean = false;
  toggleFilterConsumer: boolean = false;
  toggleFilterAccountTypes: boolean = false;

  branches: any[] = [];
  brands: any[] = [];
  items: any[] = [];
  categories: any[] = [];
  subCategories: any[] = [];
  salesPersons: any[] = [];
  consumers: any[] = [];
  branchIds: any[] = [];
  categoriesIds: any[] = [];
  subCategoriesIds: any[] = [];
  brandsIds: any[] = [];
  itemsIds: any[] = [];
  consumersIds: any[] = [];
  salesPersonsIds: any[] = [];
  orderStatues: any[] = [];


  // paginated .. items
  itemPageNumber = 0;
  itemTotalPages = 0;
  itemLastPage = false;
  isPagePageable = false;
  isPageIsItem = false;

  isPageIsBrand = false;

  // report-type
  reportType = this.constant.FINANCE_REPORT.EXPENSES;


  // revenue
  approvedRevenueSum: number;
  approvedRevenuePeriod: string = 'WEEK';
  revenueTypeChange: string = 'EXPECTED';

  // revenue chart
  branchRevenueChartPeriod: string = 'WEEK';
  branchRevenueChartTypeChange: string = 'EXPECTED';
  // profit
  approvedProfitSum: number;
  approvedProfitPeriod: string = 'WEEK';
  profitTypeChange: string = 'EXPECTED';
  // profit chart
  branchProfitChartPeriod: string = 'WEEK';
  branchProfitChartTypeChange: string = 'EXPECTED';
  // cost
  approvedCostSum: number;
  approvedCostPeriod: string = 'WEEK';
  // chart
  branchRevenueChart: any = this.prepareChart();
  branchProfitChart: any = this.prepareChart();
  branchCostChart: any = this.prepareChart();

  /**
   * Constructor
   *
   * @param {FinanceDashboardService} _financeDashboardService
   */
  constructor(
    private _financeDashboardService: FinanceDashboardService,
    private authService: AuthService,
    private _router: Router,
    private formBuilder: UntypedFormBuilder,
    private _base: BaseService,
    public _translate: TranslateService,
    public bottomSheet: MatBottomSheet,
  ) {
    // Register the custom chart.js plugin
    this._registerCustomChartJSPlugin();
    this.setConfig();
  }

  setConfig() {
    this.subscriptions.add(this.authService.config.subscribe(config => {
      if (config) {
        this.services = config.services;
        this.countries = config.countries;
        this.userTypes = config.userTypes;
        this.deliveryMethods = config.deliveryMethods;
        this.paymentMethods = config.paymentMethods;
        this.years = config.fisicalYears;
        this.months = config.months;
        this.accountTypes = config.accountTypes;
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.load();
  }

  load() {
    if (this.currentUser) {
      if (this.constant.userIsAdmin()) {
        this._financeDashboardService.getApprovedOrdersCount(null).subscribe(res => this.approvedOrdersCount = res);

        //orders
        this.prepareOrdersChart({period: 'WEEK'});
      }
      //User Type is Customer
      else if (this.constant.userIsCustomer() || this.constant.userIsSupervisor()) {
        this._financeDashboardService.getApprovedOrdersCount({branchId: null, period: this.periods[0]?.value, orderType: this.orderTypes[0]?.value}).subscribe(res => this.approvedOrdersCount = res);
        this.prepareOrdersChart({period: 'WEEK'});
      } else if (this.constant.userIsAccManager()) {
        this._financeDashboardService.getApprovedOrdersCountAM(null).subscribe(res => this.approvedOrdersCount = res);
        //orders
        this.prepareOrdersChartAM({period: 'WEEK'});
      }
    }
    this.showFilterBranch();
  }


  prepareOrdersChart(params: any) {
    //Orders
    this._financeDashboardService.getOrders(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          {
            callbacks: {
              title: function(tooltipItem, data) {
                return tooltipItem[0].xLabel;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          },
          {
            xAxes: [{
              ticks: {
                fontColor: '#FFF'
              },
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }, ticks: {
                beginAtZero: true,
                fontColor: '#FFF'
              }
            }]
          }
        );
        this.ordersChart = this.prepareChart(res.map(item => item.elementDate), res.map(item => item.elementCount), 'Orders', option, 'line');
      }
    });
  }

  prepareOrdersChartAM(params: any) {
    //Orders
    this._financeDashboardService.getOrdersAM(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          {
            callbacks: {
              title: function(tooltipItem, data) {
                return tooltipItem[0].xLabel;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          },
          {
            xAxes: [{
              ticks: {
                fontColor: '#FFF'
              },
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }, ticks: {
                beginAtZero: true,
                fontColor: '#FFF'
              }
            }]
          }
        );
        this.ordersChart = this.prepareChart(res.map(item => item.elementDate), res.map(item => item.elementCount), 'Orders', option, 'line');
      }
    });
  }


  prepareChart(labelsArr?: string[], data?: any[], chartTitle?: string, options?: chartOption, type?: string) {
    return new chart(new chartDataSet([new chartDataSetVale(data, chartTitle)], labelsArr), options, type);
  }

  orderTypeChange(orderType: string) {
    this.orderType = orderType;
    let params = {branchId: null, period: this.orderPeriod, orderType: orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChart(params);
  }

  orderPaymentMethodChange(paymentMethodId: number) {
    this.orderPaymentMethodId = paymentMethodId;
    let params = {branchId: null, period: this.orderPeriod, orderType: this.orderType, paymentMethod: paymentMethodId, deliveryMethod: this.orderDeliveryMethodId};
    this.prepareOrdersChart(params);
  }

  orderDeliveryMethodChange(deliveryMethodId: number) {
    this.orderDeliveryMethodId = deliveryMethodId;
    let params = {branchId: null, period: this.orderPeriod, orderType: this.orderType, paymentMethod: this.orderPaymentMethodId, deliveryMethod: deliveryMethodId};
    this.prepareOrdersChart(params);
  }

  ordersPeriodChange(period: any) {
    this.orderPeriod = period;
    let params = {branchId: null, period: period, orderType: this.orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChart(params);
  }


  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Register a custom plugin
   */
  private _registerCustomChartJSPlugin(): void {
    (window as any).Chart.plugins.register({
      afterDatasetsDraw: function(chart, easing): any {
        // Only activate the plugin if it's made available
        // in the options
        if (
          !chart.options.plugins.xLabelsOnTop ||
          (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
        ) {
          return;
        }

        // To only draw at the end of animation, check for easing === 1
        const ctx = chart.ctx;

        chart.data.datasets.forEach(function(dataset, i): any {
          const meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function(element, index): any {

              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
              const fontSize = 13;
              const fontStyle = 'normal';
              const fontFamily = 'Roboto, Helvetica Neue, Arial';
              ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              const dataString = dataset.data[index].toString() + 'k';

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              const padding = 15;
              const startY = 24;
              const position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, startY);

              ctx.save();

              ctx.beginPath();
              ctx.setLineDash([5, 3]);
              ctx.moveTo(position.x, startY + padding);
              ctx.lineTo(position.x, position.y - padding);
              ctx.strokeStyle = 'rgba(255,255,255,0.12)';
              ctx.stroke();

              ctx.restore();
            });
          }
        });
      }
    });
  }


  filterForm = this.formBuilder.group({});


  showFilterAccountTypes() {
    this.toggleFilterAccountTypes = true;
    this.filterForm.addControl('accountTypeIds', this.formBuilder.control('', []));
  }

  removeFilterAccountTypes() {
    this.toggleFilterAccountTypes = false;
    this.filterForm.removeControl('accountTypeIds');
  }

  showFilterPeriodMonth() {
    this.toggleFilterPeriodMonth = true;
    this.filterForm.addControl('month', this.formBuilder.control('', []));
  }

  removeFilterPeriodMonth() {
    this.toggleFilterPeriodMonth = false;
    this.filterForm.removeControl('month');
  }

  showFilterPeriodYear() {
    this.toggleFilterPeriodYear = true;
    this.filterForm.addControl('year', this.formBuilder.control('', []));
  }

  removeFilterPeriodYear() {
    this.toggleFilterPeriodYear = false;
    this.filterForm.removeControl('year');
  }


  showFilterPaymentMethod() {
    this.toggleFilterPaymentMethod = true;
    this.filterForm.addControl('paymentMethod', this.formBuilder.control('', []));
  }

  removeFilterPaymentMethod() {
    this.toggleFilterPaymentMethod = false;
    this.filterForm.removeControl('paymentMethod');
  }


  // orderType
  showFilterOrderType() {
    this.toggleFilterOrderType = true;
    this.filterForm.addControl('orderType', this.formBuilder.control('', []));

  }

  removeFilterOrderType() {
    this.toggleFilterOrderType = false;
    this.filterForm.removeControl('orderType');
  }

  // baranch
  getUserBranches() {
    this._financeDashboardService.getUserBranches().subscribe(res => {
      this.branches = res.data;
    });
  }

  showFilterBranch() {
    this.getUserBranches();
    this.toggleFilterBranch = true;
    this.filterForm.addControl('branch', this.formBuilder.control('', []));
    if (this.currentUser.branchId) {
      this.filterForm.patchValue({
        branch: [this.currentUser.branchId]
      });
    }
  }

  removeFilterBranch() {
    this.toggleFilterBranch = false;
    this.filterForm.removeControl('branch');
    this.branchIds = [];

    // load brand without branchids
    if (this.toggleFilterBrand) {
      this.filterForm.patchValue({
        brand: ''
      });
      this.findBrandsByBranchIds();
    }
    // load category without branchids
    if (this.toggleFilterCategory) {
      this.filterForm.patchValue({
        category: ''
      });
      this.findCategoriesByBranchIds();
    }
    // load subcategory without branchids
    if (this.toggleFilterSubCategory) {
      this.filterForm.patchValue({
        subCategory: ''
      });
      this.findSubCategoriesByIds();
    }
    // load items without branchids
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }

    if (this.toggleFilterSalesPerson) {
      this.filterForm.patchValue({
        salesPerson: ''
      });
      this.findSalesPersonByBranchIds();
    }
    if (this.toggleFilterConsumer) {
      this.filterForm.patchValue({
        consumer: ''
      });
      this.findConsumerByBranchIds();
    }
  }

  branchChanged(value) {
    this.branchIds = value;

    if (this.toggleFilterBrand) {
      this.findBrandsByBranchIds();
    }
    if (this.toggleFilterCategory) {
      this.findCategoriesByBranchIds();
    }
    if (this.toggleFilterSubCategory) {
      this.findSubCategoriesByIds();
    }
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
    if (this.toggleFilterSalesPerson) {
      this.findSalesPersonByBranchIds();
    }
    if (this.toggleFilterConsumer) {
      this.findConsumerByBranchIds();
    }
  }

  // brand
  findBrandsByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._financeDashboardService.findBrandsByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.brands = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
        this.isPageIsBrand = true;
      }
    });
  }

  showFilterBrand() {
    this.findBrandsByBranchIds();
    this.toggleFilterBrand = true;
    this.filterForm.addControl('brand', this.formBuilder.control('', []));
  }

  removeFilterBrand() {
    this.toggleFilterBrand = false;
    this.brands = [];
    this.brandsIds = [];
    this.filterForm.removeControl('brand');
  }

  brandChanged(event) {
    this.brandsIds = event.value;
  }

  // category
  findCategoriesByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._financeDashboardService.findCategoriesByBranchIds(ids).subscribe(res => {
      if (res.success) {
        this.categories = res.data;
      }
    });
  }

  showFilterCategory() {
    this.findCategoriesByBranchIds();
    this.toggleFilterCategory = true;
    this.filterForm.addControl('category', this.formBuilder.control('', []));
  }

  removeFilterCategory() {
    this.toggleFilterCategory = false;
    this.filterForm.removeControl('category');
    this.categories = [];
    this.categoriesIds = [];
    // load subcategoryout with catids
    if (this.toggleFilterSubCategory) {
      this.filterForm.patchValue({
        subCategory: ''
      });
      this.findSubCategoriesByIds();
    }
    // load item without catids
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }
  }

  categoryChanged(event) {
    this.categoriesIds = event.value;
    // load subcategory
    if (this.toggleFilterSubCategory) {
      this.findSubCategoriesByIds();
    }
    // load items
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
  }

  //subCategories
  findSubCategoriesByIds() {
    let branchIds = this.branchIds.length > 0 ? this.branchIds : [this.currentUser.branchId];
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    this._financeDashboardService.findSubCategoriesByIds(branchIds, categoryIds).subscribe(res => {
      if (res.success) {
        this.subCategories = res.data;
      }
    });
  }

  showFilterSubCategory() {
    this.findSubCategoriesByIds();
    this.toggleFilterSubCategory = true;
    this.filterForm.addControl('subCategory', this.formBuilder.control('', []));
  }

  removeFilterSubCategory() {
    this.toggleFilterSubCategory = false;
    this.filterForm.removeControl('subCategory');
    this.subCategories = [];
    this.subCategoriesIds = [];
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }
  }

  subCategoryChanged(event) {
    this.subCategoriesIds = event.value;
    // load items
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
  }

  // item
  findItemsByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    let subCategoriesIds = this.subCategoriesIds.length > 0 ? this.subCategoriesIds : [];
    this._financeDashboardService.findItemsByBranchIds(ids, categoryIds, subCategoriesIds, 0, 10).subscribe(res => {
      if (res.success) {
        this.items = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
        this.isPageIsItem = true;
      }
    });
  }

  showFilterItem() {
    this.findItemsByBranchIds();
    this.toggleFilterItem = true;
    this.filterForm.addControl('item', this.formBuilder.control('', []));
  }

  removeFilterItem() {
    this.toggleFilterItem = false;
    this.filterForm.removeControl('item');
    this.items = [];
    this.itemsIds = [];
  }

  itemChanged(event) {
    this.itemsIds = event.value;

  }

  // sales person
  findSalesPersonByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._financeDashboardService.findSalesPersonsByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.salesPersons = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
      }
    });
  }

  showFilterSalesPerson() {
    this.findSalesPersonByBranchIds();
    this.toggleFilterSalesPerson = true;
    this.filterForm.addControl('salesPerson', this.formBuilder.control('', []));
  }

  removeFilterSalesPerson() {
    this.toggleFilterSalesPerson = false;
    this.salesPersons = [];
    this.salesPersonsIds = [];
    this.filterForm.removeControl('salesPerson');
  }

  salesPersonChanged(event) {
    this.salesPersonsIds = event.value;
  }

  // CONSUMER
  findConsumerByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._financeDashboardService.findConsumerByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.consumers = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
      }
    });
  }

  showFilterConsumer() {
    this.findConsumerByBranchIds();
    this.toggleFilterConsumer = true;
    this.filterForm.addControl('consumer', this.formBuilder.control('', []));
  }

  removeFilterConsumer() {
    this.toggleFilterConsumer = false;
    this.consumers = [];
    this.consumersIds = [];
    this.filterForm.removeControl('consumer');
  }

  consumerChanged(event) {
    this.consumersIds = event.value;
  }

  // report
  getReportType(value) {
    this.reportType = value;
    if (this.reportType === this.EXPENSES || this.reportType === this.REVENUE || this.reportType === this.INCOME_STATEMNT || this.reportType === this.BALANCE_SHEET) {
      if (this.toggleFilterAccountTypes) {
        this.removeFilterAccountTypes();
      }
    }
    if (this.reportType === this.EXPENSES || this.reportType === this.TRIAL_BALANCE || this.reportType === this.INCOME_STATEMNT || this.reportType === this.BALANCE_SHEET) {
      if (this.toggleFilterPaymentMethod) {
        this.removeFilterPaymentMethod();
      }
      if (this.toggleFilterOrderType) {
        this.removeFilterOrderType();
      }
      if (this.toggleFilterConsumer) {
        this.removeFilterConsumer();
      }
      if (this.toggleFilterSalesPerson) {
        this.removeFilterSalesPerson();
      }
    }
    if (this.reportType === this.TRIAL_BALANCE || this.reportType === this.INCOME_STATEMNT || this.reportType === this.BALANCE_SHEET) {
      if (this.toggleFilterPeriodMonth) {
        this.removeFilterPeriodMonth();
      }
      if (this.toggleFilterPeriodYear) {
        this.removeFilterPeriodYear();
      }
      if (this.toggleFilterBrand) {
        this.removeFilterBrand();
      }

      if (this.toggleFilterCategory) {
        this.removeFilterCategory();
      }
      if (this.toggleFilterSubCategory) {
        this.removeFilterSubCategory();
      }
      if (this.toggleFilterItem) {
        this.removeFilterItem();
      }

    }
    if (this.reportType === this.TRIAL_BALANCE) {
      if (this.toggleFilterAccountTypes === false) {
        this.showFilterAccountTypes();
      }
    }
  }

  selectedReportType(value: string) {
    return this.reportType === value;
  }

  preview(formValue) {
    let initalParams = {
      branchIds: this.branchIds.length > 0 ? this.branchIds : [this.currentUser.branchId],
      brandIds: this.brandsIds.length > 0 ? this.brandsIds : null,
      categoryIds: this.categoriesIds.length > 0 ? this.categoriesIds : null,
      consumerIds: this.consumersIds.length > 0 ? this.consumersIds : null,
      createdMonths: formValue?.month ? formValue?.month : null,
      createdYears: formValue?.year ? formValue?.year : null,
      orderTypeId: formValue?.orderType ? formValue?.orderType?.toString() : '',
      paymentMethodIds: formValue?.paymentMethod ? formValue?.paymentMethod : null,
      itemIds: this.itemsIds.length > 0 ? this.itemsIds : null,
      subCategoryIds: this.subCategoriesIds.length > 0 ? this.subCategoriesIds : null,
      salesPersonIds: this.salesPersonsIds.length > 0 ? this.salesPersonsIds : null,
      currentDate: formatDate(new Date(), this.constant.DATE_TIME_FORMAT, 'en')
    };
    let paramters = this.clean(initalParams);
    let reportDetailObj = {
      reportName: this.reportType + '_' + this._translate.currentLang,
      fileName: this.reportType + '_' + this._translate.currentLang,
      paramters: paramters,
    };
    this.printReport(reportDetailObj, this.reportType);
  }

  // print report
  printReport(reportObj, reportType) {
    this._base.downLoad(reportObj, `api/v1/report/pdf`)
      .subscribe(res => {
        const blob = new Blob([res], {type: 'application/pdf'});
        this.previewDownloadPdf(blob, `${reportType}.pdf`);
      });
  }

  previewDownloadPdf(blobData, filename) {
    const dialogRef = this.bottomSheet.open(PreviewDownloadPdfDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {blob: blobData, filename: filename}
    });
    dialogRef.afterDismissed().subscribe((result) => {

    });
  }

  clean(obj) {
    for (let propName in obj) {
      if (obj[propName] === null || obj[propName] === '') {
        delete obj[propName];
      }
    }
    return obj;
  }

  // Revenue
  approvedRevenueCountTypeChange(orderType: string) {
    this.revenueTypeChange = orderType;
    let period = this.approvedRevenuePeriod;
    if (orderType === 'EXPECTED') {
      this.getExpectedRevenueSum(period);
    } else {
      this.getActualRevenueSum(period);
    }
  }

  // Revenue
  getExpectedRevenueSum(period) {
    this._financeDashboardService.getExpectedRevenueSum({period: period})
      .subscribe(res => {
        this.approvedRevenueSum = res;
      });
  }

  getActualRevenueSum(period) {
    this._financeDashboardService.getActualRevenueSum({period: period})
      .subscribe(res => this.approvedRevenueSum = res);
  }

  approvedRevenueCountPeriodChange(period: any) {
    this.approvedRevenuePeriod = period;

    if (this.revenueTypeChange === 'EXPECTED') {
      this.getExpectedRevenueSum(period);
    } else {
      this.getActualRevenueSum(period);
    }
  }

  // cost of goods
  getCostOfGoods(period) {
    this._financeDashboardService.getCostOfGoods({period: period})
      .subscribe(res => this.approvedCostSum = res);
  }

  approvedCostOfGoodsPeriodChange(period: any) {
    this.approvedCostPeriod = period;
    this.getCostOfGoods(period);
  }

  //profit
  approvedProfitCountTypeChange(orderType: string) {
    this.profitTypeChange = orderType;
    let period = this.approvedProfitPeriod;
    if (orderType === 'EXPECTED') {
      this.getExpectedProfitSum(period);
    } else {
      this.getActualProfitSum(period);
    }
  }

  approvedProfitCountPeriodChange(period: any) {
    this.approvedProfitPeriod = period;
    if (this.profitTypeChange === 'EXPECTED') {
      this.getExpectedProfitSum(period);
    } else {
      this.getActualProfitSum(period);
    }
  }

  getExpectedProfitSum(period) {
    this._financeDashboardService.getExpectedProfitSum({period: period})
      .subscribe(res => this.approvedProfitSum = res);
  }

  getActualProfitSum(period) {
    this._financeDashboardService.getActualProfitSum({period: period})
      .subscribe(res => this.approvedProfitSum = res);
  }

  // Revenue chart
  revenueBranchChartTypeChange(type: string) {
    this.branchRevenueChartTypeChange = type;
    let period = this.branchRevenueChartPeriod;
    if (type === 'EXPECTED') {
      this.getExpectedRevenueChart(period);
    } else {
      this.getActualRevenueChart(period);
    }
  }

  branchRevenueChartPeriodChange(period: any) {
    this.branchRevenueChartPeriod = period;
    if (this.branchRevenueChartTypeChange === 'EXPECTED') {
      this.getExpectedRevenueChart(period);
    } else {
      this.getActualRevenueChart(period);
    }

  }

  getExpectedRevenueChart(period) {
    this._financeDashboardService.getExpectedRevenueChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          null, null);
        this.branchRevenueChart = this.prepareChart(res.map(item => item.elementString),
          res.map(item => item.elementSum),
          'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  getActualRevenueChart(period) {
    this._financeDashboardService.getActualRevenueChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          null, null);
        this.branchRevenueChart = this.prepareChart(res.map(item => item.elementString),
          res.map(item => item.elementSum),
          'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  //branch profit chart
  profitBranchChartTypeChange(type: string) {
    this.branchProfitChartTypeChange = type;
    let period = this.branchProfitChartPeriod;
    if (type === 'EXPECTED') {
      this.getExpectedProfitChart(period);
    } else {
      this.getActualProfitChart(period);
    }
  }

  branchProfitChartPeriodChange(period: any) {
    this.branchProfitChartPeriod = period;
    if (this.branchProfitChartTypeChange === 'EXPECTED') {
      this.getExpectedProfitChart(period);
    } else {
      this.getActualProfitChart(period);
    }
  }

  getExpectedProfitChart(period) {
    this._financeDashboardService.getExpectedProfitChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, null, null);
        this.branchProfitChart = this.prepareChart(res.map(item => item.elementString), res.map(item => item.elementSum), 'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  getActualProfitChart(period) {
    this._financeDashboardService.getActualProfitChart({period: period}).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, null, null);
        this.branchProfitChart = this.prepareChart(res.map(item => item.elementString),
          res.map(item => item.elementSum), 'branchRevenue',
          option, 'doughnut');
      }
    });
  }

  // branch cost chart
  branchCostPeriodChange(period: any) {
    this.getCostOfGoodsChart(period);
  }

  getCostOfGoodsChart(period) {
    this._financeDashboardService
      .getCostOfGoodsChart({period: period})
      .subscribe(res => {
        if (res) {
          let option = new chartOption(null, null, null, null, null);
          this.branchCostChart = this.prepareChart(res.map(item => item.elementString),
            res.map(item => item.elementSum), 'branchRevenue',
            option, 'doughnut');
        }
      });
  }
}
