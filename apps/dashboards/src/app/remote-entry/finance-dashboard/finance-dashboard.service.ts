import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot,} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { BaseService } from '@khdma-frontend-nx/shared';

@Injectable()
export class FinanceDashboardService implements Resolve<any> {
  widgets: any[];
  baseUrl = 'api/v1/dashboard';

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _base: BaseService) {
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([]).then((res) => {
        resolve(res);
      }, reject);
    });
  }











  getOrders(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/orders-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getOrdersAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/orders-chart-am?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getApprovedOrdersCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/approved-orders-count?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getApprovedOrdersCountAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/approved-orders-count-am?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }




  prepareQuery(params: any) {
    if (params) {
      let paramsQuery = Object.keys(params).map(key => params[key] && params[key] != 'null' ? key + '=' + params[key] : '').filter(item => item != '');
      return paramsQuery.length > 1 ? paramsQuery.join('&') : paramsQuery.join('');
    }
    return '';
  }


  getUserBranches(): Observable<any> {
    return this._base.get(`api/v1/customer-branch/customer`);
  }

  // searchName
  findBrandsByBranchIds(branchIds: any, pageNumber, pageSize, searchName?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let name = searchName ? `&searchName=${searchName}` : '';
    return this._base.get(`api/v1/brand/filter/branchs?${q + name}`);
  }

  // api/v1/item/branchs/filter
  findItemsByBranchIds(branchIds: any, categoryIds, subCategoryIds, pageNumber, pageSize, name?: null): Observable<any> {
    let q = `branchIds=${branchIds}&categoryIds=${categoryIds}&subCategoryIds=${subCategoryIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let search = name ? `&name=${name}` : '';
    return this._base.get(`api/v1/item/filter/branchs-categories-subCategories?${q + search}`);
  }

  // api/v1/category/branchs/filter
  findCategoriesByBranchIds(branchIds: any): Observable<any> {
    let q = `branchIds=${branchIds}`;
    return this._base.get(`api/v1/category/branchs/filter?${q}`);
  }



  findSubCategoriesByIds(branchIds, categoryIds): Observable<any> {
    let q = `branchIds=${branchIds}&categoryIds=${categoryIds}`;
    return this._base.get(`api/v1/subcategory/filter/categories-branchs?${q}`);
  }

  //consumer
  findConsumerByBranchIds(branchIds: any, pageNumber, pageSize, searchName?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let name = searchName ? `&searchName=${searchName}` : '';
    return this._base.get(`api/v1/appuser/filter/consumer/branchs?${q + name}`);
  }

  //sales-person
  findSalesPersonsByBranchIds(branchIds: any, pageNumber, pageSize, searchName?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let name = searchName ? `&searchName=${searchName}` : '';
    return this._base.get(`api/v1/appuser/filter/sales-person/branchs?${q + name}`);
  }

  getActualRevenueSum(params) {
    return this._base.get(`${this.baseUrl}/actual-revenue?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getExpectedRevenueSum(params) {
    return this._base.get(`${this.baseUrl}/expected-revenue?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }


  getActualProfitSum(params) {
    return this._base.get(`${this.baseUrl}/actual-profit?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getExpectedProfitSum(params) {
    return this._base.get(`${this.baseUrl}/expected-profit?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getActualRevenueChart(params) {
    return this._base.get(`${this.baseUrl}/actual-revenue-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getExpectedRevenueChart(params) {
    return this._base.get(`${this.baseUrl}/expected-revenue-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }
  getActualProfitChart(params) {
    return this._base.get(`${this.baseUrl}/actual-profit-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getExpectedProfitChart(params) {
    return this._base.get(`${this.baseUrl}/expected-profit-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  // cost of goods
  getCostOfGoods(params) {
    return this._base.get(`${this.baseUrl}/cost-of-goods?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementSum),
      catchError(error => of(0))
    );
  }

  getCostOfGoodsChart(params) {
    return this._base.get(`${this.baseUrl}/cost-of-goods-chart?${this.prepareQuery(params)}`)
      .pipe(
        catchError(error => of(0))
      );
  }
}
