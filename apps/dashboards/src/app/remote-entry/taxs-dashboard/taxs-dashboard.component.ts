import {AuthService, Constant, BaseService} from '@khdma-frontend-nx/shared';
import { chartOption } from '../modal/dashboard/chartOption';
import {chartDataSet} from "../modal/dashboard/chartDataSet";
import { chartDataSetVale } from '../modal/dashboard/datasetValue';
import {chart} from "../modal/dashboard/chart";
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UntypedFormBuilder} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {TaxsDashboardService} from './taxs-dashboard.service';

@Component({
  selector: 'taxs-dashboard',
  templateUrl: './taxs-dashboard.component.html',
  styleUrls: ['./taxs-dashboard.component.scss'],
})
export class TaxsDashboardComponent implements OnInit {
  constant = new Constant();
  config = JSON.parse(localStorage.getItem('CONFIG'));
  currentUser = JSON.parse(localStorage.getItem('CURRENT_USER'));
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  branchId = this.branchConfig.id;
  years = this.config?.fisicalYears;
  months = this.config?.months;
  quarters = this.config?.quarters;
  //----------------------------
  purchasesSalesDifferenceChart: any = this.prepareChart();
  purchasesTaxsChart: any = this.prepareChart();
  salesTaxsChart: any = this.prepareChart();
  purchasingAndSalesTaxsChart: any = this.prepareChart();
  branches: any[] = [];
  branchIds: any[] = [];
  yearIds: any[] = [];
  monthIds: any[] = [];
  quarterIds: any[] = [];
  differenceInTaxValue: number;
  totalSalesTaxValue: number;
  totalPurchasesTaxValue: number;

  /**
   * Constructor
   *
   * @param {TaxsDashboardService} _taxsDashboardService
   */
  constructor(
    private _taxsDashboardService: TaxsDashboardService,
    private authService: AuthService,
    private _router: Router,
    private formBuilder: UntypedFormBuilder,
    private _base: BaseService,
    public _translate: TranslateService,
  ) {
    this.getUserBranches();
    // Register the custom chart.js plugin
    this._registerCustomChartJSPlugin();
  }
  ngOnInit(): void {
    this.load();
  }

  load() {
    if (this.currentUser) {
      this.apply();
    }
  }

  prepareChart(labelsArr?: string[], data?: any[], chartTitle?: string, options?: chartOption, type?: string) {
    return new chart(new chartDataSet([new chartDataSetVale(data, chartTitle)], labelsArr), options, type);
  }

  private _registerCustomChartJSPlugin(): void {
    (window as any).Chart.plugins.register({
      afterDatasetsDraw: function(chart, easing): any {
        // Only activate the plugin if it's made available
        // in the options
        if (
          !chart.options.plugins.xLabelsOnTop ||
          (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
        ) {
          return;
        }

        // To only draw at the end of animation, check for easing === 1
        const ctx = chart.ctx;

        chart.data.datasets.forEach(function(dataset, i): any {
          const meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function(element, index): any {

              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
              const fontSize = 13;
              const fontStyle = 'normal';
              const fontFamily = 'Roboto, Helvetica Neue, Arial';
              ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              const dataString = dataset.data[index].toString() + 'k';

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              const padding = 15;
              const startY = 24;
              const position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, startY);

              ctx.save();

              ctx.beginPath();
              ctx.setLineDash([5, 3]);
              ctx.moveTo(position.x, startY + padding);
              ctx.lineTo(position.x, position.y - padding);
              ctx.strokeStyle = 'rgba(255,255,255,0.12)';
              ctx.stroke();

              ctx.restore();
            });
          }
        });
      }
    });
  }

  filterForm = this.formBuilder.group({
    branchIds: [''],
    yearIds: [''],
    monthIds: [''],
    quarterIds: ['']
  });

  getUserBranches() {
    this._taxsDashboardService.getUserBranches().subscribe(res => {
      this.branches = res.data;
      localStorage.setItem('USER_BRANCHES', JSON.stringify(this.branches));
    });
  }

  apply(formValue?, e?) {
    let params = {
      'branchIds': formValue?.branchIds?.length > 0 ? formValue.branchIds : [this.branchId],
      'monthIds': formValue?.monthIds?.length > 0 ? formValue.monthIds : [],
      'quarterIds': formValue?.quarterIds?.length > 0 ? formValue.quarterIds : [],
      'yearIds': formValue?.yearIds?.length > 0 ? formValue.yearIds : [],
      'pageNumber': 0,
      'pageSize': 10
    };
    if (e?.type === 'click') {
      localStorage.setItem('TAX_DETAILS', JSON.stringify(params));
    } else {
      let taxDetails = JSON.parse(localStorage.getItem('TAX_DETAILS'));
      if (taxDetails?.branchIds?.length > 0 || taxDetails?.monthIds?.length > 0 || taxDetails?.quarterIds?.length > 0 || taxDetails?.yearIds?.length > 0) {
        params = taxDetails;
        this.filterForm.patchValue(params);
      } else {
        localStorage.setItem('TAX_DETAILS', JSON.stringify(params));
        this.filterForm.patchValue(params);
      }
    }

    // set local-storage
    this.totalPurchasesTax(params);
    this.totalSalesTax(params);
    this.differenceInTax(params);
    this.purchasesSalesDifferenceTaxesChart(params);
    this.purchasesTaxChart(params);
    this.salesTaxChart(params);
    this.purchasingAndSalesTaxChart(params);
  }

  differenceInTax(params) {
    this._taxsDashboardService.differenceInTax(params).subscribe(res => this.differenceInTaxValue = res);
  }

  totalSalesTax(params) {
    this._taxsDashboardService.totalSalesTax(params).subscribe(res => this.totalSalesTaxValue = res);
  }

  totalPurchasesTax(params) {
    this._taxsDashboardService.totalPurchasesTax(params).subscribe(res => this.totalPurchasesTaxValue = res);
  }

  purchasesSalesDifferenceTaxesChart(params) {
    this._taxsDashboardService.purchasesSalesDifferenceTaxesChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, null, null);
        this.purchasesSalesDifferenceChart = this
          .prepareChart(res.data.map(item => item.elementString), res.data.map(item => item.elementSum), 'purchasesSalesDifferenceChart',
            option, 'doughnut');
      }
    });
  }

  purchasesTaxChart(params: any) {
    this._taxsDashboardService.purchasesTaxChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.data.find(item => item.elementCount == tooltipItem[0].xLabel).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        });
        this.purchasesTaxsChart = this.prepareChart(res.data.map(item => item.elementCount), res.data.map(item => item.elementSum), 'purchasesTaxs', option);
      }
    });
  }

  salesTaxChart(params: any) {
    this._taxsDashboardService.salesTaxChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.data.find(item => item.elementCount == tooltipItem[0].xLabel).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        });
        this.salesTaxsChart = this.prepareChart(res.data.map(item => item.elementCount), res.data.map(item => item.elementSum), 'salesTaxs', option);
      }
    });
  }

  purchasingAndSalesTaxChart(params: any) {
    //Orders
    this._taxsDashboardService.purchasingAndSalesTaxChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          {
            callbacks: {
              title: function(tooltipItem, data) {
                return tooltipItem[0].xLabel;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          },
          {
            xAxes: [{
              ticks: {
                fontColor: '#FFF'
              },
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }, ticks: {
                beginAtZero: true,
                fontColor: '#FFF'
              }
            }]
          }
        );
        this.purchasingAndSalesTaxsChart = this.prepareChart(res.data.map(item => item.elementString), res.data.map(item => item.elementSum), 'purchasingAndSales', option, 'line');
      }
    });
  }

  cancel() {
    this.filterForm.reset();
    localStorage.setItem('TAX_DETAILS', JSON.stringify({}));
    this.apply();
  }
}
