import {BaseService} from '@khdma-frontend-nx/shared';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot,} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class TaxsDashboardService implements Resolve<any> {
  widgets: any[];
  taxUrl = 'api/v1/tax-dashboard';
  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _base: BaseService) {
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([]).then((res) => {
        resolve(res);
      }, reject);
    });
  }


  getUserBranches(): Observable<any> {
    return this._base.get(`api/v1/customer-branch/customer`);
  }

  differenceInTax(obj) {
    return this._base
      .post(obj, `${this.taxUrl}/tax-difference`)
      .pipe(
        map(res => res.data.elementSum),
        catchError(err => of(0))
      );
  }
  totalSalesTax(obj){
    return this._base
      .post(obj, `${this.taxUrl}/total-sales-tax`)
      .pipe(
        map(res => res.data.elementSum),
        catchError(err => of(0))
      );
  }
  totalPurchasesTax(obj){
    return this._base
      .post(obj, `${this.taxUrl}/total-purchases-tax`)
      .pipe(
        map(res => res.data.elementSum),
        catchError(err => of(0))
      );
  }
  purchasesSalesDifferenceTaxesChart(obj){
    return this._base
      .post(obj, `${this.taxUrl}/all-taxes-chart`)
      .pipe(
        catchError(err => of(0))
      );
  }
  purchasesTaxChart(obj){
    return this._base
      .post(obj, `${this.taxUrl}/purchasing-tax-chart`)
      .pipe(
        catchError(err => of(0))
      );
  }
  salesTaxChart(obj){
    return this._base
      .post(obj, `${this.taxUrl}/sales-tax-chart`)
      .pipe(
        catchError(err => of(0))
      );
  }
  purchasingAndSalesTaxChart(obj){
    return this._base
      .post(obj, `${this.taxUrl}/purchasing-sales-tax-chart`)
      .pipe(
        catchError(err => of(0))
      );
  }
  salesTaxDetails(obj){
    return this._base
      .post(obj, `${this.taxUrl}/sales-tax-details`)
      .pipe(
        catchError(err => of(err))
      );
  }
  purchasingTaxDetails(obj){
    return this._base
      .post(obj, `${this.taxUrl}/purchasing-tax-details`)
      .pipe(
        catchError(err => of(err))
      );
  }
}
