import {Constant,SnackService,SearchRequest,SearchConfiguration} from '@khdma-frontend-nx/shared';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {TaxsDashboardService} from '../taxs-dashboard.service';
import {Location} from '@angular/common';

@Component({
  selector: 'tax-details',
  templateUrl: './tax-details.component.html',
  styleUrls: ['./tax-details.component.scss'],
})
export class TaxDetailsComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  searchData: any = {};
  taxDetailsParams = JSON.parse(localStorage.getItem('TAX_DETAILS'));


  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['invoiceNumber', 'invoiceDate', 'invoiceAmountWithTax', 'invoiceAmountWithoutTax', 'taxAmount'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  page: SearchRequest = new SearchRequest();
  searchConfiguration: any = new SearchConfiguration();
  initialSearchCriteria = {
    branchIds: [],
    monthIds: [],
    quarterIds: [],
    yearIds: []
  };
  isSales: boolean = false;
  isPurchase: boolean = false;


  constructor(
    private _taxsDashboardService: TaxsDashboardService,
    private _router: Router,
    public _snack: SnackService,
    public _translateService: TranslateService,
    public constant: Constant,
    public _translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private locations: Location,
  ) {
    // this.filter();
    this.subscriptions.add(
      this.activatedRoute.params.subscribe(routeParams => {
        if (routeParams.id === 'sales') {
          this.isSales = true;
          this.salesTaxDetails(this.taxDetailsParams);
        } else if (routeParams.id === 'purchase') {
          this.isPurchase = true;
          this.purchasingTaxDetails(this.taxDetailsParams);
        }
      })
    );

  }


  loadSales() {
    this.isPurchase = false;
    this.isSales = true;
    this.salesTaxDetails(this.taxDetailsParams);
  }

  loadPurchase() {
    this.isSales = false;
    this.isPurchase = true;
    this.purchasingTaxDetails(this.taxDetailsParams);
  }

  ngOnInit(): void {

  }



  onPageChange(event?: PageEvent) {
    const query = new SearchRequest(this.initialSearchCriteria, event.pageIndex, this.page.pageSize);
    this.page.pageNumber = event.pageIndex;
    if (this.isPurchase) {
      this.purchasingTaxDetails(query);
    }
    if (this.isSales) {
      this.salesTaxDetails(query);
    }
  }

  tableDataSource(source: any) {
    this.dataSource = new MatTableDataSource(source.data.content);
    this.page.totalElements = source.data.totalElements;
  }

  navigateBack() {
    this.locations.back();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  salesTaxDetails(obj) {
    this.subscriptions.add(
      this._taxsDashboardService.salesTaxDetails(obj).subscribe(res => {
        this.tableDataSource(res);
      })
    );
  }

  purchasingTaxDetails(obj) {
    this.subscriptions.add(
      this._taxsDashboardService.purchasingTaxDetails(obj).subscribe(res => {
        this.tableDataSource(res);
      })
    );
  }
}
