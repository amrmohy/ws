import {Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {TaxsDashboardService} from '../taxs-dashboard.service';


@Injectable()
export class TaxDetailsResolve implements Resolve<any[]> {

  constructor(private _taxsDashboardService: TaxsDashboardService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    return null;
  }
}
