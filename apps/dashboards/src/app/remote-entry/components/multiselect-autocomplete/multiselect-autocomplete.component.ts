import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {UntypedFormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {delay, take, takeUntil} from 'rxjs/operators';
import {MatSelect} from '@angular/material/select';
import {SalesDashboardService} from "../../sales-dashboard/sales-dashboard.service";


interface Item {
  id: string;
  name: string;
}

@Component({
  selector: 'multiselect-autocomplete',
  templateUrl: './multiselect-autocomplete.component.html',
  styleUrls: ['./multiselect-autocomplete.component.scss']
})

export class MultiselectAutocompleteComponent implements OnInit, AfterViewInit, OnDestroy {
  public MultiFilterCtrl: UntypedFormControl = new UntypedFormControl();
  public searching: boolean = false;
  public filteredItemsMulti: ReplaySubject<Item[]> = new ReplaySubject<Item[]>(1);

  @ViewChild('multiSelect') multiSelect: MatSelect;

  protected _onDestroy = new Subject<void>();


  @Input() set data(data: any[]) {
    this._data = data;
    this.lastPage = this.inputLastPage;
    this.filteredItemsMulti.next(this.data.slice());
  }

  get data(): any[] {
    return this._data;
  }

  // lastpage bool
  @Input() set inputLastPage(inputLastPage) {
    this._isLast = inputLastPage;
    this.lastPage = this.inputLastPage;
    this.filteredItemsMulti.next(this.data.slice());
  }

  get inputLastPage() {
    return this._isLast;
  }

  // pagenumber
  @Input() set inputPageNumber(inputPageNumber) {
    this._number = inputPageNumber;
    this.pageNumber = this.inputPageNumber;
  }

  get inputPageNumber() {
    return this._number;
  }

  // total pages
  @Input() set inputTotalPages(inputTotalPages) {
    this._total = inputTotalPages;
    this.totalPages = this.inputTotalPages;
  }

  get inputTotalPages() {
    return this._total;
  }

  @Input() set pageIsPageable(pageIsPageable) {
    this._pageable = pageIsPageable;
    this.isPageable = this.pageIsPageable;
  }

  get pageIsPageable() {
    return this._pageable;
  }


  private _data: any[];
  private _isLast;
  private _number;
  private _total;
  private _pageable;
  @Output() result: EventEmitter<any> = new EventEmitter<any>();
  @Input() outlineLabel: String;
  @Input() itemMultiCtrl: UntypedFormControl;

  @Input() branchIds: [];
  @Input() categoriesIds: [];
  @Input() subCategoriesIds: [];
  @Input() branchId: number;
  pageNumber: number;
  totalPages: number;
  lastPage: Boolean = false;
  empty: Boolean = false;
  isPageable: Boolean = false;
  searchVal: null;

  constructor(private _salesDashboardService: SalesDashboardService,) {
  }

  ngOnInit() {
    this.MultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy), delay(1000))
      .subscribe(() => {
        this.filterItemsMulti();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onChange($event) {
    this.result.emit($event);
  }

  /**
   * Sets the initial value after the filteredItems are loaded initially
   */
  protected setInitialValue() {
    this.filteredItemsMulti
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.multiSelect.compareWith = (a: Item, b: Item) => a && b && a.id === b.id;
      });
  }

  protected filterItemsMulti() {
    if (!this.data) {
      return;
    }
    let search = this.MultiFilterCtrl.value;
    if (!search) {
      this.searchVal = search;
      this.filteredItemsMulti.next(this.data.slice());
      return;
    } else {
      search = search.toLowerCase();
      this.searchVal = search;
      if (this.isPageable) {
        this.initalSearch(this.searchVal);
      } else {
        this.filteredItemsMulti.next(
          this.data.filter(item => item.name.toLowerCase().indexOf(search) > -1)
        );
      }
    }
  }

  initalSearch(searchVal) {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.branchId;
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    let subCategoriesIds = this.subCategoriesIds.length > 0 ? this.subCategoriesIds : [];
    this._salesDashboardService.findItemsByBranchIds(ids, categoryIds, subCategoriesIds, 0, 10, searchVal).subscribe(res => {
      if (res.success) {
        this.data = res.data.content;
        this.empty = res.data.empty;
        this.totalPages = res.data.totalPages;
        this.lastPage = res.data.last;
        this.pageNumber = res.data.number;
      }
    });
  }

  loadMoreItems() {
    if (this.lastPage == false) {
      if (this.totalPages > this.pageNumber) {
        this.pageNumber += 1;
        if (this.searchVal) {
          this.filterMoreItems(this.searchVal);
        } else {
          this.filterMoreItems();
        }

      }
    }
  }

  filterMoreItems(searchVal?) {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.branchId;
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    let subCategoriesIds = this.subCategoriesIds.length > 0 ? this.subCategoriesIds : [];
    this._salesDashboardService.findItemsByBranchIds(ids, categoryIds, subCategoriesIds, this.pageNumber, 10, searchVal).subscribe(res => {
      if (res.success) {
        let arr = this.data;
        this.data = arr.concat(res.data.content);
        this.totalPages = res.data.totalPages;
        this.lastPage = res.data.last;
        this.pageNumber = res.data.number;
      }
    });
  }

  resetItems() {
    this.searchVal = null;
    let ids = this.branchIds.length > 0 ? this.branchIds : this.branchId;
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    let subCategoriesIds = this.subCategoriesIds.length > 0 ? this.subCategoriesIds : [];
    this._salesDashboardService.findItemsByBranchIds(ids, categoryIds, subCategoriesIds, 0, 10).subscribe(res => {
      if (res.success) {
        this.data = res.data.content;
        this.totalPages = res.data.totalPages;
        this.lastPage = res.data.last;
        this.pageNumber = res.data.number;
      }
    });
  }

  reset() {
    this.resetItems();
  }
}
