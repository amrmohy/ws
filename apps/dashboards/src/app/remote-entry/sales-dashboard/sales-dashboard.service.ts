import {BaseService} from '@khdma-frontend-nx/shared';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot,} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class SalesDashboardService implements Resolve<any> {
  widgets: any[];
  baseUrl = 'api/v1/dashboard';

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient, private _base: BaseService) {
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([]).then((res) => {
        resolve(res);
      }, reject);
    });
  }




  getConsumersCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/consumers-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }

  getCsutomersCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/customers-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getCategoriesCount(branchId: number): Observable<any> {
    return this._base.get(`${this.baseUrl}/categories-count${branchId ? '?branchId=' + branchId : ''}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getTopConsumersChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-consumers-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(null))
    );
  }

  getTopCustomersChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-customers-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(null))
    );
  }

  getTopSalesItemsChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-sales-items-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(null))
    );
  }

  getCustomersCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/customers-count?${this.prepareQuery(params)}`)
      .pipe(
        map(res => res.elementCount),
        catchError(error => of(null))
      );
  }

  getCsutomersCountAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/customers-count-am?${this.prepareQuery(params)}`)
      .pipe(
        map(res => res.elementCount),
        catchError(error => of(null))
      );
  }


  getOrders(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/orders-chart?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getOrdersAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/orders-chart-am?${this.prepareQuery(params)}`).pipe(
      catchError(error => of(0))
    );
  }

  getApprovedOrdersCount(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/approved-orders-count?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getApprovedOrdersCountAM(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/approved-orders-count-am?${this.prepareQuery(params)}`).pipe(
      map(res => res.elementCount),
      catchError(error => of(0))
    );
  }


  getCustomerSalesChart(params: any): Observable<any> {
    return this._base.get(`${this.baseUrl}/top-customer-sales-chart?${this.prepareQuery(params)}`)
      .pipe(
        catchError(error => of(0))
      );
  }

  prepareQuery(params: any) {
    if (params) {
      let paramsQuery = Object.keys(params).map(key => params[key] && params[key] != 'null' ? key + '=' + params[key] : '').filter(item => item != '');
      return paramsQuery.length > 1 ? paramsQuery.join('&') : paramsQuery.join('');
    }
    return '';
  }


  getUserBranches(): Observable<any> {
    return this._base.get(`api/v1/customer-branch/customer`);
  }

  // searchName
  findBrandsByBranchIds(branchIds: any, pageNumber, pageSize, searchName?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let name = searchName ? `&searchName=${searchName}` : '';
    return this._base.get(`api/v1/brand/filter/branchs?${q + name}`);
  }


  findItemsByBranchIds(branchIds: any, categoryIds, subCategoryIds, pageNumber, pageSize, name?: null): Observable<any> {
    let q = `branchIds=${branchIds}&categoryIds=${categoryIds}&subCategoryIds=${subCategoryIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let search = name ? `&name=${name}` : '';
    return this._base.get(`api/v1/item/filter/branchs-categories-subCategories?${q + search}`);
  }


  findCategoriesByBranchIds(branchIds: any): Observable<any> {
    let q = `branchIds=${branchIds}`;
    return this._base.get(`api/v1/category/branchs/filter?${q}`);
  }




  findSubCategoriesByIds(branchIds, categoryIds): Observable<any> {
    let q = `branchIds=${branchIds}&categoryIds=${categoryIds}`;
    return this._base.get(`api/v1/subcategory/filter/categories-branchs?${q}`);
  }

  //consumer
  findConsumerByBranchIds(branchIds: any, pageNumber, pageSize, searchName?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let name = searchName ? `&searchName=${searchName}` : '';
    return this._base.get(`api/v1/appuser/filter/consumer/branchs?${q + name}`);
  }

  //sales-person
  findSalesPersonsByBranchIds(branchIds: any, pageNumber, pageSize, searchName?: null): Observable<any> {
    let q = `branchIds=${branchIds}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    let name = searchName ? `&searchName=${searchName}` : '';
    return this._base.get(`api/v1/appuser/filter/sales-person/branchs?${q + name}`);
  }

  findUsersByBranchIds(branchIds: any): Observable<any> {

    let q = `branchIds=${branchIds}`;
    return this._base.get(`api/v1/appuser/pos/users/branchs?${q}`);
  }

  lastTwentySessionUser(userId){
    return this._base.get(`api/v1/pos-session/last-twenty-session-user/${userId}`);
  }

}
