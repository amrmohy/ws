import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SalesDashboardService} from './sales-dashboard.service';
import {UntypedFormBuilder, Validators} from '@angular/forms';
import {formatDate} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthService, Constant, BaseService, PreviewDownloadPdfDialogComponent} from '@khdma-frontend-nx/shared';
import { chartOption } from '../modal/dashboard/chartOption';
import {chartDataSet} from "../modal/dashboard/chartDataSet";
import { chartDataSetVale } from '../modal/dashboard/datasetValue';
import {chart} from "../modal/dashboard/chart";
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {Subscription} from 'rxjs';

@Component({
  selector: 'sales-dashboard',
  templateUrl: './sales-dashboard.component.html',
  styleUrls: ['./sales-dashboard.component.scss'],
})
export class SalesDashboardComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  constant = new Constant();
  periods = this.constant.PERIODS;
  orderTypes = this.constant.ORDER_TYPES;
  reportTypes = this.constant.SALES_REPORT_TYPES;
  actualExpectedKeys = this.constant.ACTUAL_EXPECTED;
  config = JSON.parse(localStorage.getItem('CONFIG'));
  currentUser = JSON.parse(localStorage.getItem('CURRENT_USER'));
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  branchId = this.branchConfig.id;
  services = this.config?.services;
  countries = this.config?.countries;
  userTypes = this.config?.userTypes;
  paymentMethods = this.config?.paymentMethods;
  deliveryMethods = this.config?.deliveryMethods;
  years = this.config?.fisicalYears;
  months = this.config?.months;
  widgets: any;
  widget1SelectedYear = '2016';
  widget5SelectedDay = 'today';
  //----------------------------
  orderPeriod: string = 'WEEK';
  orderType: string;
  orderPaymentMethodId: number;
  orderDeliveryMethodId: number;
  approvedOrdersCountPeriod: string = 'WEEK';
  approvedOrdersCountOrderType: string;
  customerCountServiceId: number;
  customerSalesCountryId: number;
  customerSalesPeriod: string = 'WEEK';
  customerCountPeriod: string = 'WEEK';

  approvedOrdersCount: number;

  customersCount: number;
  consumersCount: number;

  //--------------------------------
  topCustomersChart: any = this.prepareChart();
  topConsumersChart: any = this.prepareChart();
  topSalesItemsChart: any = this.prepareChart();
  topCustomerSalesChart: any = this.prepareChart();
  ordersChart: any = this.prepareChart();
  maxDate = new Date();
  // toggle filter
  toggleFilterBranch: boolean = true;
  toggleFilterDate: boolean = true;
  toggleFilterPeriodMonth: boolean = false;
  toggleFilterPeriodYear: boolean = false;
  toggleFilterPaymentMethod: boolean = false;
  toggleFilterOrderType: boolean = false;
  toggleFilterBrand: boolean = false;
  toggleFilterItem: boolean = false;
  toggleFilterCategory: boolean = false;
  toggleFilterSubCategory: boolean = false;
  toggleFilterSalesPerson: boolean = false;
  toggleFilterConsumer: boolean = false;

  branches: any[] = [];
  brands: any[] = [];
  items: any[] = [];
  categories: any[] = [];
  subCategories: any[] = [];
  salesPersons: any[] = [];
  consumers: any[] = [];
  branchIds: any[] = [];
  categoriesIds: any[] = [];
  subCategoriesIds: any[] = [];
  brandsIds: any[] = [];
  itemsIds: any[] = [];
  consumersIds: any[] = [];
  salesPersonsIds: any[] = [];
  orderStatues: any[] = [];

  users: any[] = [];
  pos_sessions: any[] = [];
  userId: number;
  posSessionId: number;
  // paginated .. items
  itemPageNumber = 0;
  itemTotalPages = 0;
  itemLastPage = false;
  isPagePageable = false;
  isPageIsItem = false;

  isPageIsBrand = false;

  // report-type
  reportType = this.constant.SALES_REPORT.ORDER;
  pdfUrl: any;

  /**
   * Constructor
   *
   * @param {SalesDashboardService} _salesDashboardService
   */
  constructor(
    private _salesDashboardService: SalesDashboardService,
    private authService: AuthService,
    private _router: Router,
    private formBuilder: UntypedFormBuilder,
    private _base: BaseService,
    public _translate: TranslateService,
    private sanitizer: DomSanitizer,
    public bottomSheet: MatBottomSheet,
  ) {
    // Register the custom chart.js plugin
    this._registerCustomChartJSPlugin();
    this.setConfig();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  setConfig() {
    this.subscriptions.add(this.authService.config.subscribe(config => {
      if (config) {
        this.services = config.services;
        this.countries = config.countries;
        this.userTypes = config.userTypes;
        this.deliveryMethods = config.deliveryMethods;
        this.paymentMethods = config.paymentMethods;
        this.years = config.fisicalYears;
        this.months = config.months;
      }
    }));
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.load();
  }

  load() {
    if (this.currentUser) {
      if (this.constant.userIsAdmin()) {
        this._salesDashboardService.getCsutomersCount(null).subscribe(res => this.customersCount = res);
        this._salesDashboardService.getApprovedOrdersCount(null).subscribe(res => this.approvedOrdersCount = res);
        //Top Customers chart
        this.prepareTopCustomerChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
        //Top customer sales
        this.prepareTopCustomerSalesChart({period: 'WEEK', countryId: null});
        //orders
        this.prepareOrdersChart({period: 'WEEK'});
      }
      //User Type is Customer
      else if (this.constant.userIsCustomer() || this.constant.userIsSupervisor()) {
        this._salesDashboardService.getApprovedOrdersCount({branchId: null, period: this.periods[0]?.value, orderType: this.orderTypes[0]?.value}).subscribe(res => this.approvedOrdersCount = res);
        this._salesDashboardService.getConsumersCount(this.branchId).subscribe(res => this.consumersCount = res);
        //start charts ------------------------------------------------------------------------------------------------------------------
        //Top 20 Consumers
        this.prepareTopConsumerChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
        //Top Sales Items chart
        this.prepareTopSalesItemsChart({branchId: this.branchId == 0 ? null : this.branchId, period: 'WEEK'});
        //orders
        this.prepareOrdersChart({period: 'WEEK'});
      } else if (this.constant.userIsAccManager()) {
        this._salesDashboardService.getCsutomersCountAM(null).subscribe(res => this.customersCount = res);
        this._salesDashboardService.getApprovedOrdersCountAM(null).subscribe(res => this.approvedOrdersCount = res);
        //orders
        this.prepareOrdersChartAM({period: 'WEEK'});
      }
    }
    this.showFilterBranch();
    this.showFilterDate();
  }

  prepareTopCustomerChart(params: any) {
    this._salesDashboardService.getTopCustomersChart(params).subscribe(
      res => {
        if (res) {
          let option = new chartOption(null, null, null, {
            callbacks: {
              title: function(tooltipItem, data) {
                return res.find(item => item.elementIndex == tooltipItem[0].xLabel).elementString;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          });
          this.topCustomersChart = this.prepareChart(res.map(item => item.elementIndex), res.map(item => item.elementCount), 'To Customers', option);
        }
      }
    );
  }

  prepareTopSalesItemsChart(params: any) {
    this._salesDashboardService.getTopSalesItemsChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.find(item => item.elementIndex == tooltipItem[0].xLabel).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        });
        this.topSalesItemsChart = this.prepareChart(res.map(item => item.elementIndex), res.map(item => item.elementCount), 'Top Sales Items Chart', option);
      }
    });
  }

  prepareTopConsumerChart(params: any) {
    //Top consumers chart
    this._salesDashboardService.getTopConsumersChart(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null, {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.find(item => item.elementIndex == tooltipItem[0].xLabel).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        });
        this.topConsumersChart = this.prepareChart(res.map(item => item.elementIndex), res.map(item => item.elementCount), 'To Customers', option);
      }
    });
  }

  prepareTopCustomerSalesChart(params: any) {
    this._salesDashboardService.getCustomerSalesChart(params).subscribe(res => {
      let option = new chartOption(null, null, null,
        {
          callbacks: {
            title: function(tooltipItem, data) {
              return res.find(item => item.elementString.includes(tooltipItem[0].xLabel)).elementString;
            },
            label: function(tooltipItem, data) {
              return tooltipItem.value;
            }
          }
        }, {
          xAxes: [{
            ticks: {
              fontColor: '#FFF'
            },
            gridLines: {
              display: false
            }
          }],
          yAxes: [{
            gridLines: {
              display: false
            }, ticks: {
              beginAtZero: true,
              fontColor: '#FFF'
            }
          }]
        }
      );
      this.topCustomerSalesChart = this.prepareChart(res.map(item => item.elementString.split('-')[1]),
        res.map(item => item.elementCount), 'Top Customer Sales', option, 'line');
    });
  }

  prepareOrdersChart(params: any) {
    //Orders
    this._salesDashboardService.getOrders(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          {
            callbacks: {
              title: function(tooltipItem, data) {
                return tooltipItem[0].xLabel;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          },
          {
            xAxes: [{
              ticks: {
                fontColor: '#FFF'
              },
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }, ticks: {
                beginAtZero: true,
                fontColor: '#FFF'
              }
            }]
          }
        );
        this.ordersChart = this.prepareChart(res.map(item => item.elementDate), res.map(item => item.elementCount), 'Orders', option, 'line');
      }
    });
  }

  prepareOrdersChartAM(params: any) {
    //Orders
    this._salesDashboardService.getOrdersAM(params).subscribe(res => {
      if (res) {
        let option = new chartOption(null, null, null,
          {
            callbacks: {
              title: function(tooltipItem, data) {
                return tooltipItem[0].xLabel;
              },
              label: function(tooltipItem, data) {
                return tooltipItem.value;
              }
            }
          },
          {
            xAxes: [{
              ticks: {
                fontColor: '#FFF'
              },
              gridLines: {
                display: false
              }
            }],
            yAxes: [{
              gridLines: {
                display: false
              }, ticks: {
                beginAtZero: true,
                fontColor: '#FFF'
              }
            }]
          }
        );
        this.ordersChart = this.prepareChart(res.map(item => item.elementDate), res.map(item => item.elementCount), 'Orders', option, 'line');
      }
    });
  }


  prepareChart(labelsArr?: string[], data?: any[], chartTitle?: string, options?: chartOption, type?: string) {
    return new chart(new chartDataSet([new chartDataSetVale(data, chartTitle)], labelsArr), options, type);
  }

  customerCountServiceChange(serviceId: number) {
    this.customerCountServiceId = serviceId;
    let params = {branchId: null, period: this.customerCountPeriod, serviceId: serviceId};
    this._salesDashboardService.getCustomersCount(params).subscribe(res => this.customersCount = res);
  }

  customerCountServiceChangeAM(serviceId: number) {
    this.customerCountServiceId = serviceId;
    let params = {branchId: null, period: this.customerCountPeriod, serviceId: serviceId};
    this._salesDashboardService.getCsutomersCountAM(params).subscribe(res => this.customersCount = res);
  }

  customerCountPeriodChange(period: any) {
    this.customerCountPeriod = period;
    let params = {branchId: null, period: period, serviceId: this.customerCountServiceId};
    this._salesDashboardService.getCustomersCount(params).subscribe(res => this.customersCount = res);
  }

  customerCountPeriodChangeAM(period: any) {
    this.customerCountPeriod = period;
    let params = {branchId: null, period: period, serviceId: this.customerCountServiceId};
    this._salesDashboardService.getCsutomersCountAM(params).subscribe(res => this.customersCount = res);
  }


  approvedOrdersCountPeriodChange(period: any) {
    this.approvedOrdersCountPeriod = period;
    let params = {branchId: null, period: period, orderType: this.approvedOrdersCountOrderType};
    this._salesDashboardService.getApprovedOrdersCount(params).subscribe(res => this.approvedOrdersCount = res);
  }

  approvedOrdersCountPeriodChangeAM(period: any) {
    this.approvedOrdersCountPeriod = period;
    let params = {branchId: null, period: period, orderType: this.approvedOrdersCountOrderType};
    this._salesDashboardService.getApprovedOrdersCountAM(params).subscribe(res => this.approvedOrdersCount = res);
  }

  approvedOrdersCountTypeChange(orderType: string) {
    this.approvedOrdersCountOrderType = orderType;
    let params = {branchId: null, period: this.approvedOrdersCountPeriod, orderType: orderType};
    this._salesDashboardService.getApprovedOrdersCount(params)
      .subscribe(res => this.approvedOrdersCount = res);
  }

  approvedOrdersCountTypeChangeAM(orderType: string) {
    this.approvedOrdersCountOrderType = orderType;
    let params = {branchId: null, period: this.approvedOrdersCountPeriod, orderType: orderType};
    this._salesDashboardService.getApprovedOrdersCountAM(params).subscribe(res => this.approvedOrdersCount = res);
  }

  onTopConsumerPeriodChanged(event: any) {

    this.prepareTopConsumerChart({branchId: this.branchId == 0 ? null : this.branchId, period: event.value});
  }

  onTopCustomerPeriodChanged(event: any) {
    this.prepareTopCustomerChart({branchId: this.branchId == 0 ? null : this.branchId, period: event.value});
  }

  onTopSalesItemChanged(event: any) {

    this.prepareTopSalesItemsChart({branchId: this.branchId == 0 ? null : this.branchId, period: event.value});
  }

  onTopCustomerSalesChanged(event: any) {

    this.customerSalesPeriod = event.value;
    this.prepareTopCustomerSalesChart({countryId: this.customerSalesCountryId, period: event.value});
  }

  orderTypeChange(orderType: string) {
    this.orderType = orderType;
    let params = {branchId: null, period: this.orderPeriod, orderType: orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChart(params);
  }

  orderPaymentMethodChange(paymentMethodId: number) {
    this.orderPaymentMethodId = paymentMethodId;
    let params = {branchId: null, period: this.orderPeriod, orderType: this.orderType, paymentMethod: paymentMethodId, deliveryMethod: this.orderDeliveryMethodId};
    this.prepareOrdersChart(params);
  }

  orderDeliveryMethodChange(deliveryMethodId: number) {
    this.orderDeliveryMethodId = deliveryMethodId;
    let params = {branchId: null, period: this.orderPeriod, orderType: this.orderType, paymentMethod: this.orderPaymentMethodId, deliveryMethod: deliveryMethodId};
    this.prepareOrdersChart(params);
  }

  ordersPeriodChange(period: any) {
    this.orderPeriod = period;
    let params = {branchId: null, period: period, orderType: this.orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChart(params);
  }

  ordersPeriodChangeAM(period: any) {
    this.orderPeriod = period;
    let params = {branchId: null, period: period, orderType: this.orderType, paymentMethod: null, deliveryMethod: null};
    this.prepareOrdersChartAM(params);
  }

  topCustomerSalesCountryChange(countryId: any) {
    this.customerSalesCountryId = countryId;
    this.prepareTopCustomerSalesChart({countryId: countryId, period: this.customerSalesPeriod});
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Register a custom plugin
   */
  private _registerCustomChartJSPlugin(): void {
    (window as any).Chart.plugins.register({
      afterDatasetsDraw: function(chart, easing): any {
        // Only activate the plugin if it's made available
        // in the options
        if (
          !chart.options.plugins.xLabelsOnTop ||
          (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
        ) {
          return;
        }

        // To only draw at the end of animation, check for easing === 1
        const ctx = chart.ctx;

        chart.data.datasets.forEach(function(dataset, i): any {
          const meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function(element, index): any {

              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
              const fontSize = 13;
              const fontStyle = 'normal';
              const fontFamily = 'Roboto, Helvetica Neue, Arial';
              ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              const dataString = dataset.data[index].toString() + 'k';

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              const padding = 15;
              const startY = 24;
              const position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, startY);

              ctx.save();

              ctx.beginPath();
              ctx.setLineDash([5, 3]);
              ctx.moveTo(position.x, startY + padding);
              ctx.lineTo(position.x, position.y - padding);
              ctx.strokeStyle = 'rgba(255,255,255,0.12)';
              ctx.stroke();

              ctx.restore();
            });
          }
        });
      }
    });
  }


  filterForm = this.formBuilder.group({});

  showFilterDate() {
    this.toggleFilterDate = true;
    this.filterForm.addControl('from', this.formBuilder.control('', []));
    this.filterForm.addControl('to', this.formBuilder.control('', []));
    this.filterForm.patchValue({
      from: new Date(new Date().setDate(new Date().getDate() - 1)).toISOString().substring(0, 10)
    });
    this.filterForm.patchValue({
      to: new Date(new Date().setDate(new Date().getDate() - 1)).toISOString().substring(0, 10)
    });
  }

  removeFilterDate() {
    this.toggleFilterDate = false;
    this.filterForm.removeControl('from');
    this.filterForm.removeControl('to');
  }

  showFilterPeriodMonth() {
    this.toggleFilterPeriodMonth = true;
    this.filterForm.addControl('month', this.formBuilder.control('', []));
  }

  removeFilterPeriodMonth() {
    this.toggleFilterPeriodMonth = false;
    this.filterForm.removeControl('month');
  }

  showFilterPeriodYear() {
    this.toggleFilterPeriodYear = true;
    this.filterForm.addControl('year', this.formBuilder.control('', []));
  }

  removeFilterPeriodYear() {
    this.toggleFilterPeriodYear = false;
    this.filterForm.removeControl('year');
  }


  showFilterPaymentMethod() {
    this.toggleFilterPaymentMethod = true;
    this.filterForm.addControl('paymentMethod', this.formBuilder.control('', []));
  }

  removeFilterPaymentMethod() {
    this.toggleFilterPaymentMethod = false;
    this.filterForm.removeControl('paymentMethod');
  }


  // orderType
  showFilterOrderType() {
    this.toggleFilterOrderType = true;
    this.filterForm.addControl('orderType', this.formBuilder.control('', []));

  }

  removeFilterOrderType() {
    this.toggleFilterOrderType = false;
    this.filterForm.removeControl('orderType');
  }

  // baranch
  getUserBranches() {
    this._salesDashboardService.getUserBranches().subscribe(res => {
      this.branches = res.data;
    });
  }

  showFilterBranch() {
    this.getUserBranches();
    this.toggleFilterBranch = true;
    this.filterForm.addControl('branch', this.formBuilder.control('', []));
    if (this.currentUser.branchId) {
      this.filterForm.patchValue({
        branch: [this.currentUser.branchId]
      });
    }
  }

  removeFilterBranch() {
    this.toggleFilterBranch = false;
    this.filterForm.removeControl('branch');
    this.branchIds = [];

    // load brand without branchids
    if (this.toggleFilterBrand) {
      this.filterForm.patchValue({
        brand: ''
      });
      this.findBrandsByBranchIds();
    }
    // load category without branchids
    if (this.toggleFilterCategory) {
      this.filterForm.patchValue({
        category: ''
      });
      this.findCategoriesByBranchIds();
    }
    // load subcategory without branchids
    if (this.toggleFilterSubCategory) {
      this.filterForm.patchValue({
        subCategory: ''
      });
      this.findSubCategoriesByIds();
    }
    // load items without branchids
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }

    if (this.toggleFilterSalesPerson) {
      this.filterForm.patchValue({
        salesPerson: ''
      });
      this.findSalesPersonByBranchIds();
    }
    if (this.toggleFilterConsumer) {
      this.filterForm.patchValue({
        consumer: ''
      });
      this.findConsumerByBranchIds();
    }
  }

  branchChanged(value) {
    this.branchIds = value;

    if (this.toggleFilterBrand) {
      this.findBrandsByBranchIds();
    }
    if (this.toggleFilterCategory) {
      this.findCategoriesByBranchIds();
    }
    if (this.toggleFilterSubCategory) {
      this.findSubCategoriesByIds();
    }
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
    if (this.toggleFilterSalesPerson) {
      this.findSalesPersonByBranchIds();
    }
    if (this.toggleFilterConsumer) {
      this.findConsumerByBranchIds();
    }
    if (this.reportType === this.constant.SALES_REPORT.POS_SESSION) {
      this.findUsersByBranchIds();
    }
  }

  // brand
  findBrandsByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._salesDashboardService.findBrandsByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.brands = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
        this.isPageIsBrand = true;
      }
    });
  }

  showFilterBrand() {
    this.findBrandsByBranchIds();
    this.toggleFilterBrand = true;
    this.filterForm.addControl('brand', this.formBuilder.control('', []));
  }

  removeFilterBrand() {
    this.toggleFilterBrand = false;
    this.brands = [];
    this.brandsIds = [];
    this.filterForm.removeControl('brand');
  }

  brandChanged(event) {
    this.brandsIds = event.value;
  }

  // category
  findCategoriesByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._salesDashboardService.findCategoriesByBranchIds(ids).subscribe(res => {
      if (res.success) {
        this.categories = res.data;
      }
    });
  }

  showFilterCategory() {
    this.findCategoriesByBranchIds();
    this.toggleFilterCategory = true;
    this.filterForm.addControl('category', this.formBuilder.control('', []));
  }

  removeFilterCategory() {
    this.toggleFilterCategory = false;
    this.filterForm.removeControl('category');
    this.categories = [];
    this.categoriesIds = [];
    // load subcategoryout with catids
    if (this.toggleFilterSubCategory) {
      this.filterForm.patchValue({
        subCategory: ''
      });
      this.findSubCategoriesByIds();
    }
    // load item without catids
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }
  }

  categoryChanged(event) {
    this.categoriesIds = event.value;
    // load subcategory
    if (this.toggleFilterSubCategory) {
      this.findSubCategoriesByIds();
    }
    // load items
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
  }

  //subCategories
  findSubCategoriesByIds() {
    let branchIds = this.branchIds.length > 0 ? this.branchIds : [this.currentUser.branchId];
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    this._salesDashboardService.findSubCategoriesByIds(branchIds, categoryIds).subscribe(res => {
      if (res.success) {
        this.subCategories = res.data;
      }
    });
  }

  // users
  findUsersByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._salesDashboardService.findUsersByBranchIds(ids).subscribe(res => {
      if (res && res?.length > 0) {
        this.users = res;
      }
    });
  }


  userChanged(id) {
    this.userId = id;
    this.posSessionId = null;
    this.filterForm.patchValue({
      posSessionId: ''
    });
    this.lastTwentySessionUser();
  }

  //subCategories
  lastTwentySessionUser() {
    let userId = this.userId ? this.userId : null;
    this._salesDashboardService.lastTwentySessionUser(userId).subscribe(res => {
      if (res.success) {
        console.log(res.data);
        this.pos_sessions = res.data;
      }
    });
  }

  sessionChanged(id) {
    this.posSessionId = id;
  }

  showFilterSubCategory() {
    this.findSubCategoriesByIds();
    this.toggleFilterSubCategory = true;
    this.filterForm.addControl('subCategory', this.formBuilder.control('', []));
  }

  removeFilterSubCategory() {
    this.toggleFilterSubCategory = false;
    this.filterForm.removeControl('subCategory');
    this.subCategories = [];
    this.subCategoriesIds = [];
    if (this.toggleFilterItem) {
      this.filterForm.patchValue({
        item: ''
      });
      this.findItemsByBranchIds();
    }
  }

  subCategoryChanged(event) {
    this.subCategoriesIds = event.value;
    // load items
    if (this.toggleFilterItem) {
      this.findItemsByBranchIds();
    }
  }

  // item
  findItemsByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    let categoryIds = this.categoriesIds.length > 0 ? this.categoriesIds : [];
    let subCategoriesIds = this.subCategoriesIds.length > 0 ? this.subCategoriesIds : [];
    this._salesDashboardService.findItemsByBranchIds(ids, categoryIds, subCategoriesIds, 0, 10).subscribe(res => {
      if (res.success) {
        this.items = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
        this.isPageIsItem = true;
      }
    });
  }

  showFilterItem() {
    this.findItemsByBranchIds();
    this.toggleFilterItem = true;
    this.filterForm.addControl('item', this.formBuilder.control('', []));
  }

  removeFilterItem() {
    this.toggleFilterItem = false;
    this.filterForm.removeControl('item');
    this.items = [];
    this.itemsIds = [];
  }

  itemChanged(event) {
    this.itemsIds = event.value;

  }

  // sales person
  findSalesPersonByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._salesDashboardService.findSalesPersonsByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.salesPersons = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
      }
    });
  }

  showFilterSalesPerson() {
    this.findSalesPersonByBranchIds();
    this.toggleFilterSalesPerson = true;
    this.filterForm.addControl('salesPerson', this.formBuilder.control('', []));
  }

  removeFilterSalesPerson() {
    this.toggleFilterSalesPerson = false;
    this.salesPersons = [];
    this.salesPersonsIds = [];
    this.filterForm.removeControl('salesPerson');
  }

  salesPersonChanged(event) {
    this.salesPersonsIds = event.value;
  }

  // CONSUMER
  findConsumerByBranchIds() {
    let ids = this.branchIds.length > 0 ? this.branchIds : this.currentUser.branchId;
    this._salesDashboardService.findConsumerByBranchIds(ids, 0, 10).subscribe(res => {
      if (res.success) {
        this.consumers = res.data.content;
        this.itemTotalPages = res.data.totalPages;
        this.itemLastPage = res.data.last;
        this.itemPageNumber = res.data.number;
        this.isPagePageable = true;
      }
    });
  }

  showFilterConsumer() {
    this.findConsumerByBranchIds();
    this.toggleFilterConsumer = true;
    this.filterForm.addControl('consumer', this.formBuilder.control('', []));
  }

  removeFilterConsumer() {
    this.toggleFilterConsumer = false;
    this.consumers = [];
    this.consumersIds = [];
    this.filterForm.removeControl('consumer');
  }

  consumerChanged(event) {
    this.consumersIds = event.value;
  }

  // report
  getReportType(value) {
    this.reportType = value;
    if (this.reportType === this.constant.SALES_REPORT.POS_SESSION) {
      this.filterForm.addControl('user', this.formBuilder.control('', []));
      this.filterForm.addControl('posSessionId', this.formBuilder.control('', [Validators.required]));
      this.findUsersByBranchIds();
    } else {
      this.filterForm.removeControl('user');
      this.filterForm.removeControl('posSessionId');
    }
    if (this.reportType === this.constant.SALES_REPORT.DETAILS_BRAND) {
      if (this.toggleFilterOrderType) {
        this.removeFilterOrderType();
      }
      if (this.toggleFilterPaymentMethod) {
        this.removeFilterPaymentMethod();
      }
      if (this.toggleFilterSalesPerson) {
        this.removeFilterSalesPerson();
      }

      if (this.toggleFilterConsumer) {
        this.removeFilterConsumer();
      }
    }
  }

  selectedReportType(value: string) {
    return this.reportType === value;
  }

  preview(formValue) {
    let initalParams = {
      branchIds: this.branchIds.length > 0 ? this.branchIds : [this.currentUser.branchId],
      brandIds: this.brandsIds.length > 0 ? this.brandsIds : null,
      categoryIds: this.categoriesIds.length > 0 ? this.categoriesIds : null,
      consumerIds: this.consumersIds.length > 0 ? this.consumersIds : null,
      createdMonths: formValue?.month ? formValue?.month : null,
      createdYears: formValue?.year ? formValue?.year : null,
      createDateFrom: formValue?.from ? formatDate(formValue?.from, 'yyyy-MM-dd', 'en') : '',
      createDateTo: formValue?.to ? formatDate(formValue?.to, 'yyyy-MM-dd', 'en') : '',
      orderStatusIds: formValue?.orderStatus ? formValue?.orderStatus : null,
      orderTypeId: formValue?.orderType ? formValue?.orderType?.toString() : '',
      paymentMethodIds: formValue?.paymentMethod ? formValue?.paymentMethod : null,
      itemIds: this.itemsIds.length > 0 ? this.itemsIds : null,
      subCategoryIds: this.subCategoriesIds.length > 0 ? this.subCategoriesIds : null,
      salesPersonIds: this.salesPersonsIds.length > 0 ? this.salesPersonsIds : null,
      currentDate: formatDate(new Date(), this.constant.DATE_TIME_FORMAT, 'en')
    };
    let paramters = null;
    if (this.reportType === this.constant.SALES_REPORT.POS_SESSION) {
      paramters = {posSessionId: `${this.posSessionId}`};
    } else {
      paramters = this.clean(initalParams);
    }

    let reportDetailObj = {
      reportName: this.reportType + '_' + this._translate.currentLang,
      fileName: this.reportType + '_' + this._translate.currentLang,
      paramters: paramters,
    };
    this.printReport(reportDetailObj, this.reportType);
  }

  // print report
  printReport(reportObj, reportType) {
    this._base.downLoad(reportObj, `api/v1/report/pdf`)
      .subscribe(res => {
        const blob = new Blob([res], {type: 'application/pdf'});
        this.previewDownloadPdf(blob, `${reportType}.pdf`);
      });
  }



  previewDownloadPdf(blobData, filename) {
    const dialogRef = this.bottomSheet.open(PreviewDownloadPdfDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {blob: blobData, filename: filename}
    });
    dialogRef.afterDismissed().subscribe((result) => {

    });
  }

  clean(obj) {
    for (let propName in obj) {
      if (obj[propName] === null || obj[propName] === '') {
        delete obj[propName];
      }
    }
    return obj;
  }
}
