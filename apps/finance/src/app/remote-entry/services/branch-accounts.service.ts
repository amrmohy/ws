import {Injectable} from '@angular/core';
import { BaseService } from '@khdma-frontend-nx/shared';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BranchAccountsService {
  serviceShopping = 'api/v1/service/shopping';
  serviceCategory= 'api/v1/service-category/services';
  searchCriteria: any;
  accounts: any;
  BranchId: any;
  constructor(private _base:BaseService) { }
  // getAllAccounts(account?) {
  //   return  this._base.post(account,'api/v1/branch-account/filter')
  // }
  getBranchAccounts(requestBody) {
    return  this._base.post(requestBody,'api/v1/account/branch')
  }
  getAccountById(id) {
    return this._base.get(`api/v1/branch-account/${id}`)
  }

  createAccount(accountData) {
    return this._base.post(accountData,'api/v1/account')
  }
  deleteAccount(id) {
    return this._base.deleteById(`api/v1/branch-account?id=${id}`)
  }

  updateAccount(account) {
    return this._base.put(account,'api/v1/branch-account')
  }


  enableAccount(accountId,branchId,balance){
    return this._base.post({
      "account": {
        "id": accountId
      },
      "customerBranch": {
        "id": branchId
      },
      "balance": balance
    },'api/v1/branch-account')
  }


  enableAllAccounts(branchId){
   return  this._base.get(`api/v1/branch-account/enable/${branchId}`)
  }

  disableAccount(branchId , accountId){
    return this._base.deleteById(`api/v1/branch-account/deleteByBranchIdAndAccountId?branchId=${branchId}&accountId=${accountId}`)
  }

  // google translate
  callGoogleTranslateApi(text, source, target) :any{
    return this._base.callGoogleTranslateApi(text, source,target);
  }
  filter(itemObj?: any): Observable<any> {

    return this._base.post(itemObj, `api/v1/branch-account/filter`);
  }
  getServiceShopping(): Observable<any> {
    return this._base.get(`${this.serviceShopping}`);
  }
  getServiceCategory(ids): Observable<any> {
    return this._base.get(`${this.serviceCategory }?ids=${ids}`);
  }

  getTransactions(branchId , accountId){
    return this._base.get(`api/v1/branch-account/transaction/branch/${branchId}/account/${accountId}`)
  }


  getSearchCriteria() {
    return this.searchCriteria;
  }
  setSearchCriteria(searchCriteria) {
    this.searchCriteria = searchCriteria;
  }
  getBranchId() {
    return this.BranchId;
  }
  setBranchId(BranchId) {
    this.BranchId = BranchId;
  }

}
