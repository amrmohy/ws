import {Injectable} from '@angular/core';
import { BaseService } from '@khdma-frontend-nx/shared';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PayableInvoiceService {
  payableInvoiceBaseUrl = 'api/v1/payable-invoice';
  payableInvoicePayUrl = 'api/v1/payable-invoice-pay';
  payableInvoiceInstallmentPayUrl = 'api/v1/payable-installment-pay';
  payableInvoiceSettlementUrl = 'api/v1/finance/payable-settlement';
  supplierBalanceUrl = 'api/v1/payable-invoice/supplier-prepayment-balance';
  supplierPrepaymentsUrl = 'api/v1/payable-invoice/supplier-prepayment-invoices';

  constructor(private _base: BaseService) {
  }

  getPayableInvoiceDetailsById(id: number): Observable<any> {
    return this._base
      .get(`${this.payableInvoiceBaseUrl}/${id}`);
  }


  getSuppliers(customerId): Observable<any> {
    if (customerId != null) {
      return this._base.get(`api/v1/supplier/customer?customerId=${customerId}`);
    }
  }

  getPayableInvoiceStatus(): Observable<any> {
    return this._base.get('api/v1/payable-invoice-status');
  }

  getCustomerBranch(): Observable<any> {
    return this._base.get('api/v1/customer-branch/customer');
  }

  filter(itemObj?: any): Observable<any> {
    return this._base.post(itemObj, `${this.payableInvoiceBaseUrl}/filter`);
  }

  updateInvoice(invoicObj?: any): Observable<any> {
    return this._base.put(invoicObj, `${this.payableInvoiceBaseUrl}`);
  }


  getSuppliersByBranchId(branchId): Observable<any> {
    return this._base.get(`api/v1/supplier/by-branch/${branchId}`);
  }

  receivingOrdersWOutInv(branchId, supplierId?): Observable<any> {
    return this._base.get(`api/v1/receiving-order/without-invoice?branchId=${branchId}&supplierId=${supplierId}&pageNum=0&size=100`);
  }


  createInvoice(invoicObj?: any): Observable<any> {
    return this._base.post(invoicObj, `${this.payableInvoiceBaseUrl}`);
  }

  deletePayableInvoice(invoiceId): Observable<any> {
    return this._base.deleteById(`${this.payableInvoiceBaseUrl}?id=${invoiceId}`);
  }

  createPayableInvoicePay(payableInvoicePay?: any): Observable<any> {
    return this._base.post(payableInvoicePay, `${this.payableInvoicePayUrl}`);
  }

  getInstallmentsByPayableInvoiceId(payableInvoiceId): Observable<any> {
    return this._base.get(`api/v1/payable-installment/findByPayableInvoiceId/${payableInvoiceId}`);
  }

  createPayableInvoiceInstallmentPay(payableInvoicePay?: any): Observable<any> {
    return this._base.post(payableInvoicePay, `${this.payableInvoiceInstallmentPayUrl}`);
  }


  getPayableAccounts(branchId: number): Observable<any> {
    return this._base.get(`api/v1/finance/bank-account/payable-accounts/${branchId}`);
  }

  getbranchAccounts(accountTypeId, branchId): Observable<any> {
    return this._base.get(`api/v1/branch-account/type/${accountTypeId}/branch/${branchId}`);
  }

  getSupplierBalance(supplierId: number, branchId: number): Observable<any> {
    return this._base.get(`${this.supplierBalanceUrl}?supplierId=${supplierId}&branchId=${branchId}`);
  }

  getSupplierPrepayments(supplierId: number, branchId: number): Observable<any> {
    return this._base.get(`${this.supplierPrepaymentsUrl}?supplierId=${supplierId}&branchId=${branchId}`);
  }

  getPaysByInvoice(payableInvoiceId: number): Observable<any> {
    return this._base.get(`api/v1/payable-invoice-pay/pays-by-invoice/${payableInvoiceId}`);
  }

  payableInvDraftCount(branchIds, months?, years?): Observable<any> {
    let m = months && months.length > 0  ? `&months=${months}` : '';
    let y = years && years.length > 0 ? `&years=${years}` : '';
    return this._base.get(`${this.payableInvoiceBaseUrl}/draft/count?branchIds=${branchIds}${m}${y}`);
  }

  payableInvDraftDetails(branchIds, months?, years?): Observable<any> {
    let m = months  ? `&months=${months}` : '';
    let y = years ? `&years=${years}` : '';
    return this._base.get(`${this.payableInvoiceBaseUrl}/draft/details?branchIds=${branchIds}${m}${y}`);
  }

  postBulk(branchIds, months, years,obj?: any): Observable<any> {
    let m = months  ? `&months=${months}` : '';
    let y = years ? `&years=${years}` : '';
    return this._base.put(obj, `${this.payableInvoiceBaseUrl}/post/bulk?branchIds=${branchIds}${m}${y}`);
  }


  cancelPayableInvoice(invoicObj?: any): Observable<any> {
    return this._base.put(invoicObj, `${this.payableInvoiceBaseUrl}/cancel-invoice`);
  }

  postPayableInvoiceSettlement(payableInvoicePay?: any): Observable<any> {
    return this._base.post(payableInvoicePay, `${this.payableInvoiceSettlementUrl}/save-with-post`);
  }

  postPayableInvoicePay(payableInvoicePay?: any): Observable<any> {
    return this._base.post(payableInvoicePay, `${this.payableInvoicePayUrl}/save-with-post`);
  }

  cancelPayableInvoicePay(payableInvoicePay?: any): Observable<any> {
    return this._base.put(payableInvoicePay, `${this.payableInvoicePayUrl}/cancel-void-invoice`);
  }

}

