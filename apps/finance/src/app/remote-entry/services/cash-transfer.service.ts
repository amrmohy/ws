import {Injectable} from '@angular/core';
import { BaseService } from '@khdma-frontend-nx/shared';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CashTransferService {
  bankTransferBaseUrl = 'api/v1/bank-transfer';
  bankAccountCustomerBaseUrl = 'api/v1/finance/bank-account/customer';

  constructor(private _base: BaseService) {
  }

  getBankTransferDetailsById(id: number): Observable<any> {
    return this._base
      .get(`${this.bankTransferBaseUrl}/${id}`);
  }



  filter(itemObj?: any): Observable<any> {
    return this._base.post(itemObj, `${this.bankTransferBaseUrl}/filter`);
  }

  createBankTransfer(invoicObj?: any): Observable<any> {
    return this._base.post(invoicObj, `${this.bankTransferBaseUrl}`);
  }

  updateBankTransfer(bankTransfer?: any): Observable<any> {
    return this._base.put(bankTransfer, `${this.bankTransferBaseUrl}`);
  }


  deleteBankTransfer(bankTransferId): Observable<any> {
    return this._base.deleteById(`${this.bankTransferBaseUrl}?id=${bankTransferId}`);
  }
  bankAccountByCustomerId(customerId):Observable<any>{
    return this._base
      .get(`${this.bankAccountCustomerBaseUrl}/${customerId}`);
  }
}

