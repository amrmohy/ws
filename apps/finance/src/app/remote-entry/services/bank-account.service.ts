import {Injectable} from '@angular/core';
import { BaseService } from '@khdma-frontend-nx/shared';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BankAccountService {

  constructor(
    private _base: BaseService
  ) { }

  getBankAccountTypes() {
    return this._base.get('api/v1/account-type')
  }
  getBankAccountById(id) {
    return this._base.get(`api/v1/finance/bank-account/${id}`)
  }

  addBankAccount(body) {
    return this._base.post(body,'api/v1/finance/bank-account')
  }

  updateBankAccount(body) {
    return this._base.put(body,'api/v1/finance/bank-account')
  }
  removeBankAccount(id) {
    return this._base.deleteById(`api/v1/finance/bank-account?id=${id}`)
  }

  getCustomerBranch(): Observable<any> {
    return this._base.get('api/v1/customer-branch/customer');
  }

 getBranchEmployees(branchIds){
   return  this._base.get(`api/v1/appuser/employees/branch?branchIds=${branchIds}&pageNumber=0&pageSize=20`)
  }
  filter(requestBody?: any): Observable<any> {
    return this._base.post(requestBody, `api/v1/finance/bank-account/filter`);
  }

}
