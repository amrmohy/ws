import {Injectable} from '@angular/core';
import { BaseService } from '@khdma-frontend-nx/shared';
import {Observable, Subject} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChartOfAccountsService {
  serviceShopping = 'api/v1/service/shopping';
  serviceCategory= 'api/v1/service-category/services';
  private _refreshNeeded$ = new Subject<void>()
  private _refreshNeededOnCreate$ = new Subject<void>()
  createdAccount: any;
  updatedAccount: any;
  get refreshNeeded$() {
    return this._refreshNeeded$;
  }
  get refreshNeededOnCreate$() {
    return this._refreshNeededOnCreate$;
  }

  constructor(private _base:BaseService) { }
  getAllAccounts(account?) {
   return  this._base.post(account,'api/v1/account/filter')
  }
  getAccountById(id) {
    return this._base.get(`api/v1/account/${id}`)
  }

  createAccount(accountData) {
    return this._base.post(accountData,'api/v1/account').pipe(
      tap(()=>{
        this._refreshNeededOnCreate$.next();
      })
    );
  }
  deleteAccount(id) {
    return this._base.deleteById(`api/v1/account?id=${id}`)
  }

  updateAccount(account) {
    return this._base.put(account,'api/v1/account').pipe(
      tap(()=>{
        this._refreshNeeded$.next();
      })
    );
  }
  // google translate
  callGoogleTranslateApi(text, source, target) :any{
    return this._base.callGoogleTranslateApi(text, source,target);
  }
  filter(itemObj?: any): Observable<any> {

    return this._base.post(itemObj, `api/v1/account/filter`);
  }
  getServiceShopping(): Observable<any> {
    return this._base.get(`${this.serviceShopping}`);
  }
  getServiceCategory(ids): Observable<any> {
    return this._base.get(`${this.serviceCategory }?ids=${ids}`);
  }
  getDraftAccounts(): Observable<any> {
    return this._base.get(`api/v1/account/draft-accounts`);
  }
  getDraftAccountsCount(): Observable<any> {
    return this._base.get(`api/v1/account/draftAccountsCount`);
  }
  ApproveDraftAccount(draftAccountId){
    return this._base.put({
      "id": draftAccountId,
      "statusCode": 5,
      "statusCodeStr": "ACTIVE"
    },`api/v1/account/update-status`)
  }
  RejectDraftAccount(draftAccountId){
    return this._base.deleteById(`api/v1/account?id=${draftAccountId}`)
  }


  getCreatedAccount() {
    return this.createdAccount
  }

  setCreatedAccount(createdAccount) {
    this.createdAccount = createdAccount
  }

  getUpdatedAccount() {
    return this.updatedAccount
  }

  setUpdatedAccount(updatedAccount) {
    this.updatedAccount = updatedAccount
  }

}
