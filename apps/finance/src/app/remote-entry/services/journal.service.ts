import {Injectable} from '@angular/core';
import { BaseService } from '@khdma-frontend-nx/shared';

import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JournalService {
  journalBaseUrl = 'api/v1/journal';

  constructor(private _base: BaseService) {
  }

  filter(journalObj?: any): Observable<any> {
    return this._base.post(journalObj, `${this.journalBaseUrl}/filter`);
  }

  getJournalById(journalId):Observable<any>{
    return this._base.get(`${this.journalBaseUrl}/${journalId}`)
  }

  createJournal(journalObj?: any): Observable<any> {
    return this._base.post(journalObj, this.journalBaseUrl);
  }

  updateJournal(journalObj?: any): Observable<any> {
    return this._base.put(journalObj, this.journalBaseUrl);
  }

  deleteJournal(journalId): Observable<any> {
    return this._base.deleteById(`${this.journalBaseUrl}?id=${journalId}`);
  }

  getCustomerBranch(): Observable<any> {
    return this._base.get('api/v1/customer-branch/customer');
  }

  getConfigBranches(): Observable<any> {
    return this._base.get(`api/v1/customer-branch/config`);
  }

  getServiceCategoriesByBranch(branchId): Observable<any> {
    return this._base.get(`api/v1/service-category/branch/${branchId}`);
  }

  getSericesByBranch(branchId): Observable<any> {
    return this._base.get(`api/v1/service/branch/${branchId}`);
  }

  getCustomerCountry(customerId): Observable<any> {
    return this._base.get(`api/v1/country/customer/${customerId}`);
  }

  getbranchAccounts(accountTypeId,branchId): Observable<any> {
    return this._base.get(`api/v1/branch-account/type/${accountTypeId}/branch/${branchId}`);
  }

  reverseJournal(journalId?: any): Observable<any> {
    return this._base.post({},`api/v1/journal/reverse/${journalId}`);
  }
}

