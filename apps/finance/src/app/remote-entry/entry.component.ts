import {ChangeDetectorRef, Component, OnDestroy, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {MatSidenav} from "@angular/material/sidenav";
import {MediaMatcher} from "@angular/cdk/layout";
import {TranslateService} from "@ngx-translate/core";
import {BaseService} from "@khdma-frontend-nx/shared";

@Component({
  selector: 'khdma-frontend-nx-finance-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss'],
})
export class RemoteEntryComponent implements OnDestroy {
  private subscriptions = new Subscription();
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  @ViewChild('pSnav', { static: true }) sidenav: MatSidenav;
  isMenuFolded: boolean;
  currentRoute: string;

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _media: MediaMatcher,
    public _translate: TranslateService,
    private _base: BaseService
  ) {
    this.mobileQuery = this._media.matchMedia('(max-width: 639px)');
    this._mobileQueryListener = () => this._changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.subscriptions.add(
      this._base.sideNave.subscribe((action) => {
        if (action === 'close') {
          this.sidenav.close();
          this._base.sideNave.next(null);
        }
      })
    );
  }

  toggleSideNav() {
    const _sideNav = document.getElementById('pagesSideNavigation');
    const sideNav: HTMLElement = _sideNav as HTMLElement;
    if (this.mobileQuery.matches) {
      this.sidenav.toggle();
      this.isMenuFolded = false;
      sideNav.style.width = '280px';
    } else {
      if (sideNav.style.width === '280px') {
        sideNav.style.width = '62px';
        this.isMenuFolded = true;
      } else {
        sideNav.style.width = '280px';
        this.isMenuFolded = false;
      }
    }
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
    this.subscriptions.unsubscribe();
  }
}
