import {Component, Inject, OnInit} from '@angular/core';
import {UntypedFormBuilder, Validators} from '@angular/forms';
import {BankAccountService} from '../../services/bank-account.service';
import {TranslateService} from '@ngx-translate/core';
import {SnackService} from '@khdma-frontend-nx/shared';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-add-edit-bank-account',
  templateUrl: './add-edit-bank-account.component.html',
  styleUrls: ['./add-edit-bank-account.component.scss']
})
export class AddEditBankAccountComponent implements OnInit {
  branchEmployees;
  bankAccountTypes;
  CONFIG;
  branches;
  branchIds;
  banks;
  currencies;
  bankAccountType: boolean;
  safeType: boolean;
  pettyCashType: boolean;
  toggleEmployee = true;
  toggleIBAN = true;
  toggleAccountNumber = true;
  toggleBankSwift = true;
  toggleCurrency = true;
  success: boolean = false;
  customerId: any;
  bankForm = this._fb.group({
    bankAccountType: this._fb.group({
      id: ['', Validators.required]
    }),
    bank: this._fb.group({
      id: ['']
    }),
    accountNumber: [''],
    name: [''],
    bankSwift: [''],
    user: this._fb.group({
      id: ['']
    }),
    branchs: ['', Validators.required],
    bankBranch: [''],
    iban: [''],
    currency: this._fb.group({
      id: ['']
    }),
    payment: [null],
    collection: [null],
    payroll: [null],
    enableBankReconcilation: [null],
  });

  constructor(
    private _fb: UntypedFormBuilder,
    private _bankAccountService: BankAccountService,
    public _translate: TranslateService,
    private _snack: SnackService,
    private ItemDialogRef: MatDialogRef<AddEditBankAccountComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { account: any },
  ) {
    if (this.data?.account) {
      let id = data.account.id;
      this._bankAccountService.getBankAccountById(id).subscribe((res) => {
        res.data.branchs = res.data?.branchs?.map(br => br.id);
        console.log(res.data.branchs);
        this.onBranchChange(res.data.branchs);
        this.onBankTypeChange(res.data?.bankAccountType?.id);
        this.bankForm.patchValue(res.data);
        // this.onBankTypeChange(currentBankAccount.bankAccountType.id)

      });
    }

    this.CONFIG = JSON.parse(localStorage.getItem('CONFIG'));
    this.customerId =  JSON.parse(localStorage.getItem('BRANCH_CONFIG')).customerId;
    this.banks = this.CONFIG.banks;
    this._bankAccountService.getCustomerBranch().subscribe((res) => {
      this.branches = res.data;
    });
    this.bankAccountTypes = this.CONFIG.bankAccountTypes;
    this.currencies = this.CONFIG.currencies;
  }

  ngOnInit(): void {
  }


  onBankTypeChange(bankTypeId) {
    // ***** CASE1 BANK ACCOUNT TYPE === BANK ACCOUNT *****
    if (bankTypeId === 1) {
      this.bankAccountType = true;
      this.safeType = false;
      this.pettyCashType = false;
      this.hideAndShowForBankAccount();
    }

    // ***** CASE2 BANK ACCOUNT TYPE === SAFE *****

    if (bankTypeId === 2) {
      this.bankAccountType = false;
      this.safeType = true;
      this.pettyCashType = false;
      this.hideOnSafeAndPettyCash();
    }

    // ***** CASE3 BANK ACCOUNT TYPE === PETTY CASH *****

    if (bankTypeId === 3) {
      this.bankAccountType = false;
      this.safeType = false;
      this.pettyCashType = true;
      this.hideOnSafeAndPettyCash();

    }
  }

  hideAndShowForBankAccount() {
    // this.toggleEmployee = true;
    this.toggleAccountNumber = true;
    this.toggleIBAN = true;
    this.toggleBankSwift = true;
    this.toggleCurrency = true;
  }

  hideOnSafeAndPettyCash() {
    this.toggleEmployee = true;
    this.toggleAccountNumber = false;
    this.toggleIBAN = false;
    this.toggleBankSwift = false;
    this.toggleCurrency = false;
  }

  onBranchChange(branchId) {
    if(typeof branchId === 'object') {
      this.branchIds = branchId.map((id => id));
    }else {
      this.branchIds = branchId
    }
    this.getBranchEmployees();
  }

  getBranchEmployees() {
    this._bankAccountService.getBranchEmployees(this.branchIds).subscribe((res) => {
      this.branchEmployees = res.data.content;
    });
  }

  OnSaveBankAccount() {
    let branches = this.bankForm.get('branchs').value;
    let branchesIds
    if(typeof branches === 'object') {
      branchesIds = branches.map(brId => {
        return {id: brId};
      });
    }else {
      branchesIds = [{id: branches}]
    }
    // let branchesIds = branches.map(brId => {
    //   return {id: brId};
    // });

    let bankAccountObj;
    if (this.bankAccountType) {
      bankAccountObj = {
        'name':this.bankForm.get('name').value,
        'accountNumber': this.bankForm.get('accountNumber').value,
        'iban': this.bankForm.get('iban').value,
        'bankSwift': this.bankForm.get('bankSwift').value,
        'bankBranch': this.bankForm.get('bankBranch').value,
        'bank': this.bankForm.get('bank').value,
        'branchs': branchesIds,
        'bankAccountType': this.bankForm.get('bankAccountType').value,
        'user': this.bankForm.get('user').value,
        'currency': this.bankForm.get('currency').value,
        'payment': this.bankForm.get('payment').value,
        'collection': this.bankForm.get('collection').value,
        'payroll': this.bankForm.get('payroll').value,
        'enableBankReconcilation': this.bankForm.get('enableBankReconcilation').value,
        'customerId' : this.customerId
      };
    } else if (this.safeType || this.pettyCashType) {
      bankAccountObj = {
        'name':this.bankForm.get('name').value,
        'accountNumber': null,
        'iban': null,
        'bankSwift': null,
        'bankBranch': this.bankForm.get('bankBranch').value,
        'bank': this.bankForm.get('bank').value,
        'branchs': branchesIds,
        'bankAccountType': this.bankForm.get('bankAccountType').value,
        'user': this.bankForm.get('user').value,
        'currency': null,
        'payment': this.bankForm.get('payment').value,
        'collection': this.bankForm.get('collection').value,
        'payroll': this.bankForm.get('payroll').value,
        'enableBankReconcilation': this.bankForm.get('enableBankReconcilation').value,
        'customerId' : this.customerId
      };
    }

    if (this.data) {

      bankAccountObj.id = this.data?.account?.id;
      this._bankAccountService.updateBankAccount(bankAccountObj).subscribe((res) => {
          this._snack.showSnack(
            this._translate.instant('COMMON.EDIT_SUCCESS'),
            'success'
          );
          this.ItemDialogRef.close({status: 'success'});
        },
        (error => {
          console.log(error);
        })
      );
    } else {

      this._bankAccountService.addBankAccount(bankAccountObj).subscribe((res) => {
          this._snack.showSnack(
            this._translate.instant('COMMON.ADD_SUCCESS'),
            'success'
          );
          this.ItemDialogRef.close({status: 'success'});
        },
        (error => {
          console.log(error);
        })
      );
    }

  }
  get Validation() {
    return this.bankForm.controls;
  }
  public hasError = (controlName, errorName) => {
    return this.Validation[controlName].hasError(errorName);
  };

}
