import {forkJoin, Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {SearchRequest} from '@khdma-frontend-nx/shared';
import {BankAccountService} from '../services/bank-account.service';


@Injectable()
export class FinancialSetupsResolve implements Resolve<any[]> {
  constructor(private _service: BankAccountService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    let bankAccountTypes = JSON.parse(localStorage.getItem('CONFIG'))?.bankAccountTypes;
    let query = new SearchRequest();
    if (route.queryParams.searchCriteria) {
      query = new SearchRequest(JSON.parse(route.queryParams.searchCriteria),
        route.queryParams.pageNumber, route.queryParams.pageSize);
    }
    let Data = [[bankAccountTypes],
      this._service.getCustomerBranch(),
    ];
    Data = Data.filter(el => el != null);
    return forkJoin(Data);
  }
}
