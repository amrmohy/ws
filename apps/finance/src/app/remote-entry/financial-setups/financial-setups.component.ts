import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {DialogPosition, MatDialog} from '@angular/material/dialog';
import {AddEditBankAccountComponent} from './add-edit-bank-account/add-edit-bank-account.component';
import {BankAccountService} from '../services/bank-account.service';
import {Subscription} from 'rxjs';
import {SnackService,SearchRequest,SearchConfiguration} from '@khdma-frontend-nx/shared';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-financial-setups',
  templateUrl: './financial-setups.component.html',
  styleUrls: ['./financial-setups.component.scss']
})
export class FinancialSetupsComponent implements OnInit {
bankAccounts;
   searchData: {};
   page: SearchRequest;
   subscriptions = new Subscription();
   filteredAccounts: any;
  searchConfiguration: any = new SearchConfiguration();
  private initialSearchCriteria;
  constructor(private _bankAccountService: BankAccountService,
              public _translate: TranslateService,
              private locations: Location,
              private dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private _snack:SnackService) {
    this.subscriptions.add(activatedRoute.data.subscribe(res => {
    this.searchConfiguration = {
      fields: [
        {
          fieldType: 'input',
          type: 'text',
          name: 'iban',
          placeholder: 'BANK.IBAN',
        }, {
          fieldType: 'input',
          type: 'text',
          name: 'bankSwift',
          placeholder: 'BANK.SWIFT',
        }, {
          fieldType: 'input',
          type: 'text',
          name: 'bankName',
          placeholder: 'BANK.BANK_BRANCH_NAME',
        },
        {
          fieldType: 'select',
          name: 'accountTypeId',
          placeholder: 'BANK.BANK_ACCOUNT_TYPE',
          options: res.items[0],
          optionID: 'id',
          optionName: 'name'
        },
        {
          fieldType: 'select',
          name: 'branchId',
          placeholder: 'START_RECEIVING_INVENROTY.SELECT_BRANCH',
          options: res.items[1].data,
          optionID: 'id',
          optionName: 'branchName'
        }
      ]
    }
  }));
    this._bankAccountService.filter({
      "searchCriteria": {
        "name": null,
        "iban": null,
        "bankSwift": null,
        "bankBranch": null,
        "accountTypeId": null,
        "branchId": null//JSON.parse(localStorage.getItem('BRANCH_CONFIG')).id
      }
    }).subscribe(res => {
      this.bankAccounts = res.data.content
    })
  }
  ngOnInit(): void {
    // this.getBankAccounts()
  }

  back() {
    this.locations.back();
  }

  getByFilter(data) {
    this.searchData = {};
    this.page = new SearchRequest();
    this.getDatabySearch(data);
  }



  //get Data by Search
  getDatabySearch(data) {
    this.subscriptions.add(
      this._bankAccountService.filter({
        "searchCriteria": {
          'name': data.name,
          'iban': data.iban,
          'bankSwift': data.bankSwift,
          'bankBranch': data.bankName,
          'accountTypeId': data.accountTypeId,
          'branchId': data.branchId //? data.branchId : JSON.parse(localStorage.getItem('BRANCH_CONFIG')).id
        } }).subscribe(res => {
        this.bankAccounts = res.data.content
      })
    );
    console.log(data);

  }



  getBankAccounts() {
    this._bankAccountService.filter({
      "searchCriteria": {
        "name": null,
        "iban": null,
        "bankSwift": null,
        "bankBranch": null,
        "accountTypeId": null,
        "branchId": null //JSON.parse(localStorage.getItem('BRANCH_CONFIG')).id
      }
    }).subscribe((res) => {
      this.bankAccounts = res.data.content
    });

  }

  addBankAccountDialog() {
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(AddEditBankAccountComponent,{
        direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
        position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
        disableClose: true,
        height: '100vh',
        panelClass: 'custom-drawer'
      })
    dialogRef.afterClosed().subscribe((res) => {
      if (res?.status === "success") {
        this.getBankAccounts()
      }
      }
    );
  }
  editBankAccountDialog(account) {
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(AddEditBankAccountComponent,{
      data:{account},
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh',
      panelClass: 'custom-drawer'
    })
    dialogRef.afterClosed().subscribe((res) => {
      if (res?.status === "success") {
        this.getBankAccounts()
      }
      }
    );
  }

  removeAccount(id: string | undefined) {
    this._bankAccountService.removeBankAccount(id).subscribe((res)=>{
        this._snack.showSnack(
          this._translate.instant('COMMON.DELETE_SUCCESS'),
          'success'
        );
        this.getBankAccounts()
      },
      (error => {
        console.log(error);
      })
    )
  }

}
