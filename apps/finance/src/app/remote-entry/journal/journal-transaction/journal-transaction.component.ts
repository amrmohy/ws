import {Component, Inject, LOCALE_ID, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import { SnackService } from '@khdma-frontend-nx/shared';
import {DatePipe, Location} from '@angular/common';
import { Constant,AuthService,ConfirmDialogComponent } from '@khdma-frontend-nx/shared';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {DialogPosition, MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {JournalService} from '../../services/journal.service';
import {JournalDetailsFormComponent} from '../journal-details-form/journal-details-form.component';


@Component({
  selector: 'journal-transaction',
  templateUrl: './journal-transaction.html',
  styleUrls: ['./journal-transaction.scss'],
})
export class JournalTransactionComponent implements OnInit {
  config = JSON.parse(window.localStorage.getItem('CONFIG'));
  journal: any;
  branchId: number;
  totalCreditAmount: number;
  totalDebitAmount: number;
  journalCreation = false;
  journalEditable = false;
  maxDate = new Date();
  years = this.config?.fisicalYears;
  months = this.config?.months;
  currencies = this.config?.currencies;
  journalStatus = this.config?.journalStatus;
  journalSource = this.config?.journalSource;
  currentUser = JSON.parse(window.localStorage.getItem('CURRENT_USER'));
  branch: any;
  branches: any[] = [];
  source: any;
  // table
  private subscriptions = new Subscription();
  journalDetailsDataSource: MatTableDataSource<any>;
  journalDetailsArray: any = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = [
    'line',
    'account',
    'debit',
    'credit',
    'buttons'
  ];

  constructor(
    private activateRoute: ActivatedRoute,
    public _translate: TranslateService,
    private _snack: SnackService,
    private _router: Router,
    public constant: Constant,
    private datePipe: DatePipe,
    private locations: Location,
    public dialog: MatDialog,
    private journalService: JournalService,
    private authService: AuthService,
    @Inject(LOCALE_ID) public locale: string,
  ) {
    this.getUserBranches();
  }



  journalForm = new UntypedFormGroup({
    journalName: new UntypedFormControl(null, [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]),
    description: new UntypedFormControl(null, [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]),
    postingDate: new UntypedFormControl(new Date().toISOString().substring(0, 10), Validators.required),
    fisicalYear: new UntypedFormControl(null, Validators.required),
    month: new UntypedFormControl(null, Validators.required),
    branch: new UntypedFormControl(null, Validators.required),
    currency: new UntypedFormControl(null),
    journalSource: new UntypedFormControl(null),
  });

  ngOnInit(): void {
    const id = this.activateRoute.snapshot.paramMap.get('id');
    this.authService.config.subscribe((config) => {
      this.years = config?.fisicalYears;
      this.months = config?.months;
      this.currencies = config?.currencies;
      this.journalStatus = config?.journalStatus;
      this.journalSource = config?.journalSource;
      if (id === 'creation'){
        this.source = this.journalSource?.find((source) => source.code === this.constant.JOURNAL_SOURCE.MANUAL);
        if (this.source) {
          this.journalForm.patchValue({journalSource: this.source});
          this.journalForm.get('journalSource').disable();
        }
        this.defaultDate(this.journalForm.get('postingDate').value);
      }
    });


    if (id === 'creation') {
      this.defaultDate(this.journalForm.get('postingDate').value);
      this.source = this.journalSource?.find((source) => source.code === this.constant.JOURNAL_SOURCE.MANUAL);
      if (this.source) {
        this.journalForm.patchValue({journalSource: this.source});
        this.journalForm.get('journalSource').disable();
      }
      this.journalCreation = true;
    } else {
      this.getJournalDetailsById(+id);
      this.journalEditable = true;
    }

  }

  getUserBranches() {
    this.journalService.getCustomerBranch().subscribe(res => {
      this.branches = res.data;
      if (!this.journal) {
        this.branch = res.data.find(branch => branch.id === this.currentUser.branchId);
        this.journalForm.patchValue({branch: this.branch});
        this.journalForm.patchValue({currency: this.branch.currency});
        this.journalForm.get('currency').disable();
      }
    });
  }

  getJournalDetailsById(id: number) {
    this.journalService.getJournalById(id).subscribe((res) => {
        if (res.success) {
          this.journal = res.data;
        }
      }, (err) => console.log(err),
      () => {
        this.journalForm.patchValue(this.journal);
        if (this.journal.journalSource.code === this.constant.JOURNAL_SOURCE.MANUAL) {
          this.journalForm.get('journalSource').disable();
        }
        this.getJournalDetails(this.journal.journalDetails);
        this.disableForm();
      });
  }

  setJournalDetailsDataSource(source: any) {
    this.journalDetailsDataSource = new MatTableDataSource(source);
  }

  getJournalDetails(journalDetails) {
    if (journalDetails && journalDetails.length > 0) {
      this.journalDetailsArray = journalDetails;
      this.getTotalDebitCreaditValues(journalDetails);
      this.setJournalDetailsDataSource(journalDetails);
    } else {
      this.journalDetailsArray = [];
      this.setJournalDetailsDataSource([]);
    }
  }

  back() {
    this.locations.back();
  }

  disableForm() {
    if (this.journal?.journalStatus?.code === this.constant.JOURNAL_STATUS.CONFIRMED || this.journal?.journalStatus?.code === this.constant.JOURNAL_STATUS.REVERSED) {
      this.journalForm.disable();
    }
  }

  enableForm() {
    if (this.journal?.journalStatus?.code === this.constant.JOURNAL_STATUS.ENTERED) {
      this.journalForm.enable();
    }
  }

  save(journalForm, confirm?) {
    if (journalForm.valid && journalForm.value) {
      let journalObj = journalForm.getRawValue();
      if (this.journalEditable) {
        if (this.journal) {
          this.journal.journalName = journalObj.journalName;
          this.journal.postingDate = journalObj.postingDate;
          this.journal.fisicalYear = journalObj.fisicalYear;
          this.journal.month = journalObj.month;
          this.journal.branch = journalObj.branch;
          this.journal.currency = journalObj.currency;
          this.journal.description = journalObj.description;
          if (this.journalDetailsArray && this.journalDetailsArray.length > 0) {
            this.journal.journalDetails = this.journalDetailsArray;
          }

          if (confirm) {
            if (this.journal.journalDetails == null || this.journal.journalDetails?.length === 0) {
              this._snack.showSnack('COMMON.CREATE_AT_LEAST_LINE', 'error');
              return;
            } else {
              if (this.isJournalValid()) {
                this.journal.journalStatus = {
                  id: 2,
                  code: 'CONFIRMED'
                };
                this.updateJournal(this.journal);
              } else {
                this._snack.showSnack('JOURNAL.DEBIT_CREDIT_VALIDATE', 'error');
              }
            }
          } else {
            this.updateJournal(this.journal);
          }

          //
        }
      }
      // create
      if (this.journalCreation) {
        if (this.journalDetailsArray && this.journalDetailsArray?.length > 0) {
          journalObj.journalDetails = this.journalDetailsArray;
        }

        if (confirm) {
          if (this.journalDetailsArray && this.journalDetailsArray?.length > 0) {
            journalObj.journalDetails = this.journalDetailsArray;
            if (this.isJournalValid()) {
              journalObj.journalStatus = {
                id: 2,
                code: 'CONFIRMED'
              };
              this.createJournal(journalObj);
            } else {
              this._snack.showSnack('JOURNAL.DEBIT_CREDIT_VALIDATE', 'error');
            }
          } else {
            this._snack.showSnack('COMMON.CREATE_AT_LEAST_LINE', 'error');
            return;
          }

        } else {
          this.createJournal(journalObj);
        }
      }
    }
  }

  getTotalDebitCreaditValues(journalDetails) {
    let creditAmount = 0;
    let debitAmount = 0;
    if (journalDetails && journalDetails?.length > 0) {
      journalDetails.forEach((jorDet) => {
        creditAmount += jorDet.creditAmount;
        debitAmount += jorDet.debitAmount;
      });
    }
    this.totalCreditAmount = creditAmount;
    this.totalDebitAmount = debitAmount;
    console.log(this.totalCreditAmount === this.totalDebitAmount);
  }

  isJournalValid() {
    return this.totalCreditAmount === this.totalDebitAmount;
  }

  updateJournal(journal) {
    this.journalService.updateJournal(journal).subscribe((res) => {
      if (res.success) {
        console.log(res.data);
        this.journal = res.data;
        this.journalDetailsArray = this.journal.journalDetails;
        this.getTotalDebitCreaditValues(this.journal.journalDetails);
        this.setJournalDetailsDataSource(this.journal.journalDetails);
        this._snack.showSnack('COMMON.EDIT_SUCCESS', 'success');
        this.disableForm();
      }
    });
  }

  createJournal(journalObj) {
    this.journalService.createJournal(journalObj).subscribe((res) => {
      this.journal = res.data;
      this.journalDetailsArray = this.journal.journalDetails;
      this.getTotalDebitCreaditValues(this.journal.journalDetails);
      this.setJournalDetailsDataSource(this.journal.journalDetails);
      this.journalCreation = false;
      this.journalEditable = true;
      this.journalForm.markAsTouched();
      this.disableForm();
      this._snack.showSnack('COMMON.ADD_SUCCESS', 'success');
      this._router.navigateByUrl(`pages/finance/journal/${res.data.id}`);
    });
  }


  saveAndClose(journalForm) {
    this.save(journalForm)
    this._router.navigateByUrl(`pages/finance/journal`)

  }

  saveAndNew(journalForm: UntypedFormGroup) {
    this.save(journalForm)
    this.journalForm.reset()
    this._router.navigateByUrl(`pages/finance/journal/creation`)

  }


  public objectComparisonFunction = function(option, value): boolean {
    return option?.id === value?.id;
  };

  addLine() {
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(JournalDetailsFormComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: null,
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh',
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res?.data?.journal && res.data?.action === 'save') {
        let journal = res.data.journal;
        delete journal.amountType;
        this.journalDetailsArray.push(journal);
        console.log('journal',journal)
        console.log('this.journalDetailsArray',this.journalDetailsArray)
        this.setJournalDetailsDataSource(this.journalDetailsArray);
        this.getTotalDebitCreaditValues(this.journalDetailsArray);
        this.journalForm.markAsTouched();
      }
    });
  }

  editLine(line) {
    console.log(line);
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(JournalDetailsFormComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: line,
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh'
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res?.data?.journal && res.data?.action === 'save') {
        let journal = res.data.journal;
        line.branch = journal.branch;
        line.accountType = journal.accountType;
        line.branchAccount = journal.branchAccount;
        line.service = journal.service;
        line.serviceCategory = journal.serviceCategory;
        line.debitAmount = journal.debitAmount;
        line.creditAmount = journal.creditAmount;
        line.country = journal.country;
        delete journal.amountType;
        this.getTotalDebitCreaditValues(this.journalDetailsArray);
        this.journalForm.markAsTouched();
      }
    });
  }

  deleteLine(line: any) {
    console.log(this.journalDetailsArray);
    const dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        if (line.hasOwnProperty('id')){
          this.journalDetailsArray = this.journalDetailsArray.filter((element) => element.id != line.id);
          this.setJournalDetailsDataSource(this.journalDetailsArray);
          this.journalForm.markAsTouched();
        }else {
          this.journalDetailsArray = this.journalDetailsArray.filter((value) => value !== line);
          this.setJournalDetailsDataSource(this.journalDetailsArray);
          this.journalForm.markAsTouched();
        }
      }
    });
  }

  reverseJournal() {
    this.journalService.reverseJournal(this.journal.id).subscribe((res) => {
      if (res.success) {
        this.journal = res.data;
        this.enableForm();
        this._snack.showSnack('COMMON.REVERSE_SUCCESS', 'success');
        this._router.navigateByUrl(`pages/finance/journal/${res.data.id}`);
      }
    });
  }

  branchChanged(branch) {
    this.journalForm.patchValue({currency: branch.currency});
    this.journalForm.get('currency').disable();
  }

  dateOnChange(value: any) {
    this.defaultDate(value);
  }

  defaultDate(value) {
    let newDate = new Date(value);
    let month = this.months?.find((m) => m.code == newDate.getMonth() + 1);
    let year = this.years?.find((m) => m.code == newDate.getFullYear());
    this.journalForm.patchValue({month: month, fisicalYear: year});
  }
}
