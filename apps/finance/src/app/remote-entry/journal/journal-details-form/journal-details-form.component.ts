import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {JournalService} from '../../services/journal.service';
import {SnackService , AuthService,Constant} from '@khdma-frontend-nx/shared';

@Component({
  selector: 'journal-details-form',
  templateUrl: './journal-details-form.component.html',
  styleUrls: ['./journal-details-form.component.scss']
})
export class JournalDetailsFormComponent implements OnInit {

  customer = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'));
  config = JSON.parse(window.localStorage.getItem('CONFIG'));
  accountTypes = this.config?.accountTypes;
  customers: any[] = [];
  branches: any[] = [];
  services: any[] = [];
  servicesCategories: any[] = [];
  branchAccounts: any[] = [];
  countries: any[] = [];

  currentUser = JSON.parse(window.localStorage.getItem('CURRENT_USER'));
  selectedValue;
  branchId: number;
  branch: any;
  credit = this.constant.JOURNAL_AMOUNT_TYPE.CREADIT;
  debit = this.constant.JOURNAL_AMOUNT_TYPE.DEBIT;

  constructor(
    private journalDialogRef: MatDialogRef<JournalDetailsFormComponent>,
    private constant: Constant,
    private _snack: SnackService,
    private journalService: JournalService,
    @Inject(MAT_DIALOG_DATA) public journalDetailsData: any,
    private authService: AuthService
  ) {
    this.getCustomerBranch();
  }


  ngOnInit(): void {
    if (this.journalDetailsData) {
      console.log(this.journalDetailsData)
      this.journalDetailsForm.patchValue({
        branchAccountId: this.journalDetailsData?.branchAccount
      });
      this.journalDetailsForm.patchValue(this.journalDetailsData);
      this.branchId = this.journalDetailsData?.branch?.id;
      this.branchChanged(this.journalDetailsData?.branch);
      this.accountTypeChanged(this.journalDetailsData?.accountType);
      if (this.journalDetailsData.creditAmount) {
        this.selectedValue = this.credit;
        this.journalDetailsForm.patchValue({amountType: this.credit});
      }
      if (this.journalDetailsData.debitAmount) {
        this.selectedValue = this.debit;
        this.journalDetailsForm.patchValue({amountType: this.debit});
      }
    }
    // console.log(this.customer);
    if (this.customer) {
      this.journalDetailsForm.patchValue(
        {customer: {id: this.customer.customerId, branchName: this.customer.branchName}});
      this.journalDetailsForm.get('customer').disable();
      this.getCustomerCountry(this.customer.customerId);
    }
    this.authService.config.subscribe((config)=>{
      this.accountTypes = config?.accountTypes;
    })
  }

  journalDetailsForm = new UntypedFormGroup({
    customer: new UntypedFormGroup({id: new UntypedFormControl(''), branchName: new UntypedFormControl('')}),
    country: new UntypedFormControl(null, Validators.required),
    accountType: new UntypedFormControl(null, Validators.required),
    branchAccount: new UntypedFormControl(null),
    branchAccountId: new UntypedFormControl(null, Validators.required),
    branch: new UntypedFormControl(null, Validators.required),
    service: new UntypedFormControl(null, Validators.required),
    serviceCategory: new UntypedFormControl(null, Validators.required),
    creditAmount: new UntypedFormControl(null),
    debitAmount: new UntypedFormControl(null),
    amountType: new UntypedFormControl(null, Validators.required),
  });


  get validation() {
    return this.journalDetailsForm.controls;
  }

  public hasError = (controlName, errorName) => {
    return this.validation[controlName].hasError(errorName);
  };

  public objectComparisonFunction = function(option, value): boolean {
    return option?.id === value?.id;
  };

  //on item submit
  onSubmit() {
    let journal = this.journalDetailsForm.getRawValue();
    delete journal.branchAccountId;
    if (journal.creditAmount === null && journal.debitAmount === null) {
      this._snack.showSnack('JOURNAL.DEBIT_CREDIT','error')
    } else {
      this.journalDialogRef.close({
        data: {
          action: 'save',
          journal: journal
        },
      });
    }

  }

  //on item Cancel
  onCancel() {
    this.journalDialogRef.close({
      data: {
        action: 'cancel',
        journal: this.journalDetailsForm.value
      },
    });
  }


  getCustomerBranch() {
    this.journalService.getCustomerBranch().subscribe((res) => {
      if (res.success) {
        this.branches = res.data;
        if (!this.journalDetailsData){
          this.branch = res.data.find(branch => branch.id === this.currentUser.branchId);
          this.journalDetailsForm.patchValue({branch:this.branch});
          this.branchChanged(this.branch);
          this.journalDetailsForm.patchValue({accountType:this.accountTypes[0]});
          this.accountTypeChanged(this.accountTypes[0])
        }

      }
    });
  }

  branchChanged(branch, event?) {
    this.branchId = branch.id;
    if (event) {
      this.journalDetailsForm.patchValue({accountType: null, branchAccount: null, service: null, serviceCategory: null});

    }

    this.getServices(branch.id,event);
    this.getServiceCategories(branch.id,event);
    if (!this.journalDetailsData || event){
      this.journalDetailsForm.patchValue({accountType:this.accountTypes[0]});
      this.accountTypeChanged(this.accountTypes[0],event)
    }
  }

  getServices(branchId,event) {
    this.journalService.getSericesByBranch(branchId).subscribe((res) => {
      if (res.success) {
        this.services = res.data;
        if (!this.journalDetailsData || event){
          this.journalDetailsForm.patchValue({service:res.data[0]})
        }
      }
    });
  }

  getServiceCategories(branchId,event) {
    this.journalService.getServiceCategoriesByBranch(branchId).subscribe((res) => {
      if (res.success) {
        this.servicesCategories = res.data;
        if (!this.journalDetailsData || event){
          this.journalDetailsForm.patchValue({serviceCategory:res.data[0]})
        }
      }
    });
  }

  accountTypeChanged(accountType,event?) {
    this.getbranchAccounts(accountType.id,event);
  }

  getbranchAccounts(accountTypeId,event?) {
    if (this.branchId) {
      this.journalService.getbranchAccounts(accountTypeId, this.branchId).subscribe((res) => {
        if (res.success) {
          this.branchAccounts = res.data;
          if (!this.journalDetailsData || event){
            this.journalDetailsForm.patchValue({branchAccount:res.data[0]})
          }
        }
      });
    }
  }

  changeJournalAmountType(value: any) {
    this.selectedValue = value;
    if (value === this.debit) {
      this.journalDetailsForm.patchValue({creditAmount: null});
      if (this.journalDetailsData) {
        this.journalDetailsForm.patchValue({debitAmount: this.journalDetailsData?.debitAmount});
      }
    }
    if (value === this.credit) {
      this.journalDetailsForm.patchValue({debitAmount: null});
      if (this.journalDetailsData) {
        this.journalDetailsForm.patchValue({creditAmount: this.journalDetailsData?.creditAmount});
      }
    }
  }

  getCustomerCountry(customerId) {
    if(customerId){
      this.journalService.getCustomerCountry(customerId).subscribe((res) => {
        if (res.success) {
          this.countries = res.data;
          if (res.data.length === 1) {
            this.journalDetailsForm.patchValue({country: res.data[0]});
            this.journalDetailsForm.get('country').disable();
          }
        }
      });
    }
  }
  getAccount(obj){
    this.journalDetailsForm.patchValue({
      branchAccount: obj.value
    });
  }
}
