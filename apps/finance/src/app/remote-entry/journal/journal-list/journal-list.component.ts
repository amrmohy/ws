import {Component, OnInit} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {
  AuthService,
  ConfirmDialogComponent,
  Constant,
  QueryParamsHelperService,
  SearchConfiguration,
  SearchRequest,
  ViewService
} from '@khdma-frontend-nx/shared';
import {JournalService} from '../../services/journal.service';
import {formatDate} from '@angular/common';
import {MatDialog} from "@angular/material/dialog";


@Component({
  selector: 'journal-list',
  templateUrl: './journal-list.component.html',
  styleUrls: ['./journal-list.component.scss'],
  providers: [Constant]
})
export class JournalListComponent implements OnInit {
  private subscriptions = new Subscription();
  searchConfiguration: any = new SearchConfiguration();
  dataSource: MatTableDataSource<any>;
  page: SearchRequest = new SearchRequest();
  listOfJournals: any[] = [];
  config = JSON.parse(window.localStorage.getItem('CONFIG'));
  journalStatus = this.config?.journalStatus;
  searchData: any = {};
  initialSearchCriteria;

  constructor(
    private journalService: JournalService,
    public constant: Constant,
    private _router: Router,
    public _translate: TranslateService,
    public _view: ViewService,
    private activatedRoute: ActivatedRoute,
    private paramsHelper: QueryParamsHelperService,
    private authService: AuthService,
    public dialog: MatDialog,
  ) {

    this.subscriptions.add(activatedRoute.data.subscribe(res => {
        this.setJournalStatus();
        this.searchConfiguration = {
          fields: [
            {
              fieldType: 'input',
              type: 'text',
              name: 'name',
              placeholder: 'JOURNAL.JOURNAL_NAME',
            },
            {
              fieldType: 'input',
              type: 'text',
              name: 'journalNumber',
              placeholder: 'JOURNAL.JOURNAL_NUMBER',
            },
            {
              fieldType: 'date',
              type: 'date',
              name: 'postingDate',
              placeholder: 'JOURNAL.POSTING_DATE',
            },
            {
              fieldType: 'date',
              type: 'input',
              name: 'fromDate',
              placeholder: 'FILTER.FROM_DATE',
            },
            {
              fieldType: 'date',
              type: 'input',
              name: 'toDate',
              placeholder: 'FILTER.TO_DATE',
            },
            {
              fieldType: 'select',
              name: 'branchId',
              placeholder: 'START_RECEIVING_INVENROTY.SELECT_BRANCH',
              options: res.journals[1].data,
              optionID: 'id',
              optionName: 'branchName'
            },
            {
              fieldType: 'select',
              name: 'journalStatusId',
              placeholder: 'JOURNAL.SELECT_JOURNAL_STATUS',
              options: this.journalStatus,
              optionID: 'id',
              optionName: 'name'
            }
          ]
        };
        this.tableDataSource(res.journals[0]);
      }
    ));


    document.addEventListener('item_number_barcode', function(e) {
      const req = {'journalNumber': e['detail']};
      this.getByFilter(req);

    }.bind(this));

    if (this.activatedRoute.snapshot.queryParams.searchCriteria) {
      this.initialSearchCriteria = JSON.parse(this.activatedRoute.snapshot.queryParams.searchCriteria);
    } else {
      this.initialSearchCriteria = {
        name: null,
        journalNumber: null,
        postingDate: null,
        fromDate: null,
        toDate: null,
        branchId: null,
        journalStatusId: null,
      };
    }

  }

  ngOnInit(): void {
    this._view.setView();
  }

  setJournalStatus() {
    this.authService.config.subscribe((config) => {
      console.log(config);
      this.journalStatus = config?.journalStatus;
    });
  }

  //table data source
  tableDataSource(source: any) {
    this.dataSource = new MatTableDataSource(source.data.content);
    this.listOfJournals = source.data.content;
    this.page.pageNumber = source.data.pageable.pageNumber;
    this.page.pageSize = source.data.pageable.pageSize;
    this.page.totalElements = source.data.totalElements;
  }

  getByFilter(data) {
    this.searchData = {};
    this.page = new SearchRequest();
    this.getDatabySearch(data);
  }

  //get Data by Search
  getDatabySearch(data) {
    if (data.postingDate) {
      data.postingDate = formatDate(data.postingDate, this.constant.DATE_FORMAT, 'en');
    }
    if (data.fromDate) {
      data.fromDate = formatDate(data.fromDate, this.constant.DATE_FORMAT, 'en');
    }

    if (data.toDate) {
      data.toDate = formatDate(data.toDate, this.constant.DATE_FORMAT, 'en');
    }
    if (data) {
      this.initialSearchCriteria = data;
    }
    // const query = new SearchRequest(data, this.page.pageNumber, this.page.pageSize);
    this.page = new SearchRequest(data);
    this.paramsHelper.changeUrlParams(this.page);

    this.subscriptions.add(
      this.journalService.filter(this.page).subscribe(res => {
        this.tableDataSource(res);
      })
    );

  }


  public onPageChange(criteria: any, event?: PageEvent) {
    this.page = new SearchRequest(this.initialSearchCriteria, event.pageIndex, this.page.pageSize);
    this.paramsHelper.changeUrlParams(this.page);
    this.subscriptions.add(
      this.journalService.filter(this.page).subscribe(res => {
        this.listOfJournals = res.data.content;
        this.page.totalElements = res.data.totalElements;

      })
    );
    return event;
  }

  addNew() {
    this._router.navigate(['/finance/journal/creation']);
  }

  deleteJournal(journalId) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.journalService.deleteJournal(journalId).subscribe(res => {
          if (res.body.success) {
            this.listOfJournals = this.listOfJournals.filter((inv) => inv.id != journalId);
            this.page.totalElements -= 1;
          }
        });
      }
    });
  }
}
