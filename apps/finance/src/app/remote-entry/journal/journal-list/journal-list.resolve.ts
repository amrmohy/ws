import {forkJoin, Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {SearchRequest} from '@khdma-frontend-nx/shared';
import {JournalService} from '../../services/journal.service';

@Injectable()
export class JournalListResolve implements Resolve<any[]> {
  constructor(private _service: JournalService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    let query = new SearchRequest();
    if (route.queryParams.searchCriteria) {
      query = new SearchRequest(JSON.parse(route.queryParams.searchCriteria),
        route.queryParams.pageNumber, route.queryParams.pageSize);
    }
    let Data = [
      this._service.filter(query),
      this._service.getCustomerBranch()];
    return forkJoin(Data);
  }
}
