import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

export interface Cards {
  urlNavigate: string;
  title: string;
  icon: string;
  details: string;
  color: string;
}

@Component({
  selector: 'finance-setting',
  templateUrl: './finance-setting.component.html',
  styleUrls: ['./finance-setting.component.scss'],
})
export class FinanceSettingComponent implements OnInit {
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  cards: Cards[] = [
    {
      urlNavigate: '/finance/finance-setups',
      title: 'SETUP_HOME.BANK_ACCOUNTS',
      icon: 'account_balance',
      details: 'SETUP_HOME.INV_TRANS.BANK_ACCOUNTS_DESCRIPTION',
      color: '#401DA1'
    },
    {
      urlNavigate: '/finance/branch-accounts',
      title: 'SETUP_HOME.INV_TRANS.BRANCH_ACCOUNTS',
      icon: 'account_tree',
      details: 'SETUP_HOME.INV_TRANS.BRANCH_ACCOUNTS_DETAILS',
      color: '#5b84e8'
    }
  ];


  constructor(
    private _router: Router,
    public _translate: TranslateService
  ) {
  }

  ngOnInit(): void {
  }

  /**
   * navigate to  customer profile
   */
  navigateToAccount() {
    const currentUser = JSON.parse(window.localStorage.getItem('CURRENT_USER'));
    this._router.navigateByUrl(
      `pages/user-management/user-profile`
    );
  }

}
