import {Component, OnInit} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {ConfirmDialogComponent, Constant, QueryParamsHelperService, SearchConfiguration, SearchRequest, ViewService } from '@khdma-frontend-nx/shared';
import {PayableInvoiceService} from '../services/payable-invoice.service';
import {formatDate} from '@angular/common';
import {DialogPosition, MatDialog} from '@angular/material/dialog';
import {UnpostedTransactionsDialogComponent} from './unposted-transactions-dialog/unposted-transactions-dialog.component';


@Component({
  selector: 'payable-invoice-list',
  templateUrl: './payable-invoice-list.component.html',
  styleUrls: ['./payable-invoice-list.component.scss'],
  providers: [Constant]
})
export class PayableInvoiceListComponent implements OnInit {
  private subscriptions = new Subscription();
  searchConfiguration: any = new SearchConfiguration();
  dataSource: MatTableDataSource<any>;
  page: SearchRequest = new SearchRequest();
  listOfInvoices: any[] = [];
  payableDraftCount = 0;
  branchConfigId = JSON.parse(localStorage.getItem('BRANCH_CONFIG')).id;
  searchData: any = {};
  initialSearchCriteria;

  constructor(
    private payableInvoiceService: PayableInvoiceService,
    public constant: Constant,
    private _router: Router,
    public _translate: TranslateService,
    public _view: ViewService,
    private activatedRoute: ActivatedRoute,
    private paramsHelper: QueryParamsHelperService,
    public dialog: MatDialog,
  ) {

    this.subscriptions.add(activatedRoute.data.subscribe(res => {
        this.searchConfiguration = {
          fields: [
            {
              fieldType: 'input',
              type: 'text',
              name: 'invNumber',
              placeholder: 'PAYABLE_INVOICE.INVOICE_NUMBER',
            },
            {
              fieldType: 'input',
              type: 'text',
              name: 'itemName',
              placeholder: 'RECIVING_ORDER_VIEW.ITEM_NAME',
            },
            {
              fieldType: 'input',
              type: 'text',
              name: 'itemNumber',
              placeholder: 'RECIVING_ORDER_VIEW.ITEM_NUMBER',
            },
            {
              fieldType: 'date',
              type: 'date',
              name: 'invDate',
              placeholder: 'PAYABLE_INVOICE.SEARCH.SELECT_INVOICE_DATE',
            },
            {
              fieldType: 'date',
              type: 'input',
              name: 'fromDate',
              placeholder: 'FILTER.FROM_DATE',
            },
            {
              fieldType: 'date',
              type: 'input',
              name: 'toDate',
              placeholder: 'FILTER.TO_DATE',
            },
            {
              fieldType: 'select',
              name: 'branchId',
              placeholder: 'START_RECEIVING_INVENROTY.SELECT_BRANCH',
              options: res.items[1].data,
              optionID: 'id',
              optionName: 'branchName'
            }, {
              fieldType: 'select',
              name: 'invoiceStatusId',
              placeholder: 'PAYABLE_INVOICE.SEARCH.SELECT_INVOICE_STATUS',
              options: res.items[2].data,
              optionID: 'id',
              optionName: 'name'
            }
            ,
            {
              fieldType: 'select',
              name: 'supplierId',
              placeholder: 'START_RECEIVING_INVENROTY.SELECT_SUPPLIER',
              options: res?.items[3]?.data,
              optionID: 'id',
              optionName: 'fullName'
            }
          ]
        };
        this.tableDataSource(res.items[0]);
      }
    ));


    document.addEventListener('item_number_barcode', function (e) {
      const req = {'itemNumber': e['detail']};
      this.getByFilter(req);

    }.bind(this));

    if (this.activatedRoute.snapshot.queryParams.searchCriteria) {
      this.initialSearchCriteria = JSON.parse(this.activatedRoute.snapshot.queryParams.searchCriteria);
    } else {
      this.initialSearchCriteria = {
        name: null,
        invNumber: null,
        itemName: null,
        itemNumber: null,
        invDate: null,
        fromDate: null,
        toDate: null,
        branchId: null,
        invoiceStatusId: null,
        supplierId: null
      };
    }

  }



  ngOnInit(): void {
    this.payableInvDraftCount(this.branchConfigId)
    this._view.setView();
  }

  //table data source
  tableDataSource(source: any) {
    this.dataSource = new MatTableDataSource(source.data.content);
    this.listOfInvoices = source.data.content;
    this.page.pageNumber = source.data.pageable.pageNumber;
    this.page.pageSize = source.data.pageable.pageSize;
    this.page.totalElements = source.data.totalElements;
  }

  getByFilter(data) {
    this.searchData = {};
    this.page = new SearchRequest();
    this.getDatabySearch(data);
  }

  //get Data by Search
  getDatabySearch(data) {
    if (data.invDate) {
      data.invDate = formatDate(data.invDate, this.constant.DATE_FORMAT, 'en');
    }
    if (data.fromDate) {
      data.fromDate = formatDate(data.fromDate, this.constant.DATE_FORMAT, 'en');
    }

    if (data.toDate) {
      data.toDate = formatDate(data.toDate, this.constant.DATE_FORMAT, 'en');
    }
    if (data) {
      this.initialSearchCriteria = data;
    }
    // const query = new SearchRequest(data, this.page.pageNumber, this.page.pageSize);
    this.page = new SearchRequest(data);
    this.paramsHelper.changeUrlParams(this.page);

    this.subscriptions.add(
      this.payableInvoiceService.filter(this.page).subscribe(res => {
        this.tableDataSource(res);
      })
    );

  }


  public onPageChange(criteria: any, event?: PageEvent) {
    this.page = new SearchRequest(this.initialSearchCriteria, event.pageIndex, this.page.pageSize);
    this.paramsHelper.changeUrlParams(this.page);
    this.subscriptions.add(
      this.payableInvoiceService.filter(this.page).subscribe(res => {
        this.listOfInvoices = res.data.content;
        this.page.totalElements = res.data.totalElements;

      })
    );
    return event;
  }

  addNew() {
    this._router.navigate(['finance/payable-invoice/creation']);
  }

  deleteInvoice(invoiceId) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.payableInvoiceService.deletePayableInvoice(invoiceId).subscribe(res => {
          if (res.body.success) {
            this.listOfInvoices = this.listOfInvoices.filter((inv) => inv.id != invoiceId);
            this.page.totalElements -= 1;
          }
        });
      }
    });

  }

  payableInvDraftCount(branchIds){
    this.payableInvoiceService.payableInvDraftCount(branchIds).subscribe(res => {
     if(res.success){
       this.payableDraftCount =  res.data.elementCount;
     }
    });
  }

  openDraftPayableInvoices(){
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(UnpostedTransactionsDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: this.payableDraftCount,
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh'
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.data?.item && res.data?.action === 'save') {
        this.payableInvDraftCount(this.branchConfigId)
      }
    });

  }
}
