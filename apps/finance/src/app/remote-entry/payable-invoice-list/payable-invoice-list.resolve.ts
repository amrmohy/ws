import {forkJoin, Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {PayableInvoiceService} from '../services/payable-invoice.service';
import { SearchRequest } from '@khdma-frontend-nx/shared';

@Injectable()
export class PayableInvoiceListResolve implements Resolve<any[]> {
  constructor(private _service: PayableInvoiceService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    let branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
    let query = new SearchRequest();
    if (route.queryParams.searchCriteria) {
      query = new SearchRequest(JSON.parse(route.queryParams.searchCriteria),
        route.queryParams.pageNumber, route.queryParams.pageSize);
    }
    let Data = [
      this._service.filter(query),
      this._service.getCustomerBranch(),
      this._service.getPayableInvoiceStatus(),
      branchConfig.customerId  ? this._service.getSuppliers(branchConfig.customerId) : null
    ];
    Data = Data.filter(el => el != null);
    return forkJoin(Data);
  }
}
