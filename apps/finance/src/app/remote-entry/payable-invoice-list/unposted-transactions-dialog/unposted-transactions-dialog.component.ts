import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {SnackService , AuthService} from '@khdma-frontend-nx/shared';

import {TranslateService} from '@ngx-translate/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {PayableInvoiceService} from '../../services/payable-invoice.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'unposted-transactions-dialog',
  templateUrl: './unposted-transactions-dialog.component.html',
  styleUrls: ['./unposted-transactions-dialog.component.scss']
})
export class UnpostedTransactionsDialogComponent implements OnInit ,OnDestroy {
  customerId = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'))?.customerId;
  CONFIG: any = JSON.parse(window.localStorage.getItem('CONFIG'));
  private subscriptions = new Subscription();
  unpostedDataSource: MatTableDataSource<any>;
  unpostedArray: any[] = [];
  branches: any[] = [];
  years: any[] = [];
  months: any[] = [];
  isEmpty: boolean = false;
  hasView: boolean = false;
  payableDraftCount = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  unpostedDisplayedColumns = [
    'invNumber',
    'createdDate',
    'paidAmount',
    'buttons'
  ];

  constructor(
    private  unpostedDialogRef: MatDialogRef<UnpostedTransactionsDialogComponent>,
    private _snack: SnackService,
    public _translate: TranslateService,
    private router: Router,
    private payableInvoiceService: PayableInvoiceService,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public count: any
  ) {
    this.setConfig();
    this.getUserBranches();
  }


  ngOnInit(): void {

  }

  item = new UntypedFormGroup({
    branch: new UntypedFormControl(null, Validators.required),
    month: new UntypedFormControl(null),
    fisicalYear: new UntypedFormControl(null),
  });

  submitInvDraftCount() {
    let item = this.item.getRawValue();
    this.payableInvDraftCount(item.branch, item.month, item.fisicalYear);
    this.payableInvDraftDetails(item);
  }
  payableInvDraftDetails(item){
    this.payableInvoiceService.payableInvDraftDetails(item.branch, item.month, item.fisicalYear)
      .subscribe((res) => {
        if (res.success) {
          if (res.data && res.data?.length) {
            this.unpostedArray = res.data;
          }
        }
      });
  }
  viewDraftDetails() {
    let item = this.item.value;
    this.payableInvoiceService.payableInvDraftDetails(item.branch, item.month, item.fisicalYear).subscribe((res) => {
      if (res.success) {
        if (res.data && res.data?.length) {
          this.setUnpostedDataSource(res.data);
          this.isEmpty = false;
          this.hasView = true;
        } else {
          this.isEmpty = true;
          this.hasView = false;
          this.setUnpostedDataSource([]);
        }
      }

    });
  }
  //on item Cancel
  onCancel() {
    this.unpostedDialogRef.close({
      data: {
        action: 'cancel',
        item: null
      },
    });
  }

  setUnpostedDataSource(source: any) {
    this.unpostedArray = source;
    this.unpostedDataSource = new MatTableDataSource(source);
  }

  viewInvoice(invoice) {
    this.onCancel();
    this.router.navigate(['/finance/payable-invoice', invoice?.id]);
  }

  getUserBranches() {
    this.payableInvoiceService.getCustomerBranch().subscribe(res => {
      this.branches = res.data;
    });
  }

  setConfig() {
    if (this.CONFIG) {
      this.years = this.CONFIG.fisicalYears;
      this.months = this.CONFIG.months;
    }
    this.subscriptions.add(
      this.authService.config.subscribe((config) => {
        if (config) {
          this.years  = config.fisicalYears;
          this.months = config.months;
        }
      })
    );
  }

  onPost() {

    let item = this.item.getRawValue();

    if (item.fisicalYear){
      item.fisicalYear = item.fisicalYear.map(id => {
        return Number(id)
      });
    }

    if ( item.month){
      item.month = item.month.map(id => {
       return Number(id)
      });
    }

    this.postBulk(item);

  }

  postBulk(item){
    this.payableInvoiceService.postBulk(item.branch,item.month,item.fisicalYear).subscribe(res => {
      if (res.success){
        console.log(res);
        this._snack.showSnack('COMMON.POST_SUCCESS', 'success');
        this.unpostedDialogRef.close({
          data: {
            action: 'save',
            item: item
          },
        });
      }
    });
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
  payableInvDraftCount(branchIds, months, years){
    this.payableInvoiceService.payableInvDraftCount(branchIds, months, years).subscribe(res => {
      if(res.success){
        this.payableDraftCount =  res.data.elementCount;
      }
    });
  }
  backToSearch(){
    this.hasView = false;
    this.setUnpostedDataSource([]);
  }
}
