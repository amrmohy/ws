import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RemoteEntryComponent} from "./entry.component";
import {AuthGuard} from "@khdma-frontend-nx/shared";
import {PayableInvoiceListComponent} from './payable-invoice-list/payable-invoice-list.component';
import {PayableInvoiceListResolve} from './payable-invoice-list/payable-invoice-list.resolve';
import {PayableInvoiceTransactionComponent} from './payable-invoice-transaction/payable-invoice-transaction.component';
import {FinancialSetupsComponent} from './financial-setups/financial-setups.component';
import {AddEditBankAccountComponent} from './financial-setups/add-edit-bank-account/add-edit-bank-account.component';
import {FinancialSetupsResolve} from './financial-setups/financial-setups.resolve';
import {JournalListComponent} from './journal/journal-list/journal-list.component';
import {JournalListResolve} from './journal/journal-list/journal-list.resolve';
import {JournalTransactionComponent} from './journal/journal-transaction/journal-transaction.component';
import {BranchAccountsDrawerComponent} from './branch-accounts/branch-accounts-drawer/branch-accounts-drawer.component';
import {FinanceSettingComponent} from './finance-setting/finance-setting.component';
import {FinanceTransactionComponent} from './finance-transaction/finance-transaction.component';
import {BranchAccountsResolve} from './branch-accounts/branch-accounts.resolve';
import {CashTransferListComponent} from './cash-transfer/cash-transfer-list/cash-transfer-list.component';
import {CashTransferListResolve} from './cash-transfer/cash-transfer-list/cash-transfer-list.resolve';
import {
  CashTransferTransactionComponent
} from './cash-transfer/cash-transfer-transaction/cash-transfer-transaction.component';
import {ChartOfAccountComponent} from './chart-of-account/chart-of-account.component';
const routes: Routes = [
  {
    path: '',
    component: RemoteEntryComponent,
    children: [
      {
        path: 'payable-invoice',
        component: PayableInvoiceListComponent,
        resolve: {
          items: PayableInvoiceListResolve
        }
      },
      {
        path: 'payable-invoice/:id',
        component: PayableInvoiceTransactionComponent
      },
      {
        path: 'cash-transfer',
        component: CashTransferListComponent,
        resolve: {
          items: CashTransferListResolve
        }
      },
      {
        path: 'cash-transfer/:id',
        component: CashTransferTransactionComponent
      },
      {
        path: 'chart-of-account',
        component: ChartOfAccountComponent
      },
      {
        path: 'branch-accounts',
        component: BranchAccountsDrawerComponent,
        resolve: {
          items: BranchAccountsResolve
        },
      },
      {
        path: 'finance-setups',
        component: FinancialSetupsComponent,
        resolve: {
          items: FinancialSetupsResolve
        },
      },
      {
        path: 'add-edit-bank-account',
        component: AddEditBankAccountComponent
      },
      {
        path: 'journal',
        component: JournalListComponent,
        resolve: {
          journals: JournalListResolve
        }
      },
      {
        path: 'journal/:id',
        component: JournalTransactionComponent
      },
      {
        path: 'finance-setting',
        component: FinanceSettingComponent
      }
      , {
        path: 'finance-transaction',
        component: FinanceTransactionComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntryRoutingModule {
}
