import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RemoteEntryComponent } from './entry.component';
import {SharedModule, SnackService} from "@khdma-frontend-nx/shared";
import {
  UnpostedTransactionsDialogComponent
} from "./payable-invoice-list/unposted-transactions-dialog/unposted-transactions-dialog.component";
import {
  SearchAutoCompleteBranchAccount
} from "./payable-invoice-transaction/add-expense-dialog/search-autocomplete-branch-account/search-autocomplete-branch-account.component";
import {
  CashTransferTransactionComponent
} from "./cash-transfer/cash-transfer-transaction/cash-transfer-transaction.component";
import {CashTransferListComponent} from "./cash-transfer/cash-transfer-list/cash-transfer-list.component";
import {ConfirmationDialogComponent} from "./branch-accounts/confirmation-dialog/confirmation-dialog.component";
import {
  AccountAnalysisDialogComponent
} from "./branch-accounts/account-analysis-dialog/account-analysis-dialog.component";
import {FinanceTransactionComponent} from "./finance-transaction/finance-transaction.component";
import {
  AddSettlementDialogComponent
} from "./payable-invoice-transaction/add-settlement-dialog/add-settlement-dialog.component";
import {DraftAccountsDialogComponent} from "./chart-of-account/draft-accounts-dialog/draft-accounts-dialog.component";
import {FinanceSettingComponent} from "./finance-setting/finance-setting.component";
import {JournalDetailsFormComponent} from "./journal/journal-details-form/journal-details-form.component";
import {JournalListComponent} from "./journal/journal-list/journal-list.component";
import {BranchAccountsDialogComponent} from "./branch-accounts/branch-accounts-dialog/branch-accounts-dialog.component";
import {BranchAccountsDrawerComponent} from "./branch-accounts/branch-accounts-drawer/branch-accounts-drawer.component";
import {
  PayInstalmentDialogComponent
} from "./payable-invoice-transaction/pay-instalment-dialog/pay-instalment-dialog.component";
import {JournalTransactionComponent} from "./journal/journal-transaction/journal-transaction.component";
import {BranchAccountsComponent} from "./branch-accounts/branch-accounts.component";
import {PayableInvoiceListComponent} from "./payable-invoice-list/payable-invoice-list.component";
import {PayableInvoiceTransactionComponent} from "./payable-invoice-transaction/payable-invoice-transaction.component";
import {ChartOfAccountComponent} from "./chart-of-account/chart-of-account.component";
import {ChartDialogComponent} from "./chart-of-account/chart-dialog/chart-dialog.component";
import {SearchAutoComplete} from "./payable-invoice-transaction/search-autocomplete/search-autocomplete.component";
import {FinancialSetupsComponent} from "./financial-setups/financial-setups.component";
import {AddEditBankAccountComponent} from "./financial-setups/add-edit-bank-account/add-edit-bank-account.component";
import {AddExpenseDialogComponent} from "./payable-invoice-transaction/add-expense-dialog/add-expense-dialog.component";
import {PayDialogComponent} from "./payable-invoice-transaction/pay-dialog/pay-dialog.component";
import {InstalmentDialogComponent} from "./payable-invoice-transaction/instalment-dialog/instalment-dialog.component";
import {MatTreeModule} from "@angular/material/tree";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxMatSelectSearchModule} from "ngx-mat-select-search";
import {MatChipsModule} from "@angular/material/chips";
import {MatBadgeModule} from "@angular/material/badge";
import {FinancialSetupsResolve} from "./financial-setups/financial-setups.resolve";
import {PayableInvoiceListResolve} from "./payable-invoice-list/payable-invoice-list.resolve";
import {JournalListResolve} from "./journal/journal-list/journal-list.resolve";
import {CashTransferListResolve} from "./cash-transfer/cash-transfer-list/cash-transfer-list.resolve";
import {BranchAccountsResolve} from "./branch-accounts/branch-accounts.resolve";
import {EntryRoutingModule} from "./entry-routing.module";

@NgModule({
  declarations: [
    RemoteEntryComponent,
    PayableInvoiceListComponent,
    PayableInvoiceTransactionComponent,
    ChartOfAccountComponent,
    ChartDialogComponent,
    SearchAutoComplete,
    FinancialSetupsComponent,
    AddEditBankAccountComponent,
    AddExpenseDialogComponent,
    PayDialogComponent,
    InstalmentDialogComponent,
    PayInstalmentDialogComponent,
    JournalListComponent,
    JournalTransactionComponent,
    JournalDetailsFormComponent,
    BranchAccountsComponent,
    BranchAccountsDrawerComponent,
    BranchAccountsDialogComponent,
    FinanceSettingComponent,
    FinanceTransactionComponent,
    DraftAccountsDialogComponent,
    AccountAnalysisDialogComponent,
    ConfirmationDialogComponent,
    AddSettlementDialogComponent,
    CashTransferListComponent,
    CashTransferTransactionComponent,
    SearchAutoCompleteBranchAccount,
    UnpostedTransactionsDialogComponent
  ],
  imports: [
    SharedModule,
    EntryRoutingModule,
    MatTreeModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule,
    MatChipsModule,
    MatBadgeModule,
    // ShoppingCustomerModule
  ],
  providers: [
    SnackService,
    FinancialSetupsResolve,
    PayableInvoiceListResolve,
    JournalListResolve,
    BranchAccountsResolve,
    CashTransferListResolve
  ],
  schemas: [],
})
export class RemoteEntryModule {}
