import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import { SnackService } from '@khdma-frontend-nx/shared';
import {Location} from '@angular/common';
import { Constant } from '@khdma-frontend-nx/shared';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Subject, Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {CashTransferService} from "../../services/cash-transfer.service";
import { environment } from '@env/environment';


@Component({
  selector: 'cash-transfer-transaction',
  templateUrl: './cash-transfer-transaction.html',
  styleUrls: ['./cash-transfer-transaction.scss'],
})
export class CashTransferTransactionComponent implements OnInit {
  customer = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'));
  baseImageUrl = environment.baseImageUrl;
  cashTransferObj: any;
  cashTransferCreation = false;
  cashTransferEditable = false;
  fromAccounts: any[] = [];
  toAccounts: any[] = [];
  newtoAccounts: any[] = [];

  // table
  private subscriptions = new Subscription();


  protected _onDestroy = new Subject();

  constructor(
    private activateRoute: ActivatedRoute,
    public _translate: TranslateService,
    private _snack: SnackService,
    private _router: Router,
    private cashTransferService: CashTransferService,
    public constant: Constant,
    private locations: Location,
    private _formBuilder: UntypedFormBuilder,
    public dialog: MatDialog,
    @Inject(LOCALE_ID) public locale: string,
  ) {
  }


  cashTransferForm = new UntypedFormGroup({
    fromAccount: new UntypedFormControl(null, Validators.required),
    fromAccountBalance: new UntypedFormControl({value: null, disabled: true}),
    toAccount: new UntypedFormControl(null, Validators.required),
    toAccountBalance: new UntypedFormControl({value: null, disabled: true}),
    amount: new UntypedFormControl(null, Validators.required),
    description: new UntypedFormControl(null, [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)])
  });

  ngOnInit(): void {
    this.bankAccountByCustomerId(this.customer.customerId);
    const id = this.activateRoute.snapshot.paramMap.get('id');
    if (id === 'creation') {
      this.cashTransferCreation = true;
    } else {
      this.getBankTransferDetailsById(+id);
      this.cashTransferEditable = true;
    }
    //
  }

  getBankTransferDetailsById(id: number) {
    this.cashTransferService.getBankTransferDetailsById(id).subscribe((res) => {
        if (res.success) {
          this.cashTransferObj = res.data;
        }
      },
      (err) => console.log(err),
      () => {

        this.cashTransferForm.patchValue(this.cashTransferObj);
        this.disableForm();
      }
    );
  }


  back() {
    this.locations.back();
  }

  save(form, code) {
    if (form.valid && form.value) {
      let formObj = form.value;
      formObj.customer = {id: this.customer.customerId}
      if (this.cashTransferCreation) {
        console.log('creation')
        if (code){
          formObj.transferStatus = {
            id: 1,
            code: 'ENTERED'
          }
          console.log(formObj)
          this.createBankTransfer(formObj);
        }

      }
      if (this.cashTransferEditable) {
        this.isEditable(formObj);

        if (code === 'POSTED'){
          this.isEditable(formObj)
          this.cashTransferObj.transferStatus = {
            id: 2,
            code: 'POSTED'
          }
        }
        if (code === 'CANCELED'){
          this.cashTransferObj.transferStatus = {
            id: 3,
            code: 'CANCELED'
          }
        }
        console.log('edit')
        if (this.cashTransferObj) {
          this.updateBankTransfer(this.cashTransferObj);
        }
      }
    }
  }

isEditable(formObj){
  if(this.cashTransferObj.transferStatus.code === 'ENTERED'){
    this.cashTransferObj.amount = formObj.amount;
    this.cashTransferObj.customer =  formObj.customer;
    this.cashTransferObj.fromAccount = formObj.fromAccount
    this.cashTransferObj.toAccount = formObj.toAccount;
    this.cashTransferObj.description = formObj.description;
  }
}
  createBankTransfer(formObj) {
    this.cashTransferService.createBankTransfer(formObj).subscribe(res => {
      if (res.success) {
        this.cashTransferObj = res.data;
        this.cashTransferCreation = false;
        this.cashTransferEditable = true;
        //  this.disableForm();
        this._snack.showSnack('COMMON.ADD_SUCCESS', 'success');
        this._router.navigateByUrl(`pages/finance/cash-transfer/${res.data.id}`);
      }
    });
  }


  updateBankTransfer(order) {
    this.cashTransferService.updateBankTransfer(order).subscribe(res => {
      if (res.success) {
        this.cashTransferObj = res.data;
         this.disableForm();
        this._snack.showSnack('COMMON.EDIT_SUCCESS', 'success');
      }
    });
  }

  disableForm() {
    if (this.cashTransferObj?.transferStatus?.code === 'POSTED' || this.cashTransferObj?.transferStatus?.code === 'CANCELED') {
      this.cashTransferForm.disable();
    }
  }

  public objectComparisonFunction = function (option, value): boolean {
    return option?.id === value?.id;
  };

  bankAccountByCustomerId(customerId) {
    this.cashTransferService.bankAccountByCustomerId(customerId).subscribe(res => {
      if (res.success) {
        console.log(res)
        this.fromAccounts = res.data;
        this.toAccounts = res.data;
        this.newtoAccounts = res.data;
      }
    });
  }

  fromAccountChanged(fromAccount) {
    console.log(fromAccount);
    this.toAccounts = this.newtoAccounts;
    this.toAccounts = this.toAccounts.filter(item => item.id !== fromAccount.id);
    this.cashTransferForm.patchValue({fromAccountBalance: fromAccount.branchBalance});
  }

  toAccountChanged(toAccount) {
    console.log(toAccount)
    this.cashTransferForm.patchValue({toAccountBalance: toAccount.branchBalance});
  }
}


