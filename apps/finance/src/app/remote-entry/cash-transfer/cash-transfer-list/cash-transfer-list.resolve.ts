import {forkJoin, Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {SearchRequest} from '@khdma-frontend-nx/shared';

import {CashTransferService} from "../../services/cash-transfer.service";

@Injectable()
export class CashTransferListResolve implements Resolve<any[]> {
  constructor(private _service: CashTransferService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    let query = new SearchRequest();
    let branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
    if (route.queryParams.searchCriteria) {
      query = new SearchRequest(JSON.parse(route.queryParams.searchCriteria),
        route.queryParams.pageNumber, route.queryParams.pageSize);
    }
    let Data = [
      this._service.filter(query),
      branchConfig.customerId  ? this._service.bankAccountByCustomerId(branchConfig.customerId) : null
    ];
    Data = Data.filter(el => el != null);
    return forkJoin(Data);
  }
}
