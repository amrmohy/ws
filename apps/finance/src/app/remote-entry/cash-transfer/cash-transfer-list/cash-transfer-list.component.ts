import {Component, OnInit} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import { Constant,SearchConfiguration,SearchRequest,QueryParamsHelperService } from '@khdma-frontend-nx/shared';
import {MatDialog} from "@angular/material/dialog";
import {CashTransferService} from "../../services/cash-transfer.service";
import {
  ConfirmDialogComponent
} from "@khdma-frontend-nx/shared";


@Component({
  selector: 'cash-transfer-list',
  templateUrl: './cash-transfer-list.component.html',
  styleUrls: ['./cash-transfer-list.component.scss'],
  providers: [Constant]
})
export class CashTransferListComponent implements OnInit {
  private subscriptions = new Subscription();
  searchConfiguration: any = new SearchConfiguration();
  dataSource: MatTableDataSource<any>;
  page: SearchRequest = new SearchRequest();
  listOfBankTransfer: any[] = [];

  searchData: any = {};
  initialSearchCriteria;

  constructor(
    private cashTransferService: CashTransferService,
    public constant: Constant,
    private _router: Router,
    public _translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private paramsHelper: QueryParamsHelperService,
    public dialog: MatDialog,
  ) {

    this.subscriptions.add(activatedRoute.data.subscribe(res => {
        this.searchConfiguration = {
          fields: [
            {
              fieldType: 'select',
              name: 'fromAccoundId',
              placeholder: 'CASH_TRANSFER.FROM_ACCOUNT',
              options: res.items[1].data,
              optionID: 'id',
              optionName: 'name'
            },
            {
              fieldType: 'select',
              name: 'toAccountId',
              placeholder: 'CASH_TRANSFER.TO_ACCOUNT',
              options: res.items[1].data,
              optionID: 'id',
              optionName: 'name'
            },
          ]
        };
        this.tableDataSource(res.items[0]);
        console.log(res.items[0]);
      }
    ));



    if (this.activatedRoute.snapshot.queryParams.searchCriteria) {
      this.initialSearchCriteria = JSON.parse(this.activatedRoute.snapshot.queryParams.searchCriteria);
    } else {
      this.initialSearchCriteria = {
        name: null,
        fromAccoundId: null,
        toAccountId: null,
      };
    }

  }


  ngOnInit(): void {
  }

  //table data source
  tableDataSource(source: any) {
    this.dataSource = new MatTableDataSource(source.data.content);
    this.listOfBankTransfer = source.data.content;
    this.page.pageNumber = source.data.pageable.pageNumber;
    this.page.pageSize = source.data.pageable.pageSize;
    this.page.totalElements = source.data.totalElements;
  }

  getByFilter(data) {
    this.searchData = {};
    this.page = new SearchRequest();
    this.getDatabySearch(data);
  }

  //get Data by Search
  getDatabySearch(data) {
    if (data) {
      this.initialSearchCriteria = data;
    }
    // const query = new SearchRequest(data, this.page.pageNumber, this.page.pageSize);
    this.page = new SearchRequest(data);
    this.paramsHelper.changeUrlParams(this.page);

    this.subscriptions.add(
      this.cashTransferService.filter(this.page).subscribe(res => {
        this.tableDataSource(res);
      })
    );

  }


  public onPageChange(criteria: any, event?: PageEvent) {
    this.page = new SearchRequest(this.initialSearchCriteria, event.pageIndex, this.page.pageSize);
    this.paramsHelper.changeUrlParams(this.page);
    this.subscriptions.add(
      this.cashTransferService.filter(this.page).subscribe(res => {
        this.listOfBankTransfer = res.data.content;
        this.page.totalElements = res.data.totalElements;

      })
    );
    return event;
  }

  addNew() {
    this._router.navigate(['/finance/cash-transfer/creation']);
  }

  deleteBankTransfer(bankTransferId) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.cashTransferService.deleteBankTransfer(bankTransferId).subscribe(res => {
          if (res.body.success) {
            this.listOfBankTransfer = this.listOfBankTransfer.filter((trans) => trans.id != bankTransferId);
            this.page.totalElements -= 1;
          }
        });
      }
    });

  }
}
