import {forkJoin, Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {SearchRequest} from '@khdma-frontend-nx/shared';
import {BankAccountService} from '../services/bank-account.service';
import { AuthService } from '@khdma-frontend-nx/shared';


@Injectable()
export class BranchAccountsResolve implements Resolve<any[]> {
  constructor(private _bankAccountService: BankAccountService,
              private _authService :AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    let query = new SearchRequest();
    if (route.queryParams.searchCriteria) {
      query = new SearchRequest(JSON.parse(route.queryParams.searchCriteria),
        route.queryParams.pageNumber, route.queryParams.pageSize);
    }
    let Data = [
      this._bankAccountService.getCustomerBranch(),
    ];
    Data = Data.filter(el => el != null);
    return forkJoin(Data);
  }
}
