import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UntypedFormBuilder} from '@angular/forms';
import { SnackService } from '@khdma-frontend-nx/shared';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {BranchAccountsService} from '../../services/branch-accounts.service';


export interface User {
  name: string;
}

@Component({
  selector: 'app-branch-accounts-dialog',
  templateUrl: './branch-accounts-dialog.component.html',
  styleUrls: ['./branch-accounts-dialog.component.scss']
})
export class BranchAccountsDialogComponent implements OnInit {
  @Input() isEdit;
  @Input() data;
  @Output() onCloseDrawer :EventEmitter<any> = new EventEmitter<any>()
  CONFIG: any = JSON.parse(window.localStorage.getItem('CONFIG'));
  BRANCH_CONFIG: any = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'));
  accountTypes: any;
  branchData: any ={};
  AccountDetailsForm = this._fb.group({
    code: [''],
    nameAr: [''],
    nameEn: [''],
    nameFr: [''],
    // balance: [''],
    accountType: this._fb.group({
      id: ['']
    }),
    parentAccountId:[''],
  });


  constructor(
    private _chartService: BranchAccountsService,
    private _fb: UntypedFormBuilder,
    public _translate: TranslateService,
    private _snack: SnackService,
  ) {
  }
  ngOnInit(): void {

    this.accountTypes = this.CONFIG?.accountTypes;
  }


  ngOnChanges() {
    if(this.isEdit === true) {
      this.getAccountData()
    }
  }

  get Validation() {
    return this.AccountDetailsForm.controls;
  }


  getAccountData() {
    if (this.data?.id) {
      this._chartService.getAccountById(this.data?.id).subscribe((currentAcc) => {
        this.AccountDetailsForm.patchValue(currentAcc.data.account);
        console.log(this.AccountDetailsForm.value);
      });
    }
  }


  callGoogleTranslateApi(text, source, target) {
    let subject = new Subject<string>();
    this._chartService.callGoogleTranslateApi(text, source, target).subscribe(
      (res) => {
        subject.next(res.data.translations[0].translatedText);
      },
      (error) => console.log(error)
    );

    return subject.asObservable();
  }

  changeNameAr() {
    let nameAr = this.AccountDetailsForm.controls.nameAr.value;

    this.callGoogleTranslateApi(nameAr, 'ar', 'en').subscribe((res) => {
      this.AccountDetailsForm.controls.nameEn.setValue(res);
    });

    this.callGoogleTranslateApi(nameAr, 'ar', 'fr').subscribe((res) => {
      this.AccountDetailsForm.controls.nameFr.setValue(res);
    });
  }

  OnSaveAccount() {
    if (this.AccountDetailsForm.valid) {
      let account = this.AccountDetailsForm.value;
      delete account.accountType
      console.log(account);

      //  *****create new account*****

      if (!this.data?.id && this.isEdit === false) {

        account.parentAccountId = this.data?.parentId;

        this._chartService.createAccount(account).subscribe(
          (res) => {
            this.close()
            this.AccountDetailsForm.patchValue({id: res.data.id});


          },
          (err) => console.log('Create Account Error .. ', err),
          () => {
            this._snack.showSnack(
              this._translate.instant("ADD_SUCCESS_CONFIRM"),
              'success'
            );
            // this.ItemDialogRef.close({status:'success'})
          }
        );
      }

      // *****update existing account*****
      else if(this.isEdit === true) {
        account.id = this.data?.id;
        this._chartService.updateAccount({
          "id": this.data?.id,
          "customerBranch": {
            "id": 62
          },
          "balance": 120
        }).subscribe(
          (res) => {
            console.log(account);
            console.log('Account Update .. ', res);
            this.close()
          },
          (err) => console.log('Update Account Error .. ', err),
          () => {
            this._snack.showSnack(
              this._translate.instant('COMMON.EDIT_SUCCESS'),
              'success'
            );
            // this.ItemDialogRef.close({status:'success'})
          }
        );
      }
    }
  }

  close() {
    this.onCloseDrawer.emit();
    this.AccountDetailsForm.reset()
  }
}
