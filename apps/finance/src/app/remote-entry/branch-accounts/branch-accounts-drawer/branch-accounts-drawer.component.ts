import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-branch-accounts-drawer',
  templateUrl: './branch-accounts-drawer.component.html',
  styleUrls: ['./branch-accounts-drawer.component.scss']
})
export class BranchAccountsDrawerComponent implements OnInit {

  drawerOpened: boolean = false;
  isEdit : boolean = false;
  data;
  constructor() { }

  ngOnInit(): void {
  }

  editAccount(id){
    this.data = {id};
    this.isEdit = true;
    this.drawerOpened = true
  }

  addAccount(parentId){
    this.data = {parentId};
    this.isEdit = false;
    this.drawerOpened = true
  }


  closeDrawer() {
    this.drawerOpened = false;
    this.isEdit = false;
  }

}
