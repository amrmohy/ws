import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {BranchAccountsService} from '../../services/branch-accounts.service';

@Component({
  selector: 'app-account-analysis-dialog',
  templateUrl: './account-analysis-dialog.component.html',
  styleUrls: ['./account-analysis-dialog.component.scss']
})
export class AccountAnalysisDialogComponent implements OnInit {
transactions;
  BRANCHCONFIG: any = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'));
  constructor(
    public _translate:TranslateService,
    private branchAccountsService : BranchAccountsService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.branchAccountsService.getTransactions(this.BRANCHCONFIG.id , data).subscribe((result) => {
      console.log(result.data);
      this.transactions = result.data
    })
  }

  ngOnInit(): void {
  }

}
