import {AfterViewInit, Compiler, Component, EventEmitter, Injectable, OnInit, Output} from '@angular/core';
import {FlatTreeControl} from '@angular/cdk/tree';
import {BehaviorSubject, merge, Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {BranchAccountsService} from '../services/branch-accounts.service';
import {CollectionViewer, DataSource, SelectionChange} from '@angular/cdk/collections';
import {map, toArray} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {AccountAnalysisDialogComponent} from './account-analysis-dialog/account-analysis-dialog.component';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';
import { AuthService, Constant, SearchConfiguration, SearchRequest, SnackService } from '@khdma-frontend-nx/shared';


interface AccountNode {
  id: string;
  name: string;
  hasChildren: boolean;
  childrenAccounts?: AccountNode[];
}

export class DynamicFlatNode {
  constructor(
    public item,
    public level = 1,
    public expandable = false,
    public isLoading = false,
    public hasChildren = false,
  ) {
  }
}


@Injectable({providedIn: 'root'})
export class DynamicDatabase {
  rootLevelNodes: AccountNode[];

  constructor(private _branchAccountsService: BranchAccountsService) {
  }

  dataMap;

  // ****************** make sure to provide root nodes here *************
  /** Initial data from database */
  initialData(branchId) {
    return this._branchAccountsService.getBranchAccounts({
      'searchCriteria': {
        'branchId': branchId
      }
    });
  }

  getChildren(node) {
    const searchCriteria = this._branchAccountsService.getSearchCriteria();
    const branchId = this._branchAccountsService.getBranchId();
    return this._branchAccountsService.getBranchAccounts({
      'searchCriteria': {
        'branchId': searchCriteria ? searchCriteria.branchId : branchId,
        'parentId': node?.id
      }
    }).pipe(map((accounts) => {
      this.dataMap = accounts.data;
      return accounts.data.map((account) => account);
    }));
  }

  isExpandable(node: AccountNode): boolean {
    return this.dataMap.includes(node);
    // return this.dataMap.has(node);
  }

}

export class DynamicDataSource implements DataSource<DynamicFlatNode> {
  dataChange = new BehaviorSubject<DynamicFlatNode[]>([]);
  nodeObj;

  get data(): DynamicFlatNode[] {
    return this.dataChange.value;
  }

  set data(value: DynamicFlatNode[]) {
    this._treeControl.dataNodes = value;
    this.dataChange.next(value);
  }

  constructor(
    private _treeControl: FlatTreeControl<DynamicFlatNode>,
    private _database: DynamicDatabase,
  ) {
  }

  connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {
    this._treeControl.expansionModel.changed.subscribe(change => {
      if (
        (change as SelectionChange<DynamicFlatNode>).added ||
        (change as SelectionChange<DynamicFlatNode>).removed
      ) {
        this.handleTreeControl(change as SelectionChange<DynamicFlatNode>);
      }
    });

    return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
  }

  disconnect(collectionViewer: CollectionViewer): void {
  }

  /** Handle expand/collapse behaviors */
  handleTreeControl(change: SelectionChange<DynamicFlatNode>) {
    if (change.added) {
      change.added.forEach(node => this.toggleNode(node, true));
    }
    if (change.removed) {
      change.removed
        .slice()
        .reverse()
        .forEach(node => this.toggleNode(node, false));
    }
  }

  /**
   * Toggle the node, remove from display list
   */
  toggleNode(node: DynamicFlatNode, expand: boolean) {
    const children = this._database.getChildren(node.item);
    const index = this.data.indexOf(node);
    if (!children || index < 0) {
      // If no children, or cannot find the node, no op
      return;
    }

    node.isLoading = true;

    setTimeout(() => {
      if (expand) {
        const nodes = children.pipe(map(
          (childNodes) => {
            return childNodes.map((name) => new DynamicFlatNode(name, node.level + 1), this._database.isExpandable(node.item));
          }
        ), toArray());
        nodes.forEach(node => {
          node.map((nd) => this.data.splice(index + 1, 0, ...nd));
        });
      } else {
        let count = 0;
        for (
          let i = index + 1;
          i < this.data.length && this.data[i].level > node.level;
          i++, count++
        ) {
        }
        this.data.splice(index + 1, count);
      }

      // notify the change
      setTimeout(() => {
        this.dataChange.next(this.data);
      }, 500);
      node.isLoading = false;
    }, 500);
  }
}

@Component({
  selector: 'app-branch-accounts',
  templateUrl: './branch-accounts.component.html',
  styleUrls: ['./branch-accounts.component.scss']
})
export class BranchAccountsComponent implements OnInit {
  @Output() onAddAccount: EventEmitter<any> = new EventEmitter<any>();
  @Output() onEditAccount: EventEmitter<any> = new EventEmitter<any>();
  // treeControl = new NestedTreeControl<AccountNode>(node => node.childrenAccounts);
  // dataSource = new MatTreeNestedDataSource<AccountNode>();
  filteredAccounts;
  accounts: AccountNode[];
  childAccounts;
  CONFIG: any = JSON.parse(localStorage.getItem('CONFIG'));
  BRANCHCONFIG: any = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'));
  accountTypes = this.CONFIG?.accountTypes;
  currentId: string;
  subscriptions = new Subscription();
  searchConfiguration: any = new SearchConfiguration();
  page: SearchRequest = new SearchRequest();
  listOfAccounts: any[] = [];
  searchData: any = {};
  initialSearchCriteria;
  treeControl: FlatTreeControl<DynamicFlatNode>;
  dataSource: DynamicDataSource;

  constructor(
    private _branchAccountsService: BranchAccountsService,
    public _translate: TranslateService,
    public _snack: SnackService,
    public constant: Constant,
    private locations: Location,
    private database: DynamicDatabase,
    private _router: Router,
    private activatedRoute: ActivatedRoute,
    private dynamicData: DynamicDatabase,
    private authService: AuthService,
    private dialog: MatDialog
  ) {
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new DynamicDataSource(this.treeControl, database);
    database.initialData(this.BRANCHCONFIG.id).subscribe((res) => {
      this.dataSource.data = res.data.map(name => new DynamicFlatNode(name, 0, true));
      this._branchAccountsService.setBranchId(this.BRANCHCONFIG.id);
    });
    this.subscriptions.add(activatedRoute.data.subscribe(res => {
      this.searchConfiguration = {
        fields: [
          {
            fieldType: 'input',
            type: 'text',
            name: 'code',
            placeholder: 'FINANCE.CHART_OF_ACCOUNTS.ACCOUNT_CODE',
          },
          {
            fieldType: 'input',
            type: 'text',
            name: 'fromBalance',
            placeholder: 'FINANCE.FROM_BALANCE',
          },
          {
            fieldType: 'input',
            type: 'text',
            name: 'toBalance',
            placeholder: 'FINANCE.TO_BALANCE',
          },
          {
            fieldType: 'input',
            type: 'text',
            name: 'parentName',
            placeholder: 'FINANCE.PARENT_ACCOUNT_NAME',
          },
          {
            fieldType: 'input',
            type: 'text',
            name: 'parentCode',
            placeholder: 'FINANCE.PARENT_ACCOUNT_CODE',
          },
          {
            fieldType: 'select',
            name: 'accountTypeId',
            placeholder: 'FINANCE.CHART_OF_ACCOUNTS.ACCOUNT_TYPE',
            options: this.accountTypes,
            optionID: 'id',
            optionName: 'nameAr'
          },
          {
            fieldType: 'select',
            name: 'branchId',
            placeholder: 'START_RECEIVING_INVENROTY.SELECT_BRANCH',
            options: res.items[0]?.data,
            optionID: 'id',
            optionName: 'branchName'
          }
        ]
      };
    }));
  }


  getLevel = (node: DynamicFlatNode) => node.level;

  isExpandable = (node: DynamicFlatNode) => node.expandable;

  hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable || _nodeData.item.hasChildren;


  ngOnInit(): void {
  }

  back() {
    this.locations.back();
  }

  getByFilter(data) {
    this.searchData = {};
    this.page = new SearchRequest();
    this.getDatabySearch(data);
  }

  //get Data by Search
  activeNode: any;

  getDatabySearch(data) {
    if (data) {
      this.initialSearchCriteria = data;
      this._branchAccountsService.setSearchCriteria(data);
    }
    this.subscriptions.add(
      this._branchAccountsService.getBranchAccounts({
        'searchCriteria': {
          'code': data.code,
          'name': data.name,
          'fromBalance': +data.fromBalance || null,
          'toBalance': +data.toBalance || null,
          'branchId': data.branchId ? data.branchId : JSON.parse(localStorage.getItem('BRANCH_CONFIG')).id,
          // "parentId": 1,
          'parentCode': data.parentCode,
          'parentName': data.parentName,
          'accountTypeId': data.accountTypeId
        }
      }).subscribe(res => {
        this.filteredAccounts = res.data;
        this.filteredAccounts?.map(name => new DynamicFlatNode(name, 0, true));
        this.dynamicData.rootLevelNodes = res.data;
        this.dataSource.data = this.dynamicData.rootLevelNodes?.map(name => new DynamicFlatNode(name, 0, true));
      })
    );

  }


  changeStatus(account) {
    console.log(account);
    if (account.branchAccountUsed === true) {
      if (!(account.branchBalance > 0)) {
        this._branchAccountsService.disableAccount(this.BRANCHCONFIG?.id, account.id).subscribe((res) => {
        });
      }
    } else {
      this._branchAccountsService.enableAccount(account.id, this.BRANCHCONFIG?.id, account.balance).subscribe((res) => {

      });
    }
  }


  editAccount(id) {
    this.onEditAccount.emit(id);
  }

  addAccount(parentId) {
    this.onAddAccount.emit(parentId);
  }

  deleteAccount(id) {
    this._branchAccountsService.deleteAccount(id).subscribe((res) => {
      if (res?.body?.success) {
      }
    });
  }

  openAccountAnalysisDialog(branchAccountId) {
    const dialogRef = this.dialog.open(AccountAnalysisDialogComponent, {
      data: branchAccountId
    }).addPanelClass('custom_dialog');

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openEnableConfirmation() {
    const confirmationDialogRef = this.dialog.open(ConfirmationDialogComponent).addPanelClass('confirmation_dialog');

    confirmationDialogRef.afterClosed().subscribe(result => {
      console.log(`Confirmation Dialog result: ${result.status}`);
      if (result.status && result.status === 'success') {
        this.dataSource.data.forEach((branch) => {
          branch.item.branchAccountUsed = true;
        });
      }
    });
  }
}
