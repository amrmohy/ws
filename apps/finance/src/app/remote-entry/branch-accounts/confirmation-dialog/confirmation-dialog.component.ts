import {Component, OnInit} from '@angular/core';
import {BranchAccountsService} from '../../services/branch-accounts.service';
import {TranslateService} from '@ngx-translate/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {
  BRANCHCONFIG: any = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'));

  constructor(
    private _branchAccountsService:BranchAccountsService,
    public _translate : TranslateService,
    private ItemDialogRef: MatDialogRef<ConfirmationDialogComponent>,
  ) { }

  ngOnInit(): void {
  }
  enableAllAccounts(){
    this._branchAccountsService.enableAllAccounts(this.BRANCHCONFIG?.id).subscribe((res) => {
      if (res?.success === true ){
      this.ItemDialogRef.close({status: 'success'})
      }
      }
    )
  }
}
