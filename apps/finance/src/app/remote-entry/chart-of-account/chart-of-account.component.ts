import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {DialogPosition, MatDialog} from '@angular/material/dialog';
import {ChartOfAccountsService} from '../services/chart-of-accounts.service';
import {TranslateService} from '@ngx-translate/core';
import {Constant,SearchConfiguration,SearchRequest} from '@khdma-frontend-nx/shared';
import {Router} from '@angular/router';
import {BehaviorSubject, Subscription} from 'rxjs';
import {Location} from '@angular/common';
import {DraftAccountsDialogComponent} from './draft-accounts-dialog/draft-accounts-dialog.component';
import {ChartDialogComponent} from './chart-dialog/chart-dialog.component';
import {Platform} from '@angular/cdk/platform';


interface AccountNode {
  color: string;
  id: string;
  name: string;
  level: number;
  childrenAccounts?: AccountNode[];
}


@Component({
  selector: 'app-chart-of-account',
  templateUrl: './chart-of-account.component.html',
  styleUrls: ['./chart-of-account.component.scss']
})
export class ChartOfAccountComponent implements OnInit {
  @Output() onAddAccount: EventEmitter<any> = new EventEmitter<any>();
  @Output() onEditAccount: EventEmitter<any> = new EventEmitter<any>();
  treeControl = new NestedTreeControl<AccountNode>(node => node.childrenAccounts);
  dataSource = new MatTreeNestedDataSource<AccountNode>();
  accounts: AccountNode[];
  dataChange = new BehaviorSubject<AccountNode[]>([]);
  draftAccountsCount;
  CONFIG: any = JSON.parse(window.localStorage.getItem('CONFIG'));
  accountTypes = this.CONFIG?.accountTypes.map((acc) => {
    return {name: acc.name};
  });
  palette = ['#521262','#6F38C5','#0F3460','#764AF1','#5800FF'];
  currentId: string;
  subscriptions = new Subscription();
  searchConfiguration: any = new SearchConfiguration();
  page: SearchRequest = new SearchRequest();
  listOfAccounts: any[] = [];
  searchData: any = {};
  initialSearchCriteria;

  constructor(
    private _chartService: ChartOfAccountsService,
    public _translate: TranslateService,
    public constant: Constant,
    private locations: Location,
    private dialog: MatDialog,
    private _router: Router,
    public platform: Platform) {

    this.getAllAccounts();
    this.getDraftAccountsCount();

    this.searchConfiguration = {
      fields: [
        {
          fieldType: 'input',
          type: 'text',
          name: 'code',
          placeholder: 'FINANCE.CHART_OF_ACCOUNTS.ACCOUNT_CODE',
        },
        {
          fieldType: 'select',
          name: 'accountTypeId',
          placeholder: 'FINANCE.CHART_OF_ACCOUNTS.ACCOUNT_TYPE',
          options: this.CONFIG?.accountTypes,
          optionID: 'id',
          optionName: 'nameAr'
        }
      ]
    };


  }

  ngOnInit(): void {
  }

  back() {
    this.locations.back();
  }

  getAllAccounts() {
    this._chartService.getAllAccounts({}).subscribe((account) => {
      this.accounts = account.data
      this.accounts.forEach((account) => {
        account.level = 1
        account.color = this.palette[0]
      });
      this.sortTree(this.accounts);
      console.log(this.accounts);
    });
  }

  sortTree(accounts) {
    for (let i in accounts) {
      if (accounts[i].childrenAccounts.length > 0) {
        for (let k in accounts[i].childrenAccounts) {
          accounts[i].childrenAccounts[k].level = accounts[i].level + 1;
          accounts[i].childrenAccounts[k].color = this.assignColor(accounts[i].childrenAccounts[k].level)
        }
        this.sortTree(accounts[i].childrenAccounts);
      } else {
        accounts[i].isLeaf = true
        this.assignColor(4)
      }
    }
  }

  assignColor(level) {

    let color;
    switch (level){
      case 1:
        color = this.palette[0];
        break;
      case 2:
        color = this.palette[1];
        break;
      case 3:
        color = this.palette[2];
        break;
      case 4:
        color = this.palette[3];
        break;
      case 5:
        color = this.palette[4];
        break;
      // case 6:
      //   color = this.palette[5];
      //   break;
      // case 7:
      //   color = this.palette[6];
      //   break;
      // case 8:
      //   color = this.palette[7];
      //   break;
      // case 9:
      //   color = this.palette[8];
      //   break;
      // case 10:
      //   color = this.palette[9];
      //   break;
      default:
        color = '#363537'
    }
    return color;
  }

  getByFilter(data) {
    this.searchData = {};
    this.page = new SearchRequest();
    this.getDatabySearch(data);
  }

  //get Data by Search
  getDatabySearch(data) {
    console.log(data);
    this.subscriptions.add(
      this._chartService.filter({'searchCriteria': data}).subscribe(res => {
        this.accounts = res.data;
      })
    );

  }


  hasChild = (_: number, node: AccountNode) => !!node.childrenAccounts && node.childrenAccounts.length > 0;
  activeNode: any;

  get data(): AccountNode[] {
    return this.dataChange.value;
  }

  insertItem(parent: AccountNode, childNode) {
    parent.childrenAccounts.push(childNode);
    this.dataChange.next(this.data);
    this.treeControl.expand(parent);

  }

  addAccount(node) {
    console.log(node);
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(ChartDialogComponent, {
      data: {parentId: node.id},
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh',
      panelClass: 'custom-drawer'
    });
    dialogRef.afterClosed().subscribe((res) => {
        if (res?.status === 'success') {
          this.insertItem(node, res.createdAccount);
          // node.childrenAccounts.push(res.createdAccount)
          console.log(node);
        }
      }
    );
  }

  editAccount(node) {
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(ChartDialogComponent, {
      data: {id: node.id},
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh',
      panelClass: 'custom-drawer'
    });
    dialogRef.afterClosed().subscribe((res) => {
        if (res?.status === 'success') {
          node.name = res.updatedAccount.name;
          node.code = res.updatedAccount.code;
        }
      }
    );
  }


  deleteAccount(id) {
    this._chartService.deleteAccount(id).subscribe((res) => {
      if (res?.body?.success) {
        this.accounts.filter((account) => account.id !== id);
      }
    });
  }

  getDraftAccountsCount() {
    this._chartService.getDraftAccountsCount().subscribe((res) => {
      this.draftAccountsCount = res.data;
    });
  }

  openDraftAccounts() {
    this.dialog.open(DraftAccountsDialogComponent).addPanelClass('custom_dialog').afterClosed().subscribe((result) => {
      if (result?.data === 'success') {

      }
    });
  }
}
