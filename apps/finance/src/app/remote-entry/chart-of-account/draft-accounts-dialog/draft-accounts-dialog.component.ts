import {Component, OnInit} from '@angular/core';
import {ChartOfAccountsService} from '../../services/chart-of-accounts.service';
import {TranslateService} from '@ngx-translate/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-draft-accounts-dialog',
  templateUrl: './draft-accounts-dialog.component.html',
  styleUrls: ['./draft-accounts-dialog.component.scss']
})
export class DraftAccountsDialogComponent implements OnInit {
  draftAccounts;
  constructor(
  private  _chartService : ChartOfAccountsService,
  public _translate : TranslateService,
  private dialogRef: MatDialogRef<DraftAccountsDialogComponent>
  ) {
    this.getDraftAccounts()
  }

  ngOnInit(): void {
  }

  getDraftAccounts() {
    this._chartService.getDraftAccounts().subscribe((result)=>{
      this.draftAccounts = result.data
    })
  }

  approveDraftAccount(draftAccountId) {
    this._chartService.ApproveDraftAccount(draftAccountId).subscribe((res)=>{
    },(error) => {
      console.log(error);},()=>{
      this.dialogRef.close({
        data: 'success',
      });
    })
  }

  rejectDraftAccount(draftAccountId) {
    this._chartService.RejectDraftAccount(draftAccountId).subscribe((res)=>{
    },(error) => {
      console.log(error);},()=>{
      this.dialogRef.close({
        data: 'success',
      });
    })
  }
}
