import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output} from '@angular/core';
import {UntypedFormBuilder} from '@angular/forms';
import {Subject} from 'rxjs';
// import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ChartOfAccountsService} from '../../services/chart-of-accounts.service';
import {TranslateService} from '@ngx-translate/core';
import {AuthService,SnackService} from '@khdma-frontend-nx/shared';

import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export interface User {
  name: string;
}


@Component({
  selector: 'app-chart-dialog',
  templateUrl: './chart-dialog.component.html',
  styleUrls: ['./chart-dialog.component.scss']
})
export class ChartDialogComponent implements OnInit {
  // @Input() isEdit;
  // @Input() data;
  // @Output() onCloseDrawer: EventEmitter<any> = new EventEmitter<any>();
  CONFIG: any = JSON.parse(window.localStorage.getItem('CONFIG'));
  accountTypes = this.CONFIG?.accountTypes;
  services = this.CONFIG?.services;
  servicesCategories: any;
  accountServices: any;

  branchData: any = {};
  AccountDetailsForm = this._fb.group({
    code: [''],
    nameAr: [''],
    nameEn: [''],
    nameFr: [''],
    accountType: this._fb.group({
      id: ['']
    }),
    parentAccountId: [''],
    services: [],
    serviceCategories: [],
  });


  constructor(
    private _chartService: ChartOfAccountsService,
    private _fb: UntypedFormBuilder,
    public _translate: TranslateService,
    private _snack: SnackService,
    private authService: AuthService,
    private ItemDialogRef: MatDialogRef<ChartDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { parentId: any,id:any },
  ) {
  }

  ngOnInit(): void {
    this.authService.config.subscribe((config) => {
      this.accountTypes = config?.accountTypes;
      this.services = config?.services;
    });
    if (this.data.id) {   //// edit
      this.getAccountData();
    }
  }


  get Validation() {
    return this.AccountDetailsForm.controls;
  }


  getAccountData() {
    if (this.data?.id) {
      this._chartService.getAccountById(this.data?.id).subscribe((currentAcc) => {
        currentAcc.data.serviceCategories = currentAcc?.data?.serviceCategories?.map(cat => cat.id);
        currentAcc.data.services = currentAcc?.data?.services?.map(ser => ser.id);
        this.getSerCatsOnPatch(currentAcc.data.services);
        this.AccountDetailsForm.patchValue(currentAcc.data);
        if (this.data.id) { //// edit
          // this.AccountDetailsForm.get('code').disable();
          this.AccountDetailsForm.get('accountType').disable();
        }
      });
    }
  }

  getSerCatsOnPatch(servicesIds) {
    if (servicesIds && servicesIds.length) {
      this.getServiceCategory(servicesIds);
    }
  }

  getServices() {
    this._chartService.getServiceShopping().subscribe(res => {
      this.services = res.data;
    });
  }

  getServiceCategory(ids) {
    this._chartService.getServiceCategory(ids).subscribe(res => {
      this.branchData.servicesCategories = res.data;
    });
  }


  callGoogleTranslateApi(text, source, target) {
    let subject = new Subject<string>();
    this._chartService.callGoogleTranslateApi(text, source, target).subscribe(
      (res) => {
        subject.next(res.data.translations[0].translatedText);
      },
      (error) => console.log(error)
    );

    return subject.asObservable();
  }

  changeNameAr() {
    let nameAr = this.AccountDetailsForm.controls.nameAr.value;

    this.callGoogleTranslateApi(nameAr, 'ar', 'en').subscribe((res) => {
      this.AccountDetailsForm.controls.nameEn.setValue(res);
    });

    this.callGoogleTranslateApi(nameAr, 'ar', 'fr').subscribe((res) => {
      this.AccountDetailsForm.controls.nameFr.setValue(res);
    });
  }

  OnSaveAccount() {
    if (this.AccountDetailsForm.valid) {
      let account = this.AccountDetailsForm.value;
      if (account.services) {
        account.services = account.services.map(serId => {
          return {id: serId};
        });
      }
      if (account.serviceCategories) {
        account.serviceCategories = account.serviceCategories.map(catId => {

          return {id: catId};
        });
      }

      //  *****create new account*****

      if (!this.data?.id) {

        account.parentAccountId = this.data?.parentId;
        this._chartService.createAccount(account).subscribe(
          (res) => {
            this._chartService.setCreatedAccount(res.data)
            this.ItemDialogRef.close({status: 'success',createdAccount:res.data});
            this.AccountDetailsForm.patchValue({id: res.data.id});
          },
          (err) => console.log('Create Account Error .. ', err),
          () => {
            this._snack.showSnack(
              this._translate.instant('COMMON.ADD_SUCCESS'),
              'success'
            );
            // this.ItemDialogRef.close({status:'success'})
          }
        );
      }

      // *****update existing account*****
      else if (this.data?.id) {

        account.id = this.data?.id;
        account.code = this.AccountDetailsForm.get('code').value;
        account.accountType = this.AccountDetailsForm.get('accountType').value;
        this._chartService.updateAccount(account).subscribe(
          (res) => {
            this._chartService.setCreatedAccount(res.data)
            this.ItemDialogRef.close({status: 'success',updatedAccount:res.data});
          },
          (err) => console.log('Update Account Error .. ', err),
          () => {
            this._snack.showSnack(
              this._translate.instant('COMMON.EDIT_SUCCESS'),
              'success'
            );
            // this.ItemDialogRef.close({status:'success'})
          }
        );
      }
    }
  }

  close() {
    this.ItemDialogRef.close({status: ''});
  }
}
