import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import { Constant } from '@khdma-frontend-nx/shared';
import {PayableInvoiceService} from '../../services/payable-invoice.service';

@Component({
  selector: 'pay-dialog',
  templateUrl: './pay-dialog.component.html',
  styleUrls: ['./pay-dialog.component.scss']
})
export class PayDialogComponent implements OnInit {
  AccountsData: any[] = [];
  maxDate = new Date();
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  constructor(
    private payDialogRef: MatDialogRef<PayDialogComponent>,
    private constant: Constant,
    private payableInvoiceService: PayableInvoiceService,
    @Inject(MAT_DIALOG_DATA) public invoiceTotal: any,
    @Inject(LOCALE_ID) public locale: string,
  ) {
    this.getPayableAccounts(this.invoiceTotal.branchId)
  }


  getPayableAccounts(branchId){
    this.payableInvoiceService.getPayableAccounts(branchId).subscribe((res) => {
      this.AccountsData = res.data;
    })
  }

  ngOnInit(): void {

    console.log(this.invoiceTotal);
    if (this.invoiceTotal.totalPaidAmount > 0) {
      this.item.patchValue({paidAmount: this.invoiceTotal.invoiceTotalAmount - this.invoiceTotal.totalPaidAmount});
    }
  }

  item = new UntypedFormGroup({
    date: new UntypedFormControl(new Date().toISOString().substring(0, 10), Validators.required),
    paidAmount: new UntypedFormControl(null, Validators.required),
    account: new UntypedFormControl(null, Validators.required),
  });


  get Validation() {
    return this.item.controls;
  }

  public hasError = (controlName, errorName) => {
    return this.Validation[controlName].hasError(errorName);
  };


  //on item submit
  onSubmit() {
    let item = this.item.getRawValue();
    //item.account = {id: item.accountId};
   // delete item.accountId;
    this.payDialogRef.close({
      data: {
        action: 'save',
        item: item
      },
    });
  }

//on item submit
  saveAndPost() {
    let item = this.item.getRawValue();
    //item.account = {id: item.accountId};
   // delete item.accountId;
    this.payDialogRef.close({
      data: {
        action: 'post',
        item: item
      },
    });
  }

  //on item Cancel
  onCancel() {
    this.payDialogRef.close({
      data: {
        action: 'cancel',
        item: this.item.value
      },
    });
  }
  public objectComparisonFunction = function(option, value): boolean {
    return option?.id === value?.id;
  };
}
