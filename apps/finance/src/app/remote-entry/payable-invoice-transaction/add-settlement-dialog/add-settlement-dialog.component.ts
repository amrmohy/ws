import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Component, Inject, OnInit} from '@angular/core';
import { Constant,SnackService,AuthService } from '@khdma-frontend-nx/shared';
import {PayableInvoiceService} from '../../services/payable-invoice.service';

@Component({
  selector: 'add-settlement-dialog',
  templateUrl: './add-settlement-dialog.component.html',
  styleUrls: ['./add-settlement-dialog.component.scss']
})
export class AddSettlementDialogComponent implements OnInit {
  config: any = JSON.parse(window.localStorage.getItem('CONFIG'));
  supplierPrepaymentsArray = [];
  selectedPrepaymentsArray = [];
  totalAmountToApply: number;

  constructor(
    private settlementDialogRef: MatDialogRef<AddSettlementDialogComponent>,
    private constant: Constant,
    private _snack: SnackService,
    private authService: AuthService,
    private payableInvoiceService: PayableInvoiceService,
    @Inject(MAT_DIALOG_DATA) public settlementData: any
  ) {
    this.loadSupplierPrepayments();
  }


  ngOnInit(): void {

  }

  loadSupplierPrepayments() {
    if (this.settlementData.supplierId && this.settlementData.branchId) {
      this.getSupplierPrepayments(this.settlementData.supplierId, this.settlementData.branchId)
    }
  }


  onSubmit(prepaymentForm) {
    if (prepaymentForm.touched && prepaymentForm.valid && Object.keys(prepaymentForm.value).length > 0) {
      this.settlementDialogRef.close({
        data: {
          action: 'save',
          settlement: this.selectedPrepaymentsArray
        },
      });
    } else {
      this._snack.showSnack('COMMON.CREATE_AT_LEAST_LINE', 'error');
    }
  }


  onCancel() {
    this.settlementDialogRef.close({
      data: {
        action: 'cancel',
      },
    });
  }

  public objectComparisonFunction = function (option, value): boolean {
    return option?.id === value?.id;
  };

  getSupplierPrepayments(supplierId: number, branchId: number) {
    this.payableInvoiceService.getSupplierPrepayments(supplierId, branchId).subscribe(res => {
      if (res.success) {
        this.supplierPrepaymentsArray = res.data.map((item) => {
          item.price = 0;
          item.isChecked = false;
          return item;
        });
      }
    })
  }

  onSupplierPrepaymentChecked(event, supplierPrepayment) {
    if (event.checked) {
      const index = this.supplierPrepaymentsArray.findIndex(selected => selected.id === supplierPrepayment.id);
      if (index > -1) {
        this.supplierPrepaymentsArray[index].isChecked = true;
      }
      this.selectedPrepaymentsArray.push(supplierPrepayment);
      this.AmountToApply();
    } else {
      const supplierPrepaymentIndex = this.supplierPrepaymentsArray.findIndex(selected => selected.id === supplierPrepayment.id);
      if (supplierPrepaymentIndex > -1) {
        this.supplierPrepaymentsArray[supplierPrepaymentIndex].isChecked = false;
      }
      const index = this.selectedPrepaymentsArray.findIndex(selected => selected.id === supplierPrepayment.id);
      this.selectedPrepaymentsArray.splice(index, 1);
      this.AmountToApply();
    }

  }


  AmountToApply() {
    let totalPrice = 0;
    if (this.selectedPrepaymentsArray && this.selectedPrepaymentsArray.length > 0) {
      this.selectedPrepaymentsArray.forEach(selected => {
        totalPrice += selected.price;
      });
      this.totalAmountToApply = totalPrice;
    } else {
      this.totalAmountToApply = 0;
    }
  }

}
