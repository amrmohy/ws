import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {formatNumber} from '@angular/common';
import { AuthService } from '@khdma-frontend-nx/shared';

@Component({
  selector: 'instalment-dialog',
  templateUrl: './instalment-dialog.component.html',
  styleUrls: ['./instalment-dialog.component.scss']
})
export class InstalmentDialogComponent implements OnInit {
  paymentPeriods = JSON.parse(window.localStorage.getItem('CONFIG'))?.paymentPeriods;
  installValue:number = 0;
  installCount:number = 0;
  installCountMax:number;
  installValueMax:number;
  constructor(
    private instalmentDialogRef: MatDialogRef<InstalmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public invoiceTotalAmount: any,
    @Inject(LOCALE_ID) public locale: string,
    private authService: AuthService,
  ) {

  }


  ngOnInit(): void {
    this.installCountMax = this.invoiceTotalAmount;
    this.installValueMax = this.invoiceTotalAmount;
    this.setPaymentPeriods();
  }

  setPaymentPeriods(){
    this.authService.config.subscribe((config)=>{
      this.paymentPeriods = config?.paymentPeriods;
    });
  }

  item = new UntypedFormGroup({
    paymentPeriodId: new UntypedFormControl(null, Validators.required),
    installValue: new UntypedFormControl(null, Validators.required),
    installCount: new UntypedFormControl(null, Validators.required),
  });


  get Validation() {
    return this.item.controls;
  }

  public hasError = (controlName, errorName) => {
    return this.Validation[controlName].hasError(errorName);
  };


  //on item submit
  onSubmit() {
    let item = this.item.getRawValue();
    //item.account = {id: item.accountId};
    delete item.accountId;
    this.instalmentDialogRef.close({
      data: {
        action: 'save',
        item: item
      },
    });
  }

  //on item Cancel
  onCancel() {
    this.instalmentDialogRef.close({
      data: {
        action: 'cancel',
        item: this.item.value
      },
    });
  }
  changedInstallValue(installValue) {
    if (installValue) {
      if (installValue <= this.invoiceTotalAmount) {
        this.installValue = installValue;
        this.installValueMax = installValue;
        this.item.patchValue({
          installCount: parseFloat(formatNumber(this.invoiceTotalAmount / installValue, this.locale, '1.2-2'))
        });
        this.installCountMax = parseFloat(formatNumber(this.invoiceTotalAmount / installValue, this.locale, '1.2-2'));
      }
    } else {
      this.item.patchValue({
        installCount: ''
      });
    }
  }

  changedInstallCount(installCount) {
    if (installCount) {
      if (installCount <= this.invoiceTotalAmount) {
        this.installCount = installCount;
        this.installCountMax = installCount;
        this.item.patchValue({
          installValue: parseFloat(formatNumber(this.invoiceTotalAmount / installCount, this.locale, '1.2-2'))
        });
        this.installValueMax = parseFloat(formatNumber(this.invoiceTotalAmount / installCount, this.locale, '1.2-2'));
      }
    } else {
      this.item.patchValue({
        installValue: ''
      });
    }
  }

  changeInstallValue() {
    let installValue = this.item.controls.installValue.value;
    this.changedInstallValue(installValue);
  }

  changeInstallCount() {
    let installCount = this.item.controls.installCount.value;
    this.changedInstallCount(installCount);
  }
}
