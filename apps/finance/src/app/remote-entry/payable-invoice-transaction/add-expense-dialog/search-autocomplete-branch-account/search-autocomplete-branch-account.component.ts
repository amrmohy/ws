import {Component, EventEmitter, Input, OnInit, Output, AfterViewInit, OnDestroy, ViewChild} from '@angular/core';
import {UntypedFormControl} from '@angular/forms';
import {Observable, ReplaySubject, Subject, combineLatest} from 'rxjs';
import {filter, map, startWith, take, takeUntil, delay} from 'rxjs/operators';
import {MatSelect} from '@angular/material/select';


interface BranchAccount {
  // account: {
  //   id: string;
  //   name: string;
  //   code: string;
  // }
  id: string;
  name: string;
  code: string;
}

@Component({
  selector: 'search-autocomplete-branch-account',
  templateUrl: './search-autocomplete-branch-account.component.html',
  styleUrls: ['./search-autocomplete-branch-account.component.scss']
})

export class SearchAutoCompleteBranchAccount implements OnInit, AfterViewInit, OnDestroy {

  // public itemMultiCtrl: FormControl = new FormControl();
  public branchAccountFilterCtrl: UntypedFormControl = new UntypedFormControl();
  public filteredBranchAccounts: ReplaySubject<BranchAccount[]> = new ReplaySubject<BranchAccount[]>(1);

  @ViewChild('singleSelect', {static: true}) singleSelect: MatSelect;

  protected _onDestroy = new Subject<void>();


  @Input() set data(data: any[]) {
    this._data = data;
    this.filteredBranchAccounts.next(this.data.slice());
  }

  get data(): any[] {
    return this._data;
  }


  private _data: any[];

  @Output() result: EventEmitter<any> = new EventEmitter<any>();
  @Input() outlineLabel: String;
  @Input() itemMultiCtrl: UntypedFormControl;

  account: any = {};

  constructor() {
  }

  ngOnInit() {
    this.branchAccountFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy), delay(1000))
      .subscribe(() => {
        this.filterBranchAccounts();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onChange($event) {
    this.result.emit($event);
  }


  protected setInitialValue() {
    this.filteredBranchAccounts
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: BranchAccount, b: BranchAccount) => a && b && a.id === b.id;
      });
  }


  protected filterBranchAccounts() {
    if (!this.data) {
      return;
    }

    let search = this.branchAccountFilterCtrl.value;
    if (!search) {
      this.filteredBranchAccounts.next(this.data.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredBranchAccounts.next(
      this.data.filter(branchAccount => branchAccount.account.name.toLowerCase().indexOf(search) > -1)
    );
  }
}
