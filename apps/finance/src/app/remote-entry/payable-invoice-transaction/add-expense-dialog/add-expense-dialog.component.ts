import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Component, Inject, OnInit} from '@angular/core';
import { Constant,SnackService,AuthService } from '@khdma-frontend-nx/shared';
import {PayableInvoiceService} from '../../services/payable-invoice.service';

@Component({
  selector: 'add-expense-dialog',
  templateUrl: './add-expense-dialog.component.html',
  styleUrls: ['./add-expense-dialog.component.scss']
})
export class AddExpenseDialogComponent implements OnInit {
  expenseAccountsData: any[] = [];

  config: any = JSON.parse(window.localStorage.getItem('CONFIG'));
  accountTypes = this.config?.accountTypes;
  branchAccounts: any[] = [];
  branchId: number;

  constructor(
    private expenseDialogRef: MatDialogRef<AddExpenseDialogComponent>,
    private constant: Constant,
    private _snack: SnackService,
    private authService: AuthService,
    private payableInvoiceService: PayableInvoiceService,
    @Inject(MAT_DIALOG_DATA) public expenseData: any
  ) {
    this.load();
  }


  ngOnInit(): void {
    this.setAccountTypes();
  }

  setAccountTypes(){
    this.authService.config.subscribe((config) => {
      this.accountTypes= config?.accountTypes;
    });
  }

  load() {
    if (this.expenseData.branchId) {
      this.branchId = this.expenseData.branchId;
    }
    if (this.expenseData.expense) {
      this.item.patchValue({
        accountType: this.expenseData.expense?.accountType,
        branchAccountId: this.expenseData?.expense?.branchAccount,
        price: this.expenseData?.expense?.price,
        salesTaxValue: this.expenseData?.expense?.salesTaxValue,
        total: this.expenseData?.expense?.price + this.expenseData?.expense?.salesTaxValue,
        discription: this.expenseData?.expense?.discription,
      });
      this.getbranchAccounts(this.expenseData?.expense?.accountType?.id);
    }
  }

  item = new UntypedFormGroup({
    price: new UntypedFormControl(null, Validators.required),
    salesTaxValue: new UntypedFormControl(null, Validators.required),
    total: new UntypedFormControl({value: null, disabled: true}),
    discription: new UntypedFormControl(null, [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]),
    accountType: new UntypedFormControl(null, Validators.required),
    branchAccount: new UntypedFormControl(null),
    branchAccountId: new UntypedFormControl(null, Validators.required)
  });


  get Validation() {
    return this.item.controls;
  }

  public hasError = (controlName, errorName) => {
    return this.Validation[controlName].hasError(errorName);
  };


  //on item submit
  onSubmit() {
    let item = this.item.getRawValue();
    delete item.total;
    delete item.branchAccountId;
    this.expenseDialogRef.close({
      data: {
        action: 'save',
        item: item
      },
    });
  }

  //on item Cancel
  onCancel() {
    this.expenseDialogRef.close({
      data: {
        action: 'cancel',
        item: this.item.value
      },
    });
  }

  public objectComparisonFunction = function(option, value): boolean {
    return option?.id === value?.id;
  };

  accountTypeChanged(accountType) {
    console.log(accountType);
    this.getbranchAccounts(accountType.id);
  }

  getbranchAccounts(accountTypeId) {
    if (this.branchId) {
      this.payableInvoiceService.getbranchAccounts(accountTypeId, this.branchId).subscribe((res) => {
        if (res.success) {
          this.branchAccounts = res.data;
        }
      });
    } else {
      this._snack.showSnack('START_RECEIVING_INVENROTY.SELECT_BRANCH','error');
      this.expenseDialogRef.close();
    }
  }



  getAccount(obj){
    console.log(obj.value)
    this.item.patchValue({
      branchAccount: obj.value
    });
  }

}
