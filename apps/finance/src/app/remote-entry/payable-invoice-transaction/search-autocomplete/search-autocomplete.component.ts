import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {UntypedFormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {delay, take, takeUntil} from 'rxjs/operators';
import {MatSelect} from '@angular/material/select';


interface RecOrder {
  id: string;
  supplierFullName: string;
  receiptDate: string;
}

@Component({
  selector: 'search-autocomplete',
  templateUrl: './search-autocomplete.component.html',
  styleUrls: ['./search-autocomplete.component.scss']
})

export class SearchAutoComplete implements OnInit, AfterViewInit, OnDestroy {

  // public itemMultiCtrl: FormControl = new FormControl();
  public receivingFilterCtrl: UntypedFormControl = new UntypedFormControl();
  public filteredReceivingOrders: ReplaySubject<RecOrder[]> = new ReplaySubject<RecOrder[]>(1);

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  protected _onDestroy = new Subject<void>();


  @Input() set data(data: any[]) {
    this._data = data;
    this.filteredReceivingOrders.next(this.data.slice());
  }

  get data(): any[] {
    return this._data;
  }



  private _data: any[];

  @Output() result: EventEmitter<any> = new EventEmitter<any>();
  @Input() outlineLabel: String;
  @Input() itemMultiCtrl: UntypedFormControl;


  constructor() {
  }

  ngOnInit() {
    this.receivingFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy), delay(1000))
      .subscribe(() => {
        this.filterReceivingOrders();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onChange($event) {
    this.result.emit($event);
  }


  protected setInitialValue() {
    this.filteredReceivingOrders
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: RecOrder, b: RecOrder) => a && b && a.id === b.id;
      });
  }





  protected filterReceivingOrders() {
    if (!this.data) {
      return;
    }

    let search = this.receivingFilterCtrl.value;
    if (!search) {
      this.filteredReceivingOrders.next(this.data.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredReceivingOrders.next(
      this.data.filter(receivingOrder => receivingOrder.supplierFullName.toLowerCase().indexOf(search) > -1)
    );
  }
}
