import {Component, Inject, LOCALE_ID, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmDialogComponent, Constant, SnackService  } from '@khdma-frontend-nx/shared';
import {DatePipe, Location} from '@angular/common';
import {PayableInvoiceService} from '../services/payable-invoice.service';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Subject, Subscription} from 'rxjs';
import {DialogPosition, MatDialog} from '@angular/material/dialog';
import {AddExpenseDialogComponent} from './add-expense-dialog/add-expense-dialog.component';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {PayDialogComponent} from './pay-dialog/pay-dialog.component';
import {InstalmentDialogComponent} from './instalment-dialog/instalment-dialog.component';
import {PayInstalmentDialogComponent} from './pay-instalment-dialog/pay-instalment-dialog.component';
import { AuthService } from '@khdma-frontend-nx/shared';
import {AddSettlementDialogComponent} from './add-settlement-dialog/add-settlement-dialog.component';
import { environment } from '@env/environment';


@Component({
  selector: 'payable-invoice-transaction',
  templateUrl: './payable-invoice-transaction.html',
  styleUrls: ['./payable-invoice-transaction.scss'],
})
export class PayableInvoiceTransactionComponent implements OnInit, OnDestroy {
  CONFIG: any = JSON.parse(window.localStorage.getItem('CONFIG'));
  paymentMethods: any[] = [];
  paymentTerms: any[] = [];
  payableInvoiceTypes: any[] = [];
  payableInvoiceStatus: any[] = [];
  baseImageUrl = environment.baseImageUrl;
  order: any;
  branchId: number;
  selectedBranchId: number;
  selectedSupplierId: number;
  standard_invoice: any;
  supplierBalance: number = 0;
  taxAmount: number;
  totalAmount: number;
  paidAmount: number = 0;
  paymentMethodId: number;
  installmentId: number = 4;
  invoiceCreation = false;
  invoiceEditable = false;
  suppliers: any[] = [];
  branches: any[] = [];
  receivingOrders: any[] = [];
  maxDate = new Date();
  maxDueDate = new Date();

  // table
  private subscriptions = new Subscription();
  expansesDataSource: MatTableDataSource<any>;
  expansesArray: any = [];
  settlementsDataSource: MatTableDataSource<any>;
  paysDataSource: MatTableDataSource<any>;
  currentTapIndex = 0;
  invoiceSettlements: any = [];
  searchData: any = {};
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = [
    'account',
    'discription',
    'price',
    'salesTaxValue',
    'total',
    'buttons'
  ];
  instalmentDataSource: MatTableDataSource<any>;
  instalmentArray: any = [];
  instalmentDisplayedColumns = [
    'dueDate',
    'installValue',
    'paid',
    'buttons'
  ];
  settlementsDisplayedColumns = [
    'createdDate',
    'price'
  ];
  paysDisplayedColumns = [
    'account',
    'createdDate',
    'paidAmount',
    'payStatus',
    'buttons'
  ];
  protected _onDestroy = new Subject();

  constructor(
    private activateRoute: ActivatedRoute,
    private authService: AuthService,
    public _translate: TranslateService,
    private _snack: SnackService,
    private _router: Router,
    private payableInvoiceService: PayableInvoiceService,
    public constant: Constant,
    private datePipe: DatePipe,
    private locations: Location,
    private _formBuilder: UntypedFormBuilder,
    public dialog: MatDialog,
    @Inject(LOCALE_ID) public locale: string,
  ) {
    this.branchId = JSON.parse(localStorage.getItem('CURRENT_USER')).branchId;
    this.getUserBranches();
    this.setConfig();
  }




  invoicForm = new UntypedFormGroup({

    invoiceType: new UntypedFormGroup({
      id: new UntypedFormControl(null, Validators.required)
    }),
    invDate: new UntypedFormControl(new Date().toISOString().substring(0, 10), Validators.required),
    dueDate: new UntypedFormControl(new Date().toISOString().substring(0, 10), Validators.required),
    branch: new UntypedFormGroup({
      id: new UntypedFormControl(null, Validators.required)
    }),
    supplier: new UntypedFormGroup({
      id: new UntypedFormControl(null, Validators.required)
    }),
    paymentMethodId: new UntypedFormControl(null, Validators.required),
    paymentTermId: new UntypedFormControl(null, Validators.required),
    receivingOrderId: new UntypedFormControl(null),
    receivingOrder: new UntypedFormControl(null),
  });

  setConfig() {
    if (this.CONFIG) {
      this.paymentMethods = this.CONFIG.paymentMethods;
      this.paymentTerms = this.CONFIG.paymentTerms;
      this.payableInvoiceTypes = this.CONFIG.payableInvoiceTypes;
      this.payableInvoiceStatus = this.CONFIG.payableInvoiceStatus;
    }
    this.subscriptions.add(this.authService.config.subscribe((config) => {
      this.paymentMethods = config?.paymentMethods;
      this.paymentTerms = config?.paymentTerms;
      this.payableInvoiceTypes = config?.payableInvoiceTypes;
      this.payableInvoiceStatus = config?.payableInvoiceStatus;
    }));
  }

  ngOnInit(): void {

    const id = this.activateRoute.snapshot.paramMap.get('id');
    if (id === 'creation') {
      this.invoiceCreation = true;
      this.order = null;
    } else {
      this.getOrderDetailsById(+id);
      this.invoiceEditable = true;
    }
    //
  }

  paysByInvoice(payableInvoiceId) {
    if (payableInvoiceId) {
      this.payableInvoiceService.getPaysByInvoice(payableInvoiceId).subscribe((res) => {
        if (res.success) {
          this.setpaysTableDataSource(res.data);
        }
      });
    }
  }

  getOrderDetailsById(id: number) {
    this.payableInvoiceService.getPayableInvoiceDetailsById(id).subscribe((res) => {
        if (res.success) {
          this.order = res.data;
        }
      },
      (err) => console.log(err),
      () => {

        this.invoicForm.patchValue(this.order);

        if (this.order?.branch?.id) {
          this.selectedBranchId = this.order.branch.id;
          this.branchChanged(this.order.branch.id);
        }
        if (this.order?.supplier?.id) {
          this.selectedSupplierId = this.order?.supplier?.id;
        }
        if(this.selectedBranchId && this.selectedSupplierId){
          this.receivingOrdersWOutInv(this.selectedBranchId, this.selectedSupplierId);
        }

        if (this.order?.invoiceType?.id) {
          this.invoiceTypeChanged(this.order?.invoiceType?.id);
        }

        this.setPaymentTerm(this.order?.paymentTermId);
        if (this.order?.receivingOrder?.id) {
          this.invoicForm.patchValue({
            receivingOrderId: this.order.receivingOrder
          });
        }
        if (this.order.invDate) {
          this.invoicForm.patchValue(this.order.invDate);
        }
        if (this.order.dueDate) {
          this.invoicForm.patchValue(this.order.dueDate);
        }
        this.paidAmount = this.order.paidAmount;
        this.getTaxAmout(this.order?.invoiceDetails, this.order?.invoiceExpenses);
        this.getTotalAmount(this.order?.invoiceDetails, this.order?.invoiceExpenses);
        this.removeInvoiceExpensesInvDate();
        this.removeInvoiceSettlementsInvDate();
        this.getExpenses(this.order.invoiceExpenses);
        this.getSettlements(this.order.invoiceSettlements);
        this.paymentMethodId = this.order.paymentMethodId;
        if (this.order?.invoiceStatus?.code === this.constant.invoiceStatus.INSTALL) {
          this.getInstallmentsByPayableInvoiceId(this.order.id);
        }
        this.disableForm();
      }
    );
  }

  setPaymentTerm(paymentTermId) {
    if (paymentTermId) {
      this.paymentTermOnchange(paymentTermId);
    }
  }

  getTaxAmout(invoiceDetails, invoiceExpenses) {
    let taxAmount = 0;
    if (invoiceDetails && invoiceDetails?.length > 0) {
      invoiceDetails?.forEach(invoice => {
        taxAmount += invoice.salesTaxValue;
      });
    }
    if (invoiceExpenses && invoiceExpenses?.length > 0) {
      invoiceExpenses?.forEach(expense => {
        taxAmount += expense.salesTaxValue;
      });
    }
    this.taxAmount = taxAmount;
  }

  getTotalAmount(invoiceDetails, invoiceExpenses) {
    let totalAmount = 0;

    if (invoiceDetails && invoiceDetails?.length > 0) {
      invoiceDetails?.forEach(invoice => {
        totalAmount += ((invoice.price * invoice.qty) + invoice.salesTaxValue);
      });
    }

    if (invoiceExpenses && invoiceExpenses?.length > 0) {
      invoiceExpenses.forEach(expense => {
        totalAmount += (expense.price) + (expense.salesTaxValue);
      });
    }
    this.totalAmount = totalAmount;
  }


  back() {
    this.locations.back();
  }

  save(form, code?) {
    if (form.valid && form.value) {
      let formObj = form.value;
      if (this.invoiceEditable) {
        this.invoiceIsEditable(formObj, code);
      }

      if (this.invoiceCreation) {
        this.invoiceInCreation(formObj, code);
      }

    }
  }

  invoiceInCreation(formObj, code?) {
    if (this.invoiceCreation) {
      delete formObj.receivingOrderId;
      if (formObj?.receivingOrder?.id) {
        formObj.receivingOrder = {id: formObj?.receivingOrder?.id ? formObj?.receivingOrder?.id : null};
      }
      if (this.expansesArray && this.expansesArray.length) {
        formObj.invoiceExpenses = this.expansesArray;
      }

      if (code && code === 'POST') {
        if (this.expansesArray && this.expansesArray.length) {
          formObj.invoiceExpenses = this.expansesArray;

          formObj.invoiceStatus = {
            id: 4,
            code: 'POSTED'
          };
          this.createInvoice(formObj);
        } else {
          this._snack.showSnack('COMMON.CREATE_AT_LEAST_LINE', 'error');
        }
      } else {
        this.createInvoice(formObj, code);
      }

    }
  }

  invoiceIsEditable(formObj, code?) {
    if (this.invoiceEditable) {
      if (this.order) {
        this.order.paymentMethodId = formObj.paymentMethodId;
        this.order.paymentTermId = formObj.paymentTermId;
        this.order.invDate = formObj.invDate;
        this.order.dueDate = formObj.dueDate;
        this.order.branch = {id: formObj.branch.id};
        this.order.supplier = {id: formObj.supplier.id};
        if (formObj?.receivingOrder?.id) {
          this.order.receivingOrder = {id: formObj?.receivingOrder?.id ? formObj?.receivingOrder?.id : null};
        }

        this.order.invoiceType = {id: formObj?.invoiceType?.id};

        this.refactorInvoiceExpenses();

        if (code && code === 'POST') {
          if (this.order.invoiceExpenses == null) {
            this._snack.showSnack('COMMON.CREATE_AT_LEAST_LINE', 'error');
            return;
          } else {
            this.order.invoiceStatus = {
              id: 4,
              code: 'POSTED'
            };
          }
        }
        this.updateInvoice(this.order, code);
      }
    }
  }

  refactorInvoiceExpenses() {
    if (this.order?.invoiceExpenses?.length > 0) {
      this.order.invoiceExpenses = this.order.invoiceExpenses.map((expense) => {
        delete expense?.payableInvoice?.invDate;
        return expense;
      });
    } else {
      this.order.invoiceExpenses = null;
    }
    if (this.expansesArray && this.expansesArray.length > 0) {
      this.order.invoiceExpenses = this.expansesArray;
    }
  }

  removeInvoiceExpensesInvDate() {
    if (this.order?.invoiceExpenses?.length > 0) {
      this.order.invoiceExpenses = this.order.invoiceExpenses.map((expense) => {
        delete expense?.payableInvoice?.invDate;
        return expense;
      });
    } else {
      this.order.invoiceExpenses = null;
    }
  }

  removeInvoiceSettlementsInvDate() {
    if (this.order?.invoiceSettlements?.length > 0) {
      this.order.invoiceSettlements = this.order.invoiceSettlements.map((settle) => {
        delete settle?.payableInvoice?.invDate;
        delete settle?.prepaymentInvoice?.invDate;
        return settle;
      });
    } else {
      this.order.invoiceSettlements = null;
    }
  }

  createInvoice(formObj, code?) {
    this.payableInvoiceService.createInvoice(formObj).subscribe(res => {
      if (res.success) {
        this.order = res.data;
        this.invoiceCreation = false;
        this.invoiceEditable = true;
        this.invoicForm.markAsTouched();
        this.setPaymentTerm(this.order?.paymentTermId);
        this.getTaxAmout(this.order?.invoiceDetails, this.order?.invoiceExpenses);
        this.getTotalAmount(this.order?.invoiceDetails, this.order?.invoiceExpenses);
        this.disableForm();
        this.getExpenses(this.order.invoiceExpenses);
        this._snack.showSnack('COMMON.ADD_SUCCESS', 'success');
        console.log(code);

        if (code === 'CLOSE') {
          this._router.navigateByUrl(`pages/finance/payable-invoice`);
        } else if (code === 'NEW') {
          let currentUrl = this._router.url;
          console.log(currentUrl);
          this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            this._router.navigate([currentUrl]);
          });
        } else {
          this._router.navigateByUrl(`pages/finance/payable-invoice/${res.data.id}`);
        }

      }
    });
  }


  updateInvoice(order, code?) {
    this.payableInvoiceService.updateInvoice(order).subscribe(res => {
      if (res.success) {
        this.order = res.data;
        this.getSettlements(this.order.invoiceSettlements);
        this.setPaymentTerm(this.order?.paymentTermId);
        if (this.order?.invoiceStatus?.code === 'INSTALL') {
          this.getInstallmentsByPayableInvoiceId(this.order.id);
          this._snack.showSnack('PAYABLE_INVOICE.VIEW.INSTALMENT_ADDED', 'success');
        } else {
          this._snack.showSnack('COMMON.EDIT_SUCCESS', 'success');
        }
        this.getExpenses(this.order.invoiceExpenses);
        this.disableForm();
        this.getTaxAmout(this.order?.invoiceDetails, this.order?.invoiceExpenses);
        this.getTotalAmount(this.order?.invoiceDetails, this.order?.invoiceExpenses);

        if (code === 'CLOSE') {
          this._router.navigateByUrl(`pages/finance/payable-invoice`);
        } else if (code === 'NEW') {

          this._router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            this._router.navigate([`/finance/payable-invoice/creation`]);
          });
        }

      }
    });
  }

  disableForm() {
    if (this.order?.invoiceStatus?.code === this.constant.invoiceStatus.POSTED
      || this.order?.invoiceStatus?.code === this.constant.invoiceStatus.INSTALL
      || this.order?.invoiceStatus?.code === this.constant.invoiceStatus.PAID
      || this.order?.invoiceStatus?.code === this.constant.invoiceStatus.CANCELED) {
      this.invoicForm.disable();
    }
  }

  getUserBranches() {
    this.payableInvoiceService.getCustomerBranch().subscribe(res => {
      this.branches = res.data;
    });
  }

  getInstallmentsByPayableInvoiceId(payableInvoiceId) {
    this.payableInvoiceService.getInstallmentsByPayableInvoiceId(payableInvoiceId).subscribe((res) => {
      if (res.success) {
        this.instalmentArray = res.data;
        this.getInstalmentTotalPaidAmount(this.instalmentArray);
        this.setInstalmentDataSource(res.data);
      }
    });
  }

  branchChanged(event) {
    this.selectedBranchId = event;
    this.payableInvoiceService.getSuppliersByBranchId(event).subscribe(res => {
      this.suppliers = res.data;
    }, err => console.log(err));
  }

  receivingOrdersWOutInv(branchId, supplierId) {
    this.payableInvoiceService.receivingOrdersWOutInv(branchId, supplierId).subscribe(res => {
      this.receivingOrders = res.data.content;
    });
  }

  orderChanged(e) {
    this.invoicForm.patchValue({
      receivingOrder: e.value
    });
  }

  addExpense() {
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(AddExpenseDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {branchId: this.selectedBranchId ? this.selectedBranchId : this?.order?.branch?.id},
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh',
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.data?.item && res.data?.action === 'save') {
        let payableExpenses = res.data?.item;
        this.expansesArray.push(payableExpenses);
        this.setTableDataSource(this.expansesArray);
        this.invoicForm.markAsTouched();
        this.getTaxAmout(this.order?.invoiceDetails, this.expansesArray);
        this.getTotalAmount(this.order?.invoiceDetails, this.expansesArray);
      }
    });
  }

  editExpense(expense) {
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(AddExpenseDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {branchId: this.selectedBranchId ? this.selectedBranchId : this?.order?.branch?.id, expense: expense},
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh',
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.data?.item && res.data?.action === 'save') {
        if (this.order) {
          let payableExpenses = res.data?.item;
          expense.price = payableExpenses.price;
          expense.salesTaxValue = payableExpenses.salesTaxValue;
          expense.accountType = payableExpenses.accountType;
          expense.branchAccount = payableExpenses.branchAccount;
          expense.discription = payableExpenses.discription;
          this.invoicForm.markAsTouched();
          this.getTaxAmout(this.order?.invoiceDetails, this.expansesArray);
          this.getTotalAmount(this.order?.invoiceDetails, this.expansesArray);
        }
      }
    });
  }


  getExpenses(invoiceExpenses) {
    if (invoiceExpenses && invoiceExpenses.length > 0) {
      this.expansesArray = invoiceExpenses;
      this.setTableDataSource(invoiceExpenses);
    } else {
      this.expansesArray = [];
      this.setTableDataSource([]);
    }
  }

  getSettlements(invoiceSettlements) {
    if (invoiceSettlements && invoiceSettlements.length > 0) {
      this.invoiceSettlements = invoiceSettlements;
      this.setSettlementsTableDataSource(invoiceSettlements);
    } else {
      this.invoiceSettlements = [];
      this.setSettlementsTableDataSource([]);
    }
  }

  deleteDialog(expanse: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.expansesArray = this.expansesArray.filter((element) => element.id != expanse.id);
        this.setTableDataSource(this.expansesArray);
        this.getTaxAmout(this.order?.invoiceDetails, this.expansesArray);
        this.getTotalAmount(this.order?.invoiceDetails, this.expansesArray);
        this.invoicForm.markAsTouched();
      }
    });
  }

  //table data source
  setTableDataSource(source: any) {
    this.expansesDataSource = new MatTableDataSource(source);
  }

  setSettlementsTableDataSource(source: any) {
    this.settlementsDataSource = new MatTableDataSource(source);
  }

  setpaysTableDataSource(source: any) {
    this.paysDataSource = new MatTableDataSource(source);
  }

  setInstalmentDataSource(source: any) {
    this.instalmentDataSource = new MatTableDataSource(source);
  }

  paymentMethodChanged(paymentId) {
    this.paymentMethodId = paymentId;
  }

  // pay
  addPay() {
    const dialogRef = this.dialog.open(PayDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {
        branchId: this.order.branch.id,
        invoiceTotalAmount: this.totalAmount,
        totalPaidAmount: this.paidAmount
      }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res?.data?.item && res?.data?.action === 'save') {
        if (this.order) {
          let payableInvoicePay = res.data?.item;
          // draft
          payableInvoicePay.payableInvoice = {id: this.order.id};
          payableInvoicePay.invoicePayStatus = {
            id: 1,
            code: "DRAFT",
            name: "Draft"
          };
          this.payableInvoiceService.createPayableInvoicePay(payableInvoicePay).subscribe(res => {
            if (res.success) {
              if (this.currentTapIndex === 4) {
                this.paysByInvoice(this.order.id);
              }
              this.getOrderDetailsById(this.order.id);
              this._snack.showSnack('PAYABLE_INVOICE.VIEW.PAY_ADDED', 'success');
            }
          });
        }
      }
      if (res?.data?.item && res?.data?.action === 'post') {
        if (this.order) {
          let payableInvoicePay = res.data?.item;
          payableInvoicePay.payableInvoice = {id: this.order.id};
          // paid
          payableInvoicePay.invoicePayStatus = {
            id: 2,
            code: "PAID",
            name: "paid"
          };
          this.payableInvoiceService.postPayableInvoicePay(payableInvoicePay).subscribe(res => {
            if (res.success) {
              if (this.currentTapIndex === 4) {
                this.paysByInvoice(this.order.id);
              }
              this.getOrderDetailsById(this.order.id);
              this._snack.showSnack('PAYABLE_INVOICE.VIEW.PAY_POSTED_ADDED', 'success');
            }
          });
        }
      }
    });
  }

  // installmentPay
  installmentPay() {
    const dialogRef = this.dialog.open(InstalmentDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: this.totalAmount
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res?.data?.item && res?.data?.action === 'save') {
        if (this.order) {
          let instalment = res.data?.item;
          this.order.paymentPeriodId = instalment.paymentPeriodId;
          this.order.installCount = instalment.installCount;
          this.order.installValue = instalment.installValue;
          if (this.order?.invoiceExpenses?.length > 0) {
            this.order.invoiceExpenses = this.order.invoiceExpenses.map((expense) => {
              delete expense.payableInvoice.invDate;
              return expense;
            });
          } else {
            this.order.invoiceExpenses = null;
          }
          this.order.invoiceStatus = {
            id: 5,
            code: this.constant.invoiceStatus.INSTALL
          };
          this.updateInvoice(this.order);
        }
      }
    });

  }


  addInstalmentPay(instalItem) {
    const dialogRef = this.dialog.open(PayInstalmentDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {
        branchId: this.order.branch.id,
        invoiceTotalAmount: this.totalAmount,
        totalPaidAmount: this.paidAmount,
        installValue: instalItem.installValue
      }
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res?.data?.item && res.data?.action === 'save') {
        if (this.order) {
          let payableInvoiceInstalmentPay = res.data?.item;
          // payableInvoiceInstalmentPay.payableInvoice = {id: instalItem.payableInvoice.id};
          payableInvoiceInstalmentPay.payableInstallment = {id: instalItem.id};
          this.payableInvoiceService.createPayableInvoiceInstallmentPay(payableInvoiceInstalmentPay).subscribe(res => {
            if (res.success) {
              this.getInstallmentsByPayableInvoiceId(this.order.id);
              this._snack.showSnack('PAYABLE_INVOICE.VIEW.PAY_ADDED', 'success');
            }
          });
        }
      }
    });
  }

  getInstalmentTotalPaidAmount(instalmentArray) {
    this.paidAmount = 0;
    instalmentArray.forEach((instalObj) => {
      if (instalObj.isPaid) {
        this.paidAmount += instalObj.paidAmount;
      }
    });
  }

  getSettlementsTotalPaidAmount(invoiceSettlements) {
    if (invoiceSettlements && invoiceSettlements.length > 0) {
      invoiceSettlements.forEach((settle) => {
        if (settle.price) {
          this.paidAmount += settle.price;
        }
      });
    }
  }

  // invoiceSettlements


  paymentTermOnchange(paymentTermId) {
    let term = this.paymentTerms.find(paymentTerm => paymentTerm.id === paymentTermId);
    if (term.code === this.constant.PAYMENT_TERM.Immediate || term.code === this.constant.PAYMENT_TERM.Cash_next_delivery || term.code === this.constant.PAYMENT_TERM.Cash_on_delivery) {
      let today = new Date();
      this.invoicForm.patchValue({dueDate: today});
    }
    if (term.code === this.constant.PAYMENT_TERM.End_of_month) {
      let today = new Date();
      let lastDayOfMonth = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      this.maxDueDate = lastDayOfMonth;
      this.invoicForm.patchValue({dueDate: lastDayOfMonth});
    }
    if (term.code === this.constant.PAYMENT_TERM.Net10) {
      this.addDaySByNumber(10);
    }
    if (term.code === this.constant.PAYMENT_TERM.Net15) {
      this.addDaySByNumber(15);
    }
    if (term.code === this.constant.PAYMENT_TERM.Net30) {
      this.addDaySByNumber(30);
    }
    if (term.code === this.constant.PAYMENT_TERM.Net45) {
      this.addDaySByNumber(45);
    }
    if (term.code === this.constant.PAYMENT_TERM.Net60) {
      this.addDaySByNumber(60);
    }
    if (term.code === this.constant.PAYMENT_TERM.Net90) {
      this.addDaySByNumber(90);
    }
  }

  addDaySByNumber(days) {
    let today = new Date();
    today.setDate(today.getDate() + days);
    this.maxDueDate = today;
    this.invoicForm.patchValue({dueDate: today});
  }

  supplierChanged(event) {
    this.selectedSupplierId = event;
    if (this.standard_invoice && this.selectedBranchId && this.selectedSupplierId) {
      this.getSupplierBalance(this.selectedSupplierId, this.selectedBranchId);
    }
    //receivingOrdersWOutInv
    if (this.selectedBranchId && this.selectedSupplierId) {
      this.receivingOrdersWOutInv(this.selectedBranchId, this.selectedSupplierId);
    }

  }

  invoiceTypeChanged(invoiceTypeId) {
   if (this.payableInvoiceTypes && this.payableInvoiceTypes.length > 0){
     let invoiceType = this.payableInvoiceTypes.find(invoiceType => invoiceType.id === invoiceTypeId);
     if (invoiceType.code === this.constant.PAYABLE_INVOICE_TYPES.STANDARD) {
       this.standard_invoice = invoiceType;
       if (this.selectedBranchId && this.selectedSupplierId) {
         this.getSupplierBalance(this.selectedSupplierId, this.selectedBranchId);
       }
     } else {
       this.standard_invoice = null;
     }
   }
  }

  getSupplierBalance(supplierId: number, branchId: number) {
    this.payableInvoiceService.getSupplierBalance(supplierId, branchId).subscribe(res => {
      if (res.success) {
        this.supplierBalance = res.data;
      }
    });
  }

  addSettlement() {
    const endialogPosition: DialogPosition = {
      top: '0',
      right: '5',
    };
    const ardialogPosition: DialogPosition = {
      top: '0',
      left: '5',
    };
    const dialogRef = this.dialog.open(AddSettlementDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      data: {
        branchId: this.selectedBranchId ? this.selectedBranchId : this?.order?.branch?.id
        , supplierId: this.selectedSupplierId ? this.selectedSupplierId : this?.order?.supplier?.id
      },
      position: this._translate.currentLang === 'ar' ? ardialogPosition : endialogPosition,
      disableClose: true,
      height: '100vh',
      panelClass: 'custom-drawer'
    });
    dialogRef.afterClosed().subscribe((res) => {
      if (res.data?.settlement && res.data?.action === 'save') {
        let settlement = res.data?.settlement;
        settlement = settlement.map(item => {
          item.payableInvoice = {id: this.order.id};
          item.prepaymentInvoice = {id: item.id};
          item.salesTaxValue = 0;
          delete item.createdDate;
          delete item.createdFromChannelId;
          delete item.expensesTotalAmount;
          delete item.expensesTotalTax;
          delete item.id;
          delete item.invDate;
          delete item.invNumber;
          delete item.isChecked;
          delete item.settlementTotalAmount;
          delete item.settlementTotalTax;
          delete item.statusCode;
          return item;
        });


        // settlement.forEach((item) => {
        //   if (this.invoiceSettlements && this.invoiceSettlements.length > 0) {
        //     this.invoiceSettlements.forEach((invSettle) => {
        //       if (item.prepaymentInvoice.id === invSettle.prepaymentInvoice.id) {
        //         console.log(item)
        //       } else {
        //         this.invoiceSettlements.push(item)
        //       }§
        //     })
        //   }else {
        //     this.invoiceSettlements.push(item)
        //   }
        // });
        this.invoiceSettlements.push(...settlement);
        this.getSettlementsTotalPaidAmount(settlement);
        if (this.invoiceSettlements && this.invoiceSettlements.length > 0) {
          this.removeInvoiceExpensesInvDate();
          this.removeInvoiceSettlementsInvDate();
          this.order.invoiceSettlements = this.invoiceSettlements;
          this.updateInvoice(this.order);
        }
      }
    });
  }

  ChangeTap(e) {
    if (e.index === 4) {
      this.currentTapIndex = 4;
      this.paysByInvoice(this.order.id);
    }
  }

  cancelInvoice() {
    if (this.order) {
      this.order.invoiceStatus = {
        id: 3,
        code: this.constant.invoiceStatus.CANCELED
      };
      this.cancelPayableInvoice(this.order);
    }
  }

  cancelPayableInvoice(invoice) {
    this.payableInvoiceService.cancelPayableInvoice(invoice).subscribe(res => {
      if (res.success) {
        this.order = res.data;
        this._snack.showSnack('PAYABLE_INVOICE.VIEW.INVOICE_CANCELLED_SUCCESS', 'success');
      }
    });
  }

  postPayableInvoicePay(pay){
    pay.payableInvoice = {id: this.order.id};
    pay.invoicePayStatus = {
      id: 2,
      code: "PAID",
      name: this._translate.currentLang === 'ar' ? "مدفوع" : "paid"
    };
    this.payableInvoiceService.postPayableInvoicePay(pay).subscribe(res => {
      if (res.success) {
        console.log(res);
        this._snack.showSnack('PAYABLE_INVOICE.VIEW.PAY_POSTED_ADDED', 'success');
      }
    });
  }

  cancelPayableInvoicePay(pay){
    pay.payableInvoice = {id: this.order.id};
    pay.invoicePayStatus = {
      id: 3,
      code: "CANCELED",
      name: this._translate.currentLang === 'ar' ? "ألغيت" : "canceled"
    };
    this.payableInvoiceService.cancelPayableInvoicePay(pay).subscribe(res => {
      if (res.success) {
        console.log(res);
        this._snack.showSnack('PAYABLE_INVOICE.VIEW.PAY_POSTED_CANCELLED', 'success');
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }


}


