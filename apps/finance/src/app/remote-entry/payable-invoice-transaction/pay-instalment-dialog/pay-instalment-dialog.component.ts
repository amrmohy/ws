import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import { Constant } from '@khdma-frontend-nx/shared';
import {PayableInvoiceService} from '../../services/payable-invoice.service';
import {formatNumber} from '@angular/common';

@Component({
  selector: 'pay-instalment-dialog',
  templateUrl: './pay-instalment-dialog.component.html',
  styleUrls: ['./pay-instalment-dialog.component.scss']
})
export class PayInstalmentDialogComponent implements OnInit {
  AccountsData: any[] = [];
  maxDate = new Date();

  constructor(
    private payDialogRef: MatDialogRef<PayInstalmentDialogComponent>,
    private constant: Constant,
    private payableInvoiceService: PayableInvoiceService,
    @Inject(LOCALE_ID) public locale: string,
    @Inject(MAT_DIALOG_DATA) public invoiceTotal: any
  ) {
    this.getPayableAccounts(this.invoiceTotal.branchId)
  }



  getPayableAccounts(branchId){
    this.payableInvoiceService.getPayableAccounts(branchId).subscribe((res) => {
      this.AccountsData = res.data;
    })
  }

  ngOnInit(): void {
    this.item.patchValue({paidAmount: formatNumber(this.invoiceTotal.installValue,this.locale,'1.2-2')});
  }

  item = new UntypedFormGroup({
    date: new UntypedFormControl(new Date().toISOString().substring(0, 10), Validators.required),
    paidAmount: new UntypedFormControl(null, Validators.required),
    account: new UntypedFormControl(null),
  });


  get Validation() {
    return this.item.controls;
  }

  public hasError = (controlName, errorName) => {
    return this.Validation[controlName].hasError(errorName);
  };


  //on item submit
  onSubmit() {
    let item = this.item.getRawValue();
    delete item.accountId;
    this.payDialogRef.close({
      data: {
        action: 'save',
        item: item
      },
    });
  }

  //on item Cancel
  onCancel() {
    this.payDialogRef.close({
      data: {
        action: 'cancel',
        item: this.item.value
      },
    });
  }
  public objectComparisonFunction = function(option, value): boolean {
    return option?.id === value?.id;
  };
}
