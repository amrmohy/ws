import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

export interface Cards {
  urlNavigate: string;
  title: string;
  icon: string;
  details: string;
  color: string;
}

@Component({
  selector: 'finance-transaction',
  templateUrl: './finance-transaction.component.html',
  styleUrls: ['./finance-transaction.component.scss'],
})
export class FinanceTransactionComponent implements OnInit {
  branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  cards: Cards[] = [
    {
      urlNavigate: '/finance/payable-invoice',
      title: 'SETUP_HOME.INV_TRANS.PAYABLE_INVOICE',
      icon: 'receipt',
      details: 'SETUP_HOME.INV_TRANS.PAYABLE_INVOICE_DETAILS',
      color: '#5e35b1'
    },
    {
      urlNavigate: '/finance/journal',
      title: 'SETUP_HOME.INV_TRANS.JOURNALS',
      icon: 'receipt_long',
      details: 'SETUP_HOME.INV_TRANS.JOURNALS_DETAILS',
      color: '#1d2d88'
    },
    {
      urlNavigate: '/finance/cash-transfer',
      title: 'CASH_TRANSFER.TITLE',
      icon: 'payments',
      details: 'CASH_TRANSFER.TITLE',
      color: '#328d05'
    }
  ];

  constructor(
    private _router: Router,
    public _translate: TranslateService
  ) {

  }

  ngOnInit(): void {
  }

  /**
   * navigate to  customer profile
   */
  navigateToAccount() {
    const currentUser = JSON.parse(window.localStorage.getItem('CURRENT_USER'));
    this._router.navigateByUrl(
      `pages/user-management/user-profile`
    );
  }

}
