import { Component } from '@angular/core';

@Component({
  selector: 'khdma-frontend-nx-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {}
