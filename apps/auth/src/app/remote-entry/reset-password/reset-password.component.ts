import {TranslateService} from '@ngx-translate/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService, MustMatch, SnackService} from "@khdma-frontend-nx/shared";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnDestroy {
  passwordPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$";
  private subscriptions = new Subscription();
  form:UntypedFormGroup = this.fb.group({
    newPassword: [null, [Validators.required, Validators.pattern(this.passwordPattern)]],
    passwordConfirm: [null,[Validators.required, Validators.pattern(this.passwordPattern)]]
    // newPassword:[null, Validators.required],
    // passwordConfirm:[null, Validators.required],
  }, {
    validator: MustMatch('newPassword', 'passwordConfirm')
  });

  code: string;
  canLoadForm: boolean;
  userId: number;


  constructor(
    public _translate: TranslateService,
    private _auth: AuthService,
    public _snack: SnackService,
    public _route: ActivatedRoute,
    public _router: Router,
    private fb:UntypedFormBuilder
  ) {
    this.code = this._route.snapshot.queryParamMap.get('code');
    console.log( this.code)
    this.subscriptions.add(
      this._auth.checkResetLink(this.code).subscribe((res) => {
        this.canLoadForm = res?.success ? true : false;
        this.userId = res.data.id;
      })
    );
    this.checkResetLink();
  }

  checkResetLink(){
    this.subscriptions.add(
      this._auth.checkResetLink(this.code).subscribe((res) => {
        if (res?.success) {

          return true
        }
        this._snack.showSnack('RESET_PASS.link_error', 'error');
        this._router.navigate(['/index']);
      })
    )

  }
  onSubmit(): void {

      const query = {
        newPassword: this.form.controls.newPassword.value,
        code: this.code,
      };

      this.subscriptions.add(
        this._auth.changePassword(query).subscribe((res) => {
          if (res?.success) {
            this._snack.showSnack('RESET_PASS.SUCCESS_MESSAGE', 'success');
            this._router.navigate(['/index']);
          }
        })
      );

  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
