import {ActivatedRoute} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import { AuthService } from '@khdma-frontend-nx/shared';

@Component({
  selector: 'email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.scss']
})
export class EmailVerificationComponent implements OnInit {
  showSuccessMsg: boolean = false;
  constructor(private route: ActivatedRoute, private _authService: AuthService) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params.code) {
          this._authService.verifyEmail(params.code).subscribe(res => {
            this.showSuccessMsg = res.data;
          })
        }
      });
  }

}
