import {MatDialog} from '@angular/material/dialog';
import {Subject, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {OwlOptions} from 'ngx-owl-carousel-o';
import {MatMenuTrigger} from '@angular/material/menu';
import {SnackService, VideosDialogComponent} from "@khdma-frontend-nx/shared";

@HostListener('window:scroll', ['$event'])
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],

})
export class IndexComponent implements OnDestroy, OnInit {
  currLang = this._translate.currentLang
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  lang = localStorage.getItem('LANG') ? localStorage.getItem('LANG') : 'ar';
  @ViewChild('stickyHeader') stickyHeader: ElementRef<HTMLInputElement>;
  public indexHeader = 'indexHeader';
  sliderInfo = [
    {image:"./assets/images/login/SliderPics/1.jpg",
     title:"LOGIN.LOGIN_SLIDER_TITLE1",
     desc:"LOGIN.LOGIN_SLIDER_DESC1"
    },
    {image:"./assets/images/login/SliderPics/2.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE2",
      desc:"LOGIN.LOGIN_SLIDER_DESC2"
    },
    {image:"./assets/images/login/SliderPics/3.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE3",
      desc:"LOGIN.LOGIN_SLIDER_DESC3"
    },
    {image:"./assets/images/login/SliderPics/4.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE4",
      desc:"LOGIN.LOGIN_SLIDER_DESC4"
    },
    {image:"./assets/images/login/SliderPics/5.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE5",
      desc:"LOGIN.LOGIN_SLIDER_DESC5"
    },
    {image:"./assets/images/login/SliderPics/6.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE6",
      desc:"LOGIN.LOGIN_SLIDER_DESC6"
    },
    {image:"./assets/images/login/SliderPics/7.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE7",
      desc:"LOGIN.LOGIN_SLIDER_DESC7"
    },
    {image:"./assets/images/login/SliderPics/8.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE8",
      desc:"LOGIN.LOGIN_SLIDER_DESC8"
    },
    {image:"./assets/images/login/SliderPics/9.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE9",
      desc:"LOGIN.LOGIN_SLIDER_DESC9"
    },
    {image:"./assets/images/login/SliderPics/10.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE10",
      desc:"LOGIN.LOGIN_SLIDER_DESC10"
    },
    {image:"./assets/images/login/SliderPics/11.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE11",
      desc:"LOGIN.LOGIN_SLIDER_DESC11"
    },
    {image:"./assets/images/login/SliderPics/12.jpg",
      title:"LOGIN.LOGIN_SLIDER_TITLE12",
      desc:"LOGIN.LOGIN_SLIDER_DESC12"
    },
  ]


  flip: string = 'inactive';
  mobileQuery: MediaQueryList;
  customOptions: OwlOptions = {
    dots: true,
    nav: false,
    // navSpeed: 6000,
    smartSpeed: 4500,
    loop: true,
    items: 1,
    autoplay: true,
    // autoplayTimeout: 2000,
    responsive: {
      0: {
        items: 1,
      },

      2000: {
        items: 1,
      }
    }

  };


  private subscriptions = new Subscription();

  constructor(
    public _translate: TranslateService,
    public _snack: SnackService,
    public dialog: MatDialog,
  ) {
  }
ngOnInit() {
}

  // fillImagesArr() {
  //   let baseSliderImagesPath ='./assets/images/index/SliderPics/'
  //   for (let index = 1; index <= 12; index++) {
  //     this.sliderInfo.sliderImages.push(baseSliderImagesPath + index + '.jpg');
  //   }
  // }
  // fillTextArr() {
  //   let baseSliderTextPath ='LOGIN.LOGIN_SLIDER_TITLE'
  //   for (let index = 1; index <= 12; index++) {
  //     this.sliderText.push(baseSliderTextPath + index);
  //   }
  //   console.log(this.sliderText);
  // }

  openVideosDialog() {
    this.dialog.open(VideosDialogComponent, {
      direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
      width: '500px',
      height: '300px'
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }


  scrollToServices() {
    let elmnt = document.getElementById('servicesSection');
    elmnt?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest'
    });
  }

}



