import {Component, OnInit, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {animate, state, style, transition, trigger,} from '@angular/animations';
import {Location} from "@angular/common";
import {MatDialog} from '@angular/material/dialog';
import {AuthService, Constant, ItemAttach, SnackService, UploadFilesComponent} from '@khdma-frontend-nx/shared';


declare var google;

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss'],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(179deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(0)'
      })),
      transition('active => inactive', animate('500ms ease-out')),
      transition('inactive => active', animate('500ms ease-in'))
    ])
  ]

})


export class RegisterDialogComponent implements OnInit {
  @ViewChild(UploadFilesComponent, { static: false })
  private uploadFilesComponent: UploadFilesComponent;

  basicInformation: UntypedFormGroup;
  thirdInformation: UntypedFormGroup;
  subscriptions = new Subscription();
  bankList: any[] = [];
  location: any = {};
  map: any;
  markers: any;
  address: any = {};
  countries: any[] = [];
  governorates: any[] = [];
  cities: any[] = [];
  isUploadedBranchImg:boolean;
  brachImageUrl: string;
  itemAttachments: ItemAttach[] = [];
  uploadInfo: any;

  constructor(fb: UntypedFormBuilder,
     private locations: Location,
     private snack:SnackService,
     public translate: TranslateService,public constant: Constant,
     private authService:AuthService,
     public dialog: MatDialog
  ) {

    this.basicInformation = fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', Validators.required],
      nationalId:['',Validators.required],
      customerLegalEntityName: ['', Validators.required],
      customerLegalEntityNumber: ['', Validators.required],

    })

    this.thirdInformation = fb.group({
      country: ['', Validators.required],
      governorate: ['', Validators.required],
      city: ['', Validators.required],
    })

  }

  ngOnInit(): void {
    this.geolocation();
    this.getCountry();
    this.selectCountry();
    this.selectGovernorate();

  }

  get Validation() { return this.basicInformation.controls }

  get validationThirdInformation() {
    return this.thirdInformation.controls;
  }

 public hasError = (controlName,errorName)=>{
    return this.Validation[controlName].hasError(errorName)
  }
  public hasInformationError = (controlName,errorName)=>{
    return this.validationThirdInformation[controlName].hasError(errorName)
  }
  getCountry(){
    this.authService.getCountries().subscribe((res:any)=>{
      this.countries = res.data
    })
  }
  // get loction
  geolocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.location = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
      this.loadMap();
    });

  }



  // Choose country
  selectCountry() {

    this.subscriptions.add(
      this.thirdInformation.get('country').valueChanges.subscribe(() => {
        const selectCountryId = this.validationThirdInformation.country.value;
        if (selectCountryId > 0) {
          const country = this.countries.find(country => country.id == selectCountryId);
          this.governorates = country.governorates;
        }
      })
    );
  }

  // Choose governorate
  selectGovernorate() {
    this.subscriptions.add(
      this.thirdInformation.get('governorate').valueChanges.subscribe(() => {
        const governorateId = this.thirdInformation.controls.governorate.value;
        if (governorateId > 0) {
          const governorate = this.governorates.find(country => country.id == governorateId);
          this.cities = governorate.cities;
        }
      }));
  }

  //load map
  loadMap() {
    this.map = new google.maps.Map(document.getElementById("map")!, {
      zoom: 16,
      center: this.location,
    });
    this.addMarker();
    let that = this;
    this.map.addListener("click", (mapsMouseEvent) => {
      that.location = mapsMouseEvent.latLng;
      that.location = { lat: Number(that.location.toJSON().lat), lng: Number(that.location.toJSON().lng) }
      that.removeMarkers();
      that.addMarker();
    });
  }

  // Add marker on map
  addMarker() {
    this.markers = []
    const marker = new google.maps.Marker({
      position: this.location,
      animation: google.maps.Animation.DROP,
      zoom: 16,
      map: this.map,
    })
    this.markers.push(marker)
    return this.markers
  }

  //remove markers
  removeMarkers() {
    this.markers[0].setMap(null)
  }

  //create customer account
  createCustomerAccount() {
    const query = {
      ...this.basicInformation.value,
      address: {
        country: { id: this.thirdInformation.controls.country.value },
        governorate: { id: this.thirdInformation.controls.governorate.value },
        city: { id: this.thirdInformation.controls.city.value },
        lat: this.location.lat,
        lon: this.location.lng,

      }
    }
    this.authService.registerCustomer(query).subscribe(res => {

      this.snack.showSnack('COMMON.ADD_SUCCESS', 'success');
      this.dialog.closeAll();
    })
  }
}



