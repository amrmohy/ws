import {Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChange, ViewChild} from '@angular/core';
import {MatMenuTrigger} from '@angular/material/menu';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-index-header',
  templateUrl: './index-header.component.html',
  styleUrls: ['./index-header.component.scss']
})
export class IndexHeaderComponent implements OnInit{
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  lang = localStorage.getItem('LANG') ? localStorage.getItem('LANG') : 'ar';
  @ViewChild('stickyHeader') stickyHeader: ElementRef<HTMLInputElement>;
  flip: string = 'inactive';
  mobileQuery: MediaQueryList;
  constructor(public _translate : TranslateService) {
  }

  ngOnInit(): void {
      window.addEventListener('scroll', this.scroll, true)
  }
  scroll = (): void => {
    let scrollHeight;
//
    if(window.innerWidth < 350){
      scrollHeight = 150;
    }else if(window.innerWidth < 500 && window.innerWidth > 350){
      scrollHeight = 250;
    }else if(window.innerWidth < 700 && window.innerWidth > 500){
      scrollHeight = 350;
    }else if(window.innerWidth < 1000 && window.innerWidth > 700){
      scrollHeight = 500;
    }else{
      scrollHeight = 480;
    }

    if(window.scrollY >= scrollHeight){
      // document.body.style.setProperty('--navbar-scroll', "linear-gradient(90deg, rgba(48,4,88,1) 0%, rgba(135,49,212,1) 35%, rgba(48,4,88,1) 100%)");
      document.body.style.setProperty('--navbar-scroll', "#300458");
      document.body.style.setProperty('--navbar-scroll-height', "62px");
      document.body.style.setProperty('--navbar-scroll-text', "white");
      document.body.style.setProperty('--navbar-scroll-btn-border', "1px solid #ffff");
      document.body.style.setProperty('--navbar-scroll-btn-back', "transparent");
      document.body.style.setProperty('--navbar-scroll-shadow', "rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px");
    }else if(window.scrollY < scrollHeight){
      document.body.style.setProperty('--navbar-scroll', "transparent");
      document.body.style.setProperty('--navbar-scroll-height', "0px");
      document.body.style.setProperty('--navbar-scroll-text', "white");
      document.body.style.setProperty('--navbar-scroll-btn-border', "none");
      document.body.style.setProperty('--navbar-scroll-btn-back', "#8731d4");
      document.body.style.setProperty('--navbar-scroll-shadow', "none");
    }
  }

  setMainStyle() {
    document.body.style.setProperty('--navbar-scroll', "#300458");
    document.body.style.setProperty('--navbar-scroll-height', "62px");
    document.body.style.setProperty('--navbar-scroll-text', "white");
    document.body.style.setProperty('--navbar-scroll-btn-border', "1px solid #ffff");
    document.body.style.setProperty('--navbar-scroll-btn-back', "transparent");
    document.body.style.setProperty('--navbar-scroll-shadow', "rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px");
  }

  changeLanguage() {
    const lang = this._translate.currentLang === 'en' ? 'ar' : 'en';
    this._translate.use(lang);
    localStorage.setItem('LANG', lang);
    location.reload();
    this.trigger.closeMenu();
    // this.fillImagesArr();
  }
}
