import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-index-service-card',
  templateUrl: './index-service-card.component.html',
  styleUrls: ['./index-service-card.component.scss']
})
export class IndexServiceCardComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit(): void {
  }

}
