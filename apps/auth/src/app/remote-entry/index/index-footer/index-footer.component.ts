import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-index-footer',
  templateUrl: './index-footer.component.html',
  styleUrls: ['./index-footer.component.scss']
})
export class IndexFooterComponent implements OnInit {
@Input() lang:any;
  constructor() { }

  ngOnInit(): void {
  }

}
