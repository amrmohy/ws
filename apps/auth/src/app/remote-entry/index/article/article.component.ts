import {Component, OnChanges, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {environment} from "@env/environment";
import {ArticlesService} from "@khdma-frontend-nx/shared";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  currLang = this._translate.currentLang;
  articles;
  articleDetails;
  articleId;
  baseImageUrl = environment.baseImageUrl;
  idParam;

  constructor(public _translate: TranslateService,
              private _articleService: ArticlesService,
              private route: ActivatedRoute,
              private router: Router) {
    this.articleId = this.route.snapshot.paramMap.get('id');
    this.route.params.subscribe(
      (params: Params) => {
        this.getArticleById(params['id']);
        this.articleId = params['id']
        console.log('changed', this.articleId);
      });
    this.getArticles();

  }

  ngOnInit(): void {
  }

  getArticles() {
    this._articleService.getArticles().subscribe((res) => {
      let articles = res.data.slice(0, 5);
      this.articles = articles.filter((article) => article.id !== this.articleDetails?.id);
      console.log(res);
    });
  }

  getArticleById(articleId) {
    this._articleService.getArticleDetails(articleId).subscribe((res) => {
      this.articleDetails = res.data;
      console.log(res);
    });
  }


  navigateToArticle(articleId) {
    this.router.navigateByUrl(this.router.url.replace(this.articleId, articleId));
    console.log(articleId);
  }

  // scrollToServices() {
  //   this.router.navigateByUrl('/index');
  //   let element = document.getElementById('articlesSection');
  //   element?.scrollIntoView({
  //     behavior: 'smooth',
  //     block: 'end',
  //     inline: 'end'
  //   });
  // }

}
