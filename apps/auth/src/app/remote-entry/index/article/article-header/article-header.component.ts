import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatMenuTrigger} from '@angular/material/menu';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-article-header',
  templateUrl: './article-header.component.html',
  styleUrls: ['./article-header.component.scss']
})
export class ArticleHeaderComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  lang = localStorage.getItem('LANG') ? localStorage.getItem('LANG') : 'ar';
  @ViewChild('stickyHeader') stickyHeader: ElementRef<HTMLInputElement>;
  flip: string = 'inactive';
  mobileQuery: MediaQueryList;
  constructor(public _translate : TranslateService) { }

  ngOnInit(): void {
  }
  changeLanguage() {
    const lang = this._translate.currentLang === 'en' ? 'ar' : 'en';
    this._translate.use(lang);
    localStorage.setItem('LANG', lang);
    location.reload();
    this.trigger.closeMenu();
    // this.fillImagesArr();
  }
}
