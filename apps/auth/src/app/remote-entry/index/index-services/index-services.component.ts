import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {OwlOptions} from 'ngx-owl-carousel-o';
import {environment} from "@env/environment";
import {ArticlesService} from "@khdma-frontend-nx/shared";

@Component({
  selector: 'app-index-services',
  templateUrl: './index-services.component.html',
  styleUrls: ['./index-services.component.scss'],
})
export class IndexServicesComponent implements OnInit {


  services = [
    {
      icon: './assets/images/login/services/cloud-computing.png',
      title: 'LOGIN.LOGIN_SERVICE.CLOUD_PLATFORM',
      description: 'LOGIN.LOGIN_SERVICE.CLOUD_PLATFORM_DESC',
    },
    {
      icon: './assets/images/login/services/support.png',
      title: 'LOGIN.LOGIN_SERVICE.SUPPORT',
      description: 'LOGIN.LOGIN_SERVICE.SUPPORT_DESC',
    },

    {
      icon: './assets/images/login/services/pos.png',
      title: 'LOGIN.LOGIN_SERVICE.POS',
      description: 'LOGIN.LOGIN_SERVICE.POS_DESC',
    },
    {
      icon: './assets/images/login/services/package.png',
      title: 'LOGIN.LOGIN_SERVICE.HOME_DELIVERY',
      description: 'LOGIN.LOGIN_SERVICE.HOME_DELIVERY_DESC',
    },

    {
      icon: './assets/images/login/services/budget.png',
      title: 'LOGIN.LOGIN_SERVICE.E_INVOICE',
      description: 'LOGIN.LOGIN_SERVICE.E_INVOICE_DESC',
    },
    {
      icon: './assets/images/login/services/tax.png',
      title: 'LOGIN.LOGIN_SERVICE.TAX',
      description: 'LOGIN.LOGIN_SERVICE.TAX_DESC',
    },

    {
      icon: './assets/images/login/services/register.png',
      title: 'LOGIN.LOGIN_SERVICE.HR',
      description: 'LOGIN.LOGIN_SERVICE.HR_DESC',
    },
    {
      icon: './assets/images/login/services/tablet&mobile.png',
      title: 'LOGIN.LOGIN_SERVICE.USER_INTERFACE',
      description: 'LOGIN.LOGIN_SERVICE.USER_INTERFACE_DESC',
    },

    {
      icon: './assets/images/login/services/online-shop.png',
      title: 'LOGIN.LOGIN_SERVICE.ECOMMERCE',
      description: 'LOGIN.LOGIN_SERVICE.ECOMMERCE_DESC',
    },

    {
      icon: './assets/images/login/services/security.png',
      title: 'LOGIN.LOGIN_SERVICE.SECURED',
      description: 'LOGIN.LOGIN_SERVICE.SECURED_DESC',
    },

    {
      icon: './assets/images/login/services/infrastructure.png',
      title: 'LOGIN.LOGIN_SERVICE.INFRASTRUCTURE',
      description: 'LOGIN.LOGIN_SERVICE.INFRASTRUCTURE_DESC',
    },
    {
      icon: './assets/images/login/services/profile.png',
      title: 'LOGIN.LOGIN_SERVICE.REGISTRATION',
      description: 'LOGIN.LOGIN_SERVICE.REGISTRATION_DESC',
    },


  ];
  beforeLastService = {
    icon: './assets/images/login/services/inventory.png',
    title: 'LOGIN.LOGIN_SERVICE.INTEGRATED_WAREHOUSE',
    description: 'LOGIN.LOGIN_SERVICE.INTEGRATED_WAREHOUSE_DESC',
  };
  lastService = {
    icon: './assets/images/login/services/financial.png',
    title: 'LOGIN.LOGIN_SERVICE.AUTOMATIC_FINANCIAL',
    description: 'LOGIN.LOGIN_SERVICE.AUTOMATIC_FINANCIAL_DESC',
  };
  servicesRes = [

    // {
    //   icon: './assets/images/index/services/budget.png',
    //   title: 'LOGIN.LOGIN_SERVICE.CALCULATE_COST',
    //   description: 'LOGIN.LOGIN_SERVICE.CALCULATE_COST_DESC',
    // },
    {
      icon: './assets/images/login/services/menu.png',
      title: 'LOGIN.LOGIN_SERVICE.ELEC_FOOD_MENU',
      description: 'LOGIN.LOGIN_SERVICE.ELEC_FOOD_MENU_DESC',
    },
    {
      icon: './assets/images/login/services/moving-home.png',
      title: 'LOGIN.LOGIN_SERVICE.INSIDE',
      description: 'LOGIN.LOGIN_SERVICE.INSIDE_DESC',
    },
    {
      icon: './assets/images/login/services/budget.png',
      title: 'LOGIN.LOGIN_SERVICE.MANUFACTURING',
      description: 'LOGIN.LOGIN_SERVICE.MANUFACTURING_DESC',
    },
  ];

  lastResService = {
    icon: './assets/images/login/services/dashboard.png',
    title: 'LOGIN.LOGIN_SERVICE.DESIGN_PLACE',
    description: 'LOGIN.LOGIN_SERVICE.DESIGN_PLACE_DESC',
  };
  articles: any;
  baseImageUrl = environment.baseImageUrl;
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    autoplay:true,
    navSpeed: 600,
    navText: ['&#8249', '&#8250;'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      760: {
        items: 3
      },
      1000: {
        items: 4
      }
    },
    nav: true
  }
  constructor(public _translate: TranslateService,
              private _articleService: ArticlesService,
              ) {
    this.getArticles();

  }
  ngOnInit(): void {
  }

  getArticles() {
   this._articleService.getArticles().subscribe((res) => {
      this.articles = res.data
      console.log(res);
    });
  }
}
