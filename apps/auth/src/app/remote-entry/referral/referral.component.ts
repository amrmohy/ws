import {Component, OnInit, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {animate, state, style, transition, trigger,} from '@angular/animations';
import {MatMenuTrigger} from '@angular/material/menu';
import {MatDialog} from '@angular/material/dialog';
import {AuthService} from "@khdma-frontend-nx/shared";


@Component({
  selector: 'app-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.scss'],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(179deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(0)'
      })),
      transition('active => inactive', animate('500ms ease-out')),
      transition('inactive => active', animate('500ms ease-in'))
    ])
  ]

})


export class ReferralComponent implements OnInit {
  referralForm: UntypedFormGroup;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  flip: string = 'inactive';

  private subscriptions = new Subscription();

  constructor(private fb: UntypedFormBuilder,
              public dialog: MatDialog,
              public _translate: TranslateService,
              private _auth: AuthService,
              private _router: Router) {
  }


  ngOnInit(): void {
    this.createReferralForm();
  }

  createReferralForm = () => {
    this.referralForm = this.fb.group({
      accountManagerPhoneNumber: [''],
      customerPhoneNumber: ['']
    });
  };

  toggleFlip() {
    this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
  }


  onSubmit(): void {
    let referral = this.referralForm.value;
    console.log(referral);
    let branchId = sessionStorage.getItem('id');
    if ((referral.accountManagerPhoneNumber || referral.customerPhoneNumber) && branchId) {
      this.subscriptions.add(
        this._auth.getreferralSetting(branchId, referral.accountManagerPhoneNumber, referral.customerPhoneNumber).subscribe((res) => {
            if (res.success) {
              sessionStorage.removeItem('id');
              this._router.navigate(['/index/login']);
            }
          },
          ({error}) => {
            console.log(error);
          },
          () => {

          }
        )
      );
    }


  }


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
