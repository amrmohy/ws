import {Component, OnInit, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {animate, state, style, transition, trigger,} from '@angular/animations';
import {MatMenuTrigger} from '@angular/material/menu';
import {MatDialog} from '@angular/material/dialog';
import {ForgetPasswordComponent} from '../forget-password/forget-password.component';
import {AuthService} from "@khdma-frontend-nx/shared";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(179deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(0)'
      })),
      transition('active => inactive', animate('500ms ease-out')),
      transition('inactive => active', animate('500ms ease-in'))
    ])
  ]

})


export class LoginComponent implements OnInit {
  loginForm: UntypedFormGroup;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  flip: string = 'inactive';
  showPasswordText: boolean = true;
  private subscriptions = new Subscription();

  constructor(private fb:UntypedFormBuilder ,public dialog: MatDialog, public _translate: TranslateService,  private _auth: AuthService,private _router: Router) { }


  ngOnInit(): void {
    this.createLoginForm();
  }

  createLoginForm = () => {
    this.loginForm = this.fb.group({
      userId: ['',Validators.required],
      password: ['',Validators.required]
    })
  }

  toggleFlip() {
    this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
  }


  onSubmit(): void {
    let result = null;
    this.subscriptions.add(
      this._auth.login(this.loginForm.value).subscribe((res) => {
        result = res ;
          // this._router.navigate(['/dashboards/analytics']);
        },
        ({ error }) => {},
        () => {
          if(result && result.success){
          this.dialog.closeAll();
          this._router.navigate(['/dashboards/analytics']);
        }
      }
      )
    );

  }

  openForgetPassDialog() {
  this.dialog.open(ForgetPasswordComponent,{
    direction: this._translate.currentLang === 'ar' ? 'rtl' : 'ltr',
  })
  }
  changeLanguage() {
    const lang = this._translate.currentLang === 'en' ? 'ar' : 'en';
    this._translate.use(lang);
    localStorage.setItem('LANG', lang);
    location.reload();
    this.trigger.closeMenu();
    // this.fillImagesArr();
  }
}
