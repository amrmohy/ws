import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RemoteEntryComponent } from './entry.component';
import { NxWelcomeComponent } from './nx-welcome.component';
import {IndexComponent} from "./index/index.component";
import {ForgetPasswordComponent} from "./forget-password/forget-password.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";
import {EmailVerificationComponent} from "./email-verification/email-verification.component";
import {TermsConditionsComponent} from "./terms-conditions/terms-conditions.component";
import {PrivacyPolicyComponent} from "./privacy-policy/privacy-policy.component";
import {ContactUsComponent} from "./contact-us/contact-us.component";
import {WelcomeDialogComponent} from "./index/welcome-dialog/welcome-dialog.component";
import {LoginComponent} from "./login/login.component";
import {RegisterDialogComponent} from "./index/register-dialog/register-dialog.component";
import {RegisterComponent} from "./register/register.component";
import {IndexFooterComponent} from "./index/index-footer/index-footer.component";
import {IndexServicesComponent} from "./index/index-services/index-services.component";
import {IndexServiceCardComponent} from "./index/index-service-card/index-service-card.component";
import {ReferralComponent} from "./referral/referral.component";
import {ArticleComponent} from "./index/article/article.component";
import {IndexHeaderComponent} from "./index/index-header/index-header.component";
import {ArticleHeaderComponent} from "./index/article/article-header/article-header.component";
import {NgxCaptchaModule} from "ngx-captcha";
import {EntryRoutingModule} from "./entry-routing.module";
import {onlyNumberDirective, SharedModule} from "@khdma-frontend-nx/shared";

@NgModule({
  declarations: [
    RemoteEntryComponent,
    NxWelcomeComponent,
    IndexComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    EmailVerificationComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent ,
    ContactUsComponent ,
    WelcomeDialogComponent,
    LoginComponent,
    RegisterDialogComponent,
    RegisterComponent,
    EmailVerificationComponent,
    IndexFooterComponent,
    IndexServicesComponent,
    IndexServiceCardComponent,
    ReferralComponent,
    ArticleComponent,
    IndexHeaderComponent,
    ArticleHeaderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    EntryRoutingModule,
    NgxCaptchaModule
  ],
  providers: [onlyNumberDirective],
})
export class RemoteEntryModule {}
