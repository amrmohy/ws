import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {Component, OnDestroy} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AuthService, SnackService} from "@khdma-frontend-nx/shared";


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss'],
})
export class ForgetPasswordComponent implements OnDestroy {
  private subscriptions = new Subscription();
  form = new UntypedFormGroup({
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
  });

  constructor(
    public _translate: TranslateService,
    private _auth: AuthService,
    public _snack: SnackService,
    public dialog: MatDialog
  ) {}

  onSubmit(): void {
    this.subscriptions.add(
      this._auth.forget(this.form.controls.email.value).subscribe((res) => {
        if (res?.success) {
          this.dialog.closeAll()
          this._snack.showSnack('FORGET.MAIL_SENT_MESSAGE', 'success');
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
