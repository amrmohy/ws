import {TermsConditionsComponent} from './terms-conditions/terms-conditions.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {IndexComponent} from './index/index.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {RegisterComponent} from './register/register.component';
import {EmailVerificationComponent} from './email-verification/email-verification.component';
import {LoginComponent} from './login/login.component';
import {ReferralComponent} from './referral/referral.component';
import {ArticleComponent} from './index/article/article.component';
import {RemoteEntryComponent} from "./entry.component";

const routes: Routes = [
  {
    path: '',
    component: RemoteEntryComponent,
    children: [
      { path: '', component: IndexComponent },
      { path: 'forget', component: ForgetPasswordComponent },
      { path: 'email-verification', component: EmailVerificationComponent },
      { path: 'reset-password', component: ResetPasswordComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'login', component: LoginComponent },
      { path: 'article/:id', component: ArticleComponent },
       { path: 'referral', component: ReferralComponent },
      { path: 'terms-conditions/:lang', component: TermsConditionsComponent },
      { path: 'privacy-policy/:lang', component: PrivacyPolicyComponent },
      { path: 'contact-us/:lang', component: ContactUsComponent },
    ],
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntryRoutingModule { }
