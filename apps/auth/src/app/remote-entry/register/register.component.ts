import {Component, OnInit, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {ReCaptchaV3Service} from 'ngx-captcha';
import {MatStepper} from '@angular/material/stepper';
import {AuthService, Constant, ItemAttach, SnackService, UploadFilesComponent} from '@khdma-frontend-nx/shared';

declare var google;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild(UploadFilesComponent, {static: false})
  private uploadFilesComponent: UploadFilesComponent;

  basicInformation: UntypedFormGroup;
  thirdInformation: UntypedFormGroup;
  phoneInformation: UntypedFormGroup;
  subscriptions = new Subscription();
  bankList: any[] = [];
  location: any = {};
  // map: any;
  markers: any;
  address: any = {};
  countries: any[] = [];
  governorates: any[] = [];
  cities: any[] = [];
  isUploadedBranchImg: boolean;
  brachImageUrl: string;
  itemAttachments: ItemAttach[] = [];
  uploadInfo: any;
  processLogin: boolean = true;
  toggleRegister: boolean = true;
  siteKey: string = '6LcX_ccaAAAAAGyjqSKX60yM-iJoZEs4E-PYFgTJ';
  user: any = {};
  msg = '';
  passwordPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$";
  showPasswordText: boolean = true;
  showConfirmPasswordText: boolean = true;
  constructor(fb: UntypedFormBuilder,
              private snack: SnackService,
              public translate: TranslateService, public constant: Constant,
              private authService: AuthService,
              private router: Router,
              private reCaptchaV3Service: ReCaptchaV3Service
  ) {

    this.basicInformation = fb.group({
      fullName: [null, Validators.required],
      email: [null, [Validators.email]],
      nationalId: [null],
      customerLegalEntityName: [null, Validators.required],
      customerLegalEntityNumber: [null],
      recaptchaToken: ['', Validators.required]
    });
   // const phoneInformationOptions: AbstractControlOptions = { validators: this.checkPasswords  };

    this.phoneInformation = fb.group({
      phoneNumber: [null, [ Validators.required,
        Validators.pattern("^[0-9]*$")]],
      password: [null, [Validators.required, Validators.pattern(this.passwordPattern)]],
      confirmPassword: [null,[Validators.required, Validators.pattern(this.passwordPattern)]]
    });


    this.thirdInformation = fb.group({
      country: [null, Validators.required],
      governorate: [null, Validators.required],
      city: [null, Validators.required],
    });

  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  checkPhoneNumber(stepper: MatStepper) {
    this.authService
      .getUserByPhoneNumber(this.phoneInformation.controls.phoneNumber.value, this.constant.userTypes.CUST)
      .subscribe(res => {
        if (res.success) {
          let user = res.data;
          console.log(user);
          if (user) {
            this.basicInformation.patchValue({
              fullName: user.fullName,
              email: user.email
            });
            this.basicInformation.controls['fullName'].disable();
            this.basicInformation.controls['email'].disable();
            stepper.next();
          } else {
            this.basicInformation.controls['fullName'].enable();
            this.basicInformation.controls['fullName'].reset();
            this.basicInformation.controls['email'].enable();
            this.basicInformation.controls['email'].reset();
            stepper.next();
          }
        }
      }, err => {
        this.msg = err.message;
      });
  }

  ngOnInit(): void {
    // this.geolocation();
    this.getCountry();
    this.selectCountry();
    this.selectGovernorate();
    this.generateReCaptcha();

  }

  get phoneNumberValidation() {
    return this.phoneInformation.controls;
  }

  get Validation() {
    return this.basicInformation.controls;
  }

  get validationThirdInformation() {
    return this.thirdInformation.controls;
  }

  public hasPhoneNumberError = (controlName, errorName) => {
    return this.phoneNumberValidation[controlName].hasError(errorName);
  };

  public hasError = (controlName, errorName) => {
    return this.Validation[controlName].hasError(errorName);
  };


  public hasInformationError = (controlName, errorName) => {
    return this.validationThirdInformation[controlName].hasError(errorName);
  };

  getCountry() {
    this.authService.getCountries().subscribe((res: any) => {
      this.countries = res.data;
    });
  }

  // get loction
  // geolocation() {
  //   navigator.geolocation.getCurrentPosition((position) => {
  //     this.location = {
  //       lat: position.coords.latitude,
  //       lng: position.coords.longitude,
  //     };
  //     this.loadMap();
  //   });
  //
  // }


  // Choose country
  selectCountry() {

    this.subscriptions.add(
      this.thirdInformation.get('country').valueChanges.subscribe(() => {
        const selectCountryId = this.validationThirdInformation.country.value;
        if (selectCountryId > 0) {
          this.thirdInformation.controls['governorate'].reset();
          this.thirdInformation.controls['city'].reset();
          const country = this.countries.find(country => country.id == selectCountryId);
          if (country.governorates.length > 0) {
            this.governorates = country.governorates;
            this.cities = [];
          } else {
            this.governorates = [];
            this.cities = [];
          }
        }
      })
    );
  }

  // Choose governorate
  selectGovernorate() {
    this.subscriptions.add(
      this.thirdInformation.get('governorate').valueChanges.subscribe(() => {
        const governorateId = this.thirdInformation.controls.governorate.value;
        if (governorateId > 0) {
          this.thirdInformation.controls['city'].reset();
          const governorate = this.governorates.find(country => country.id == governorateId);
          if (governorate.cities.length > 0) {
            this.cities = governorate.cities;
          } else {
            this.cities = [];
          }
        }
      }));
  }

  //load map
  // loadMap() {
  //   this.map = new google.maps.Map(document.getElementById('map')!, {
  //     zoom: 16,
  //     center: this.location,
  //   });
  //   this.addMarker();
  //   let that = this;
  //   this.map.addListener('click', (mapsMouseEvent) => {
  //     that.location = mapsMouseEvent.latLng;
  //     that.location = {lat: Number(that.location.toJSON().lat), lng: Number(that.location.toJSON().lng)};
  //     that.removeMarkers();
  //     that.addMarker();
  //   });
  // }

  // Add marker on map
  // addMarker() {
  //   this.markers = [];
  //   const marker = new google.maps.Marker({
  //     position: this.location,
  //     animation: google.maps.Animation.DROP,
  //     zoom: 16,
  //     map: this.map,
  //   });
  //   this.markers.push(marker);
  //   return this.markers;
  // }

  //remove markers
  // removeMarkers() {
  //   this.markers[0].setMap(null);
  // }

  //create customer account
  createCustomerAccount() {
    const query = {
      ...this.basicInformation.getRawValue(),
      ...this.phoneInformation.value,
      address: {
        country: {id: this.thirdInformation.controls.country.value},
        governorate: {id: this.thirdInformation.controls.governorate.value},
        city: {id: this.thirdInformation.controls.city.value},
        lat: this?.location?.lat,
        lon: this?.location?.lng,

      }
    };
    console.log(query);
    this.authService.registerCustomer(query).subscribe(res => {
      if (res.success){
        sessionStorage.setItem('id', res.data.id);
        console.log(res);
        this.snack.showSnack('COMMON.ADD_SUCCESS', 'success');
        this.router.navigate(['/index/referral'])
      }
    });
    //
    // this.router.navigate(['./index'])
  }

  generateReCaptcha() {
    this.processLogin = false;
    this.reCaptchaV3Service.execute(this.siteKey, 'login', (token) => {
      this.basicInformation.controls.recaptchaToken.setValue(token);
      this.processLogin = true;
    }, {
      useGlobalDomain: false
    });
  }

  checkSpace(event:any) {
    if(event.target.selectionStart === 0 && event .code === 'Space') {
      event.preventDefault()
    }
  }

}
