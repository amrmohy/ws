import {UntypedFormControl, UntypedFormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
})
export class ContactUsComponent implements OnInit {
  dir: string = 'rtl';
  constructor(
    private route: ActivatedRoute,
    private _translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.dir = params['lang'] && params['lang'] == 'en' ? 'ltr' : 'rtl';
      this._translate.use(params['lang'] ? params['lang'] : 'ar');
    });
  }

  contactUsForm = new UntypedFormGroup({
    name: new UntypedFormControl(null),
    email: new UntypedFormControl(null, Validators.required),
    subject: new UntypedFormControl(null, Validators.required),
    message: new UntypedFormControl(null, Validators.required),
  });

  contanctUsAction() {
    if (this.contactUsForm.valid) {
      this.contactUsForm.reset();
      this.contactUsForm.controls['name'].setErrors(null);
      this.contactUsForm.controls['email'].setErrors(null);
      this.contactUsForm.controls['subject'].setErrors(null);
      this.contactUsForm.controls['message'].setErrors(null);
      document.getElementById('successMessageId').classList.add('showSuccessMessage');
      setTimeout(() => {
        document.getElementById('successMessageId').classList.remove('showSuccessMessage');
      }, 2000);
    }
  }

}
