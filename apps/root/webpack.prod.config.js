const { withModuleFederation } = require('@nrwl/angular/module-federation');
const config = require('./module-federation.config');
module.exports = withModuleFederation({
  ...config,
  /*
   * Remote overrides for production.
   * Each entry is a pair of an unique name and the URL where it is deployed.
   *
   * e.g.
   * remotes: [
   *   ['app1', 'https://app1.example.com'],
   *   ['app2', 'https://app2.example.com'],
   * ]
   */

   remotes: [
      ['auth', 'http://localhost:4200/auth'],
      ['dashboards', 'http://localhost:4200/dashboards'],
      ['finance', 'http://localhost:4200/finance'],
   ]

});
