importScripts('https://www.gstatic.com/firebasejs/9.6.7/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.6.7/firebase-messaging-compat.js');
firebase.initializeApp({
    apiKey: "AIzaSyD8SgJXnz7LzQAhk02uyQbV-BtnE2ahhc8",
    authDomain: "dmaapp-56741.firebaseapp.com",
    databaseURL: "https://dmaapp-56741.firebaseio.com",
    projectId: "dmaapp-56741",
    storageBucket: "dmaapp-56741.appspot.com",
    messagingSenderId: "360289453312",
    appId: "1:360289453312:web:196f1c27e252ff4f9f1fe5",
    measurementId: "G-4TVDLLP6G5"
});
var messaging = firebase.messaging();

messaging.onBackgroundMessage( function (payload)  {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
    const notificationTitle = payload.data.title;
    const notificationOptions = {
      body: payload.data.body,
      icon: payload.data.icon
    };
    return  self.registration.showNotification(notificationTitle,
      notificationOptions);
  });
