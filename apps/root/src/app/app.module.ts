import { NxWelcomeComponent } from './nx-welcome.component';
import { RouterModule } from '@angular/router';
import {
  appInitializer,
  AuthService,
  CanDeactivateGuard,
  httpInterceptorProviders,
  LoaderService,
  MatPaginatorIntlCro,
  MessagingService,
  SharedModule,
} from '@khdma-frontend-nx/shared';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { AsyncPipe, DatePipe } from '@angular/common';
import { ConnectionServiceModule } from 'ng-connection-service';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { environment } from '@env/environment';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, NxWelcomeComponent],
  imports: [
    BrowserModule,
    SharedModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      defaultLanguage: 'ar',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    RouterModule.forRoot(
      [
        {
          path: '',
          redirectTo: 'dashboards',
          pathMatch: 'full',
        },
        {
          path: 'index',
          loadChildren: () =>
            import('auth/Module').then((m) => m.RemoteEntryModule),
        },
        {
          path: 'dashboards',
          loadChildren: () =>
            import('dashboards/Module').then((m) => m.RemoteEntryModule),
        },
        {
          path: 'finance',
          loadChildren: () =>
            import('finance/Module').then((m) => m.RemoteEntryModule),
        },
      ],
      { initialNavigation: 'enabledBlocking' }
    ),
    ReactiveFormsModule,
    MatNativeDateModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    ConnectionServiceModule,
  ],
  providers: [
    httpInterceptorProviders,
    MessagingService,
    AsyncPipe,
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro },
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [AuthService],
    },
    LoaderService,
    CanDeactivateGuard,
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
