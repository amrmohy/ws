// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  headerStyle: {
    'background-color': '#2f4550',
    color: '#dfe3e4'
  },
  prodHeaderStyle: {
    'background-color': '#FFFFFF',
    color: '#e44891'
  },

  billIconColor: '#ffffff',
   // apiUrl: 'http://35.246.72.224:1111/service-5dma/',
  apiUrl: 'https://5dma.website/service-5dma/',
  // apiUrl: 'http://localhost:8081/',
  appToken: 'PORTAL_OF5a6P1iKbVDdrIe5OOvkGxYZo6XT3g0wEKC9PlsoWfNUBf8sZgWt31Jz4L8e7hRMLNxZ8vOYdTJIotb1uwDRX6LbNWRALXzV5iUNjKBomxHCsUkgKKTdVToxIDf8fE3',
  // baseImageUrl:
  //   'https://khdma-bucket.s3.eu-west-2.amazonaws.com/',
  baseImageUrl: 'https://khdma-bucket.s3.eu-west-2.amazonaws.com/',
  // appToken: 'PORTAL_OF5a6P1iKbVDdrIe5OOvkGxYZo6XT',
  // baseImageUrl:
  //   'https://khdma-bucket.s3.eu-west-2.amazonaws.com/',

  googleTranslateApiKey: 'https://translation.googleapis.com/language/translate/v2?key=AIzaSyD8SgJXnz7LzQAhk02uyQbV-BtnE2ahhc8',

  firebase: {
    apiKey: 'AIzaSyD8SgJXnz7LzQAhk02uyQbV-BtnE2ahhc8',
    authDomain: 'dmaapp-56741.firebaseapp.com',
    databaseURL: 'https://dmaapp-56741.firebaseio.com',
    projectId: 'dmaapp-56741',
    storageBucket: 'dmaapp-56741.appspot.com',
    messagingSenderId: '360289453312',
    appId: '1:360289453312:web:fff38dcc00791f409f1fe5',
    measurementId: 'G-Y4QEJ0ZVXV'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
