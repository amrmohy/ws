import {MatDialog} from '@angular/material/dialog';
import {BaseService} from './../../service/base.service';
import {TranslateService} from '@ngx-translate/core';
import {filter} from 'rxjs/operators';
import {NavigationEnd, Router} from '@angular/router';
import {Component, Input, OnChanges, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {MediaMatcher} from '@angular/cdk/layout';
import {Constant} from '../../constant/constant';
import {UpdateBranchService} from '../../service/update-branch.service';
import {ReleaseNotesComponent} from '../release-notes/release-notes.component';
import { AuthService } from '../../service/auth.service';


export interface SideMenuItem {
  nameAr: string;
  nameEn: string;
  nameFr: string;
  id: number;
  fontIcon: string;
  active?: boolean;
  routingPath?: string;
  sortRanking?: number;
  screens?: SideMenuItem[];
}

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
  providers: [Constant]
})
export class SideNavComponent implements OnChanges, OnDestroy {
  private subscriptions = new Subscription();
  mobileQuery: MediaQueryList;
  @Input() folded: boolean;
  sideMenu: SideMenuItem[];
  currentRoute: string;
  currentUser: any;
  userImagePath: String;
  activeScreenId: number;
  branches: any[] = [];
  branchConfig: any;
  selectedItem:any;
  constructor(
    private _router: Router,
    public _translate: TranslateService,
    private _base: BaseService,
    private _media: MediaMatcher,
    private dialog: MatDialog,
    public constant: Constant,
    private updateBranchService : UpdateBranchService,
    private _auth : AuthService
  ) {
    if (sessionStorage.getItem('activeScreenId')){
      this.activeScreenId = Number(sessionStorage.getItem('activeScreenId'));
    }
    this.mobileQuery = this._media.matchMedia('(max-width: 639px)');
    _base.updateUserImage();
    this.userImagePath = this._base.userImagePath;
    this.currentUser = JSON.parse(window.localStorage.getItem('CURRENT_USER'));
    if (
      !this.constant.userIsCustomer() ||
      !this.constant.userIsEmployee()
    ) {
      this.sideMenu = this.currentUser?.userPrivilage?.menu;
      this.subscriptions.add(
        _router.events
          .pipe(filter((event) => event instanceof NavigationEnd))
          .subscribe((event: any) => {
            this.currentRoute = event.url;
          })
      );
      this.subscriptions.add(
        this.updateBranchService.getConfigBranches().subscribe(res => {
          if (res.success) {
            let branchConfigLocal = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
            if (constant.userIsAdmin()) {
             this.branchConfig = [];
              localStorage.setItem('BRANCH_CONFIG', JSON.stringify(this.currentUser.branchConfig));
              if (branchConfigLocal){
                this.branchConfig = res.data.find(branch => branch.id === branchConfigLocal.id);
              }
            } else {
              if (this.currentUser.branchId){
                if (branchConfigLocal){
                  this.branchConfig = res.data.find(branch => branch.id === branchConfigLocal.id);
                }else{
                  this.branchConfig = res.data.find(branch => branch.id === this.currentUser.branchId);
                }
                localStorage.setItem('BRANCH_CONFIG', JSON.stringify(this.branchConfig));
              }
            }
            this.branches = res.data;
          }
        })
      );
    }
  }


  ngOnChanges() {
    if (this.folded === true) {
      this.sideMenu.forEach((item) => {
        item.active = false;
      });
    }
  }

  toggleChildren(id: number) {
    this.sideMenu.forEach((item) => {
      if (item.id === id) {
        item.active = item.active === true ? false : true;
      } else {
        item.active = false;
      }
    });
  }

  close() {
    this._base.sideNave.next('close');
  }

  changeRoute(route, screenId: number) {
    if (this.mobileQuery.matches) {
      this._base.sideNave.next('close');
    }
    this.activeScreenId = screenId;
    sessionStorage.setItem('activeScreenId',JSON.stringify(this.activeScreenId));
    this._router.navigate([route]);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }



  openReleaseNotesDialog() {
    this.dialog.open(ReleaseNotesComponent)
  }

  branchChanged(branch) {
    localStorage.setItem('BRANCH_CONFIG', JSON.stringify(branch));
    this.updateBranchService.setBranch(branch)
  }

  navTo() {
    this.sideMenu.forEach((item) => {item.active = false;});
    this.activeScreenId = -1;
    sessionStorage.setItem('activeScreenId',JSON.stringify(this.activeScreenId));
    if (this.mobileQuery.matches) {
      this._base.sideNave.next('close');
    }
    if (this.constant.userIsEmployee() || this.constant.userIsAccManager()) {
      this._router.navigateByUrl('/user-management/user-profile');
    }
    if (this.constant.userIsCustomer() || this.constant.userIsAdmin()) {
      this._router.navigateByUrl('/shopping-customer/manage-account');
    }
  }

  logout() {
    this._auth.logout();
  }
}
