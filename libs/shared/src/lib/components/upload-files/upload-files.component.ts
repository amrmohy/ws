import {TranslateService} from '@ngx-translate/core';
import {BaseService} from './../../service/base.service';
import {Component, EventEmitter, Input, OnDestroy, Output,} from '@angular/core';

@Component({
  selector: 'upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss'],
})
export class UploadFilesComponent implements OnDestroy {
  constructor(
    private _base: BaseService,
    public _translate: TranslateService
  ) { }

  uploadComponentId = 'UC' + Math.floor(Math.random() * 100);
  static uploadedFiles: Object[] = [];
  //--------------------------------

  @Input()
  componentId: string;
  @Input()
  multiple_upload: boolean = false;
  @Output()
  uploadInfo = new EventEmitter<any>();
  @Output()
  imagesUrlsEvent = new EventEmitter<string>();

  onSelectFileChange(event: any) {
    const files: FileList = event.target.files;
    event.target.nextElementSibling.innerHTML = `${files.length} ${this._translate.instant('COMMON.SELECTED_FILES_NUMBER_IS')}`;
    if (files.length > 0) {
      if (UploadFilesComponent.uploadedFiles?.length > 0) {
        const foundedItem = UploadFilesComponent.uploadedFiles.find(
          (item) => item['pathId'] == this.componentId
        );
        console.log(foundedItem);
        if (foundedItem) {
          foundedItem['files'] = files;
        } else {
          UploadFilesComponent.uploadedFiles.push({
            pathId: this.componentId,
            files: files,
          });
        }
      } else {
        UploadFilesComponent.uploadedFiles.push({
          pathId: this.componentId,
          files: files,
        });
      }
      //-------------------------------------------------
      if (UploadFilesComponent.uploadedFiles?.length > 0) {
        this.uploadInfo.emit(UploadFilesComponent.uploadedFiles);
      }
      console.log(UploadFilesComponent.uploadedFiles);
    }
  }

  uploadData(uploadedObject: any) {
    console.log(uploadedObject);
    let uploadedDataa = [];
    const pathId = uploadedObject.pathId;
    if (UploadFilesComponent.uploadedFiles.length > 0) {
      const returnedItem = UploadFilesComponent.uploadedFiles.find(
        (item) => item['pathId'] == pathId
      );
      if (returnedItem) {
        const returnedFileList = returnedItem['files'];
        if (returnedFileList.length > 0) {
          console.log('here is returnedFileList : ', returnedFileList);
          let files = Array.from(returnedFileList);
          for (let index = 0; index < files.length; index++) {
            console.log(
              'here my upload item :',
              uploadedObject.uploadData[index]
            );
            uploadedDataa.push({
              id: uploadedObject.uploadData[index].id,
              pathId: pathId,
              file: files[index],
            });
          }
        }
        console.log(uploadedDataa);
        for (const uploadItem of uploadedDataa) {
          this._base
            .uploadToGoogle(
              uploadItem['id'],
              uploadItem['pathId'],
              uploadItem['file']
            )
            .subscribe(
              (res) => {
                console.log('File Upload Res', res, res.data.fileDownloadUri);
                this.imagesUrlsEvent.emit(res.data.fileDownloadUri);
              },
              (err) => console.log(err),
              () => {
                console.log('file Upload Complete');
              }
            );
        }
        //remove uploaded items
        UploadFilesComponent.uploadedFiles = UploadFilesComponent.uploadedFiles.filter(
          (item) => item['pathId'] != pathId
        );
        console.log(UploadFilesComponent.uploadedFiles);
      } else {
        console.log('Files Already Uploaded ------- ');
      }
    }
  }

  openFileInput(event: any) {
    event.target.nextElementSibling.click();
  }

  ngOnDestroy(): void {
    console.log('Start Destroied Upload Files Component ... ');
    UploadFilesComponent.uploadedFiles.length = 0;
  }
}
