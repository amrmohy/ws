import {Component, OnInit} from '@angular/core';
import {CustomerProgressService} from '../../../service/customer-progress.service';
import {TranslateService} from '@ngx-translate/core';
import {MatDialogRef} from '@angular/material/dialog';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-customer-progress-dialog',
  templateUrl: './customer-progress-dialog.component.html',
  styleUrls: ['./customer-progress-dialog.component.scss']
})
export class CustomerProgressDialogComponent implements OnInit {
  panelOpenState = false;
  fullPercentage: number;
  BRANCHCONFIG: any = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG'));
  branchId: any = JSON.parse(window.localStorage.getItem('BRANCH_CONFIG')).id;
  modulesOfProgress = [
    {
      name: 'BRANCHES',
      progressStages: [],
      progressPercentage: null,
      stagesLinks: [
        `/branch/basic`,
        `/branch/details`,
        `/branch/general-setup`,
        `/branch/business`,
        `/branch/address`,
        `/branch/delivery-method`,
        `/branch/working-days`,
      ]
    },
    {
      name: 'INVENTORY',
      progressStages: [],
      progressPercentage: null,
      stagesLinks: [
        '/shopping-customer/suppliers',
        '/shopping-customer/customer-categories',
        '/shopping-customer/customer-categories',
        '/shopping-customer/customer-items',
        '/shopping-customer/customer-brands',
      ]
    },
    {
      name: 'HR',
      progressStages: [],
      progressPercentage: null,
      stagesLinks: [
        '/hr/customer-employees',
      ]
    },
    {
      name: 'ACCOUNTING',
      progressStages: [],
      progressPercentage: null,
      stagesLinks: [
        '/finance/finance-setting'
      ]
    },
  ];

  constructor(
    private _customerProgressService: CustomerProgressService,
    public _translate: TranslateService,
    private ItemDialogRef: MatDialogRef<CustomerProgressDialogComponent>,
    private router: Router
  ) {
    this._customerProgressService.getCustomerProgress(this.BRANCHCONFIG.id).subscribe((res) => {
      let branchProgressStages = res.data.filter((stage, index) => index <= 6);
      let inventoryProgressStages = res.data.filter((stage, index) => (index > 6 && index <= 11));
      let hrProgressStages = res.data.filter((stage, index) => index === 12);
      let accountingProgressStages = res.data.filter((stage, index) => index === 13);
      this.modulesOfProgress[0].progressStages = branchProgressStages;
      this.getPercentage(0);
      this.modulesOfProgress[1].progressStages = inventoryProgressStages;
      this.getPercentage(1);
      this.modulesOfProgress[2].progressStages = hrProgressStages;
      this.getPercentage(2);
      this.modulesOfProgress[3].progressStages = accountingProgressStages;
      this.getPercentage(3);
      this.fullPercentage = Math.round((this.modulesOfProgress.map((module) => {
        return module.progressPercentage;
      }).reduce((current, accumulate) => current + accumulate, 0) / 400) * 100);
    });
  }

  ngOnInit(): void {
  }

  getPercentage(index) {
    this.modulesOfProgress[index].progressPercentage = Math.round((this.modulesOfProgress[index].progressStages.map((stage) => {
      return stage.flag;
    }).filter((flag) => flag === true).length / (this.modulesOfProgress[index].progressStages.map((stage) => {
      return stage.flag;
    }).length)) * 100);
  }

  closeDialog(route, name) {

    if (name === 'BRANCHES') {
      const navigationExtras: NavigationExtras = {
        queryParams: {
          id: this.branchId
        },
        skipLocationChange: true,
      };
      this.router.navigate([route], navigationExtras);
    }
    this.ItemDialogRef.close();
  }
}
