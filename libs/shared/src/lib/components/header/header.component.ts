import {TranslateService} from '@ngx-translate/core';
import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {BaseService} from '../../service/base.service';
import {CustomerProgressDialogComponent} from './customer-progress-dialog/customer-progress-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Output() toggle: EventEmitter<any> = new EventEmitter();
  public width: number;
  public screenWidth: any;
  headerStyle = this._base.headerStyle;
  prodHeaderStyle = this._base.prodHeaderStyle;
  production = this._base.production;
  sidenav: boolean = true;
  calcHeader(){
    const _sideNav = document.getElementById('pagesSideNavigation');
    const sideNav: HTMLElement = _sideNav as HTMLElement;
    this.width = screen.width - sideNav.clientWidth ;
  }
  constructor(public _translate: TranslateService, private _base: BaseService,private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.screenWidth = window.innerWidth;
  }

  @ViewChild('fixedHeader') fixedHeader: ElementRef;

  ngAfterViewInit() {
    // this.width = this.fixedHeader.nativeElement.offsetWidth - 217;
   this.calcHeader();
  }



  toggleSideNav() {
    this.toggle.emit();
    this.calcHeader();
    this.sidenav = !this.sidenav;
  }

  openCustomerProgressDialog() {
    const dialogRef = this.dialog.open(CustomerProgressDialogComponent,{
    }).addPanelClass('progress_dialog');
  }

}
