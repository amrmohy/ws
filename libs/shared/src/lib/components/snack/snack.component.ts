import {TranslateService} from '@ngx-translate/core';
import {Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef,} from '@angular/material/snack-bar';

@Component({
  selector: 'app-snack',
  templateUrl: './snack.component.html',
  styleUrls: ['./snack.component.scss'],
})
export class SnackComponent {
  constructor(
    public _snackBarRef: MatSnackBarRef<SnackComponent>,
    public _translate: TranslateService,
    @Inject(MAT_SNACK_BAR_DATA) public _data: any
  ) {}
}
