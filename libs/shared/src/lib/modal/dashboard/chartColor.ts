export class chartColor {
    borderColor: string = "#42a5f5";
    backgroundColor: string = "#42a5f5";
    pointBackgroundColor: string = "#42a5f5";
    pointHoverBackgroundColor: string = "#42a5f5";
    pointBorderColor: string = "#42a5f5";
    pointHoverBorderColor: string = "#42a5f5";

    constructor(
        borderColor?: string,
        backgroundColor?: string,
        pointBackgroundColor?: string,
        pointHoverBackgroundColor?: string,
        pointBorderColor?: string,
        pointHoverBorderColor?: string) {
        if (borderColor) this.borderColor = borderColor;
        if (backgroundColor) this.backgroundColor = backgroundColor;
        if (pointBackgroundColor) this.pointBackgroundColor = pointBackgroundColor;
        if (pointHoverBackgroundColor) this.pointHoverBackgroundColor = pointHoverBackgroundColor;
        if (pointBorderColor) this.pointBorderColor = pointBorderColor;
        if (pointHoverBorderColor) this.pointHoverBorderColor = pointHoverBorderColor;
    }
}