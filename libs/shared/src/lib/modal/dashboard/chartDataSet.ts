import {chartDataSetVale} from './datasetValue';

export class chartDataSet {
    datasets: chartDataSetVale[];
    labels: string[] = ['2'];
    constructor(datasets: chartDataSetVale[], labels: string[]) {
        if (datasets) this.datasets = datasets;
        if (labels) this.labels = labels;
    }
}
