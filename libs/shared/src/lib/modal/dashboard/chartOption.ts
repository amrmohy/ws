export class chartOption {
    spanGaps: boolean = false;
    maintainAspectRatio: boolean = false;
    legend: any = { display: false };
    tooltips: any = {
        position: "nearest",
        mode: "index",
        intersect: false
    };
    scales: any = {
        xAxes: [{
            gridLines: {
                display: false
            }
        }],
        yAxes: [{
            gridLines: {
                display: false
            }, ticks: {
                beginAtZero: true,
            }
        }]
    };

    layout = {
        padding: { bottom: 16, top: 16, left: 16, right: 16 }
    }

    constructor(
        spanGaps?: boolean,
        maintainAspectRatio?: boolean,
        legend?: any,
        tooltips?: any,
        scales?: any) {
        if (spanGaps) this.spanGaps = spanGaps;
        if (maintainAspectRatio) this.maintainAspectRatio = maintainAspectRatio;
        if (legend) this.legend = legend;
        if (tooltips) this.tooltips = tooltips;
        if (scales) this.scales = scales;
    }
}