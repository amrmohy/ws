import {chartDataSetVale} from './datasetValue';
import {chartOption} from './chartOption';
import {chartDataSet} from "./chartDataSet";

export class chart {
  type: string = 'bar';
  data: chartDataSet = new chartDataSet([new chartDataSetVale([])], []);
  options: any = new chartOption();

  constructor(
    data: chartDataSet,
    options?: chartOption,
    type?: string) {
    if (data) this.data = data;
    if (type) this.type = type;
    if (options) this.options = options;
  }
}
