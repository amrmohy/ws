import {Governorate} from './Governorate';

export class City {
    id:          number;
    name:        string;
    cityCode:    string;
    governorate: Governorate;
}
