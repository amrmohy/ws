import {City} from './City';

export class Governorate {
    id:              number;
    name:            string;
    governorateCode: string;
    country:         null;
    cities:          City[];
}
