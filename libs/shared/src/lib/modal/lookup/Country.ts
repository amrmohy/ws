import {Governorate} from './Governorate';

export class Country {
    id:                 number;
    name:               string;
    countryCode:        string;
    countryCallingCode: string;
    vat:                number;
    governorates:       Governorate[];
}
