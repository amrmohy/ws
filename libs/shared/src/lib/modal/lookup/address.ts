import {Country} from './Country';
import {Governorate} from './Governorate';
import {City} from './City';

export interface Address {
  addressType: number;
  street: null;
  street2: null;
  postalCode: null;
  building: null;
  floor: null;
  flatNumber: null;
  landMark: null;
  lat: string;
  lon: string;
  country: Country;
  governorate: Governorate;
  city: City;
}
