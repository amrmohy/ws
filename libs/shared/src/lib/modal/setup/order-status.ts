export class OrderStatus {
    id: number;
    code: string;
    name: string;
}
