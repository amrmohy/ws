export class DeliveryMethod {
    id: number;
    code: string;
    name: string;
}
