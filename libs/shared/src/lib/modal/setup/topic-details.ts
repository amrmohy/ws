export interface Topicdetails {
  id: number;
  title: string;
  topic: Object;
  imagePath: string;
  message: string;
  priority: string;
  statusCode:Number;
}
