import {Country} from '../lookup/Country';
import {Service} from '../user-management/service';
import {Governorate} from '../lookup/Governorate';
import {City} from '../lookup/City';

export class ServiceCategory {
  id: number;
  name: string;
  nameAr: string;
  nameEn: string;
  nameFr: string;
  imagePath: string;
  code: string;
  description: string;
  country: Country = new Country;
  governorate: Governorate = new Governorate;
  city: City = new City;
  service: Service = new Service;
}
