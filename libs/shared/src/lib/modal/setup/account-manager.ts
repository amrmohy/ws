import {User} from '../user-management/user';

export class AccountManager {
    id: number;
    user: User = new User;
    percentage: number;
    countryId : number ;
    governorateId: number;
    cityId: number ;
    agentId : number ;
    isAgent : boolean ;
    statusCode: number;

}
