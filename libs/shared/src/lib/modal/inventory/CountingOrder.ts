import {CountingOrderDetails} from './CountingOrderDetails';

export class CountingOrder {
	id: number;
	branchId : number = 0
	countingDate : string = '';
	countingNumber : string  = '';
	description : string = '' ;
	countingOrderStatus : any  ;
	countingOrderDetails: CountingOrderDetails[] = [];
}
