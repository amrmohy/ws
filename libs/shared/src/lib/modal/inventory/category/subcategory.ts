export interface Subcategory {
  id: number;
  name: string;
  category: Object;
  imagePath: string;
  description: string;
  country: Object;
  serviceCategory: Object;
  customerCount:number;
  statusCode:Number;
}
