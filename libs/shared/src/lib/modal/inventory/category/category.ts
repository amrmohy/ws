import {Subcategory} from './subcategory';

export interface Category {
  id: number;
  name: string;
  nameAr: string;
  nameEn: string;
  nameFr: string;
  imagePath: string;
  description: string;
  country: Object;
  serviceCategory: Object;
  customerCount:number;
  subCategories:Subcategory[];
  statusCode:Number
}
