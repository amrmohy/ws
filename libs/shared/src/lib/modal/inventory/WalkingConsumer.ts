export interface WalkingConsumer {
    id: number;
    fullName:string;
    email:string;
    addressType?: any;
    defaultAddress: boolean;
    phoneNumber?: string;
    street?: string;
    street2?: string;
    postalCode?: string;
    building?: string;
    floor?: string;
    flatNumber?: string;
    landMark?: string;
    lat?: string;
    lon?: string;
    country?: any;
    governorate?: any;
    city?: any;
}
