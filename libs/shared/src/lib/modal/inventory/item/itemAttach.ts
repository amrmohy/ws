export interface ItemAttach {
  id: number;
  imagePath: string;
  item: Object;
}
