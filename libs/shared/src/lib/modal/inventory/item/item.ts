export interface Item {
  [x: string]: any;
  id: number;
  description: string;
  nameAr: string;
  nameEn: string;
  name: string;
  discount: number;
  expiryDays: Date;
  imagePath: string;
  itemNumber: string;
  salesPrice: number;
  salesTaxValue: number;
  secondaryDescription: string;
  statusCode: number;
  category: Object,
  subCategory: Object,
  country: Object,
  serviceCategory: Object,
  baseUnit: Object,
  customerCount: number;
  orderedCount: number;
  revisionGroups: any[];
  itemRevisions: any[];
  availableQty:number;
}
