export interface ItemRevisionValue {
  id: number;
  name: string;
  nameAr: string;
  nameEn: string;
  nameFr: string;

}
