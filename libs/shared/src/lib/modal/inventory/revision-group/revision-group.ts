import {ItemRevisionValue} from "./revision-value";


export class ItemRevisionGroup {
  id: number;
  name: string;
  nameAr: string;
  nameEn: string;
  nameFr: string;
  itemRevisionValue:ItemRevisionValue[];

}
