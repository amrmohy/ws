export interface ScheduleDaySlots {
    id: number;
    dayId: number;
    fromHour: string;
    toHour: string;
    preparedOrders: number;
    finishedOrders: number;
    isActive: boolean;
}