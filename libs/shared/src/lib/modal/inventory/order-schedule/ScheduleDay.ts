export interface ScheduleDay {
    dayId: number,
    dayDate: string,
    dayName: string
}
