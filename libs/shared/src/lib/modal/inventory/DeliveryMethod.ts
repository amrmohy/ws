export interface DeliveryMethod {
  id:number;
  code:string;
  name:string;
}
