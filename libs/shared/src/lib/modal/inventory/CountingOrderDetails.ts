export class CountingOrderDetails {
	id: number;
	branchItem : any;
	countingReasonId : number;
	currentQty: number;
	newQty: number;
	color:any;
	size:any;
}
