export interface subclass {
  id: number;
  name: string;
  nameAr: string;
  nameEn: string;
  nameFr: string;
  description: string;
  unitClass: Object;
}
