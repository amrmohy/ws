export interface unitclass {
  id: number;
  name: string;
  nameAr: string;
  nameEn: string;
  nameFr: string;
  description: string;
}
