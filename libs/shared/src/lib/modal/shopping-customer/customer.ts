import {User} from 'src/app/shared/modal/user-management/user';

export interface Customer {
  id: number;
  customerCode: string;
  legalEntityName: string;
  legalEntityNumber: string;
  user: User;
}
