import {Address} from './../lookup/address';
import {Customer} from './customer';

export interface CustomerBranches{
    id:                number;
    customer:          Customer;
    branchName:        string;
    bankId:          number;
    accountNumber:     string;
    rateValueTypeId:   number;
    fixedRate:         any;
    percentageRate:    any;
    serviceCategories: any[];
    address:           Address;
    accountManager:    Object;
}
