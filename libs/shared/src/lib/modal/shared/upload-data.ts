export interface UploadData {
  id: number;
  pathId: number;
  file:File;
}
