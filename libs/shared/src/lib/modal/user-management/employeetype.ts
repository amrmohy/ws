export class EmployeeType {
    id: number;
    salary: number;
    discountPercentage:number;
    maxDiscountAmount:number;
    nameAr: string;
    nameEn: string;
    nameFr: string;
    description: string;


}
