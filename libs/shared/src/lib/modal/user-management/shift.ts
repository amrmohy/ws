export class Shift {
    id: number;
    shiftName: string;
    startTime: string;
    endTime: string;	
    description: string;
}
