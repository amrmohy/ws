export class BaseUserProfile {
  customer: {
    createdFromChannelId: string;
    customerCode: string;
    id: number;
    legalEntityName: string;
    legalEntityNumber: string;
    statusCode: number;
  };
  user: {
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    createdFromChannelId: string;
    email: string;
    enabled: boolean;
    expireDate: string;
    fullName: string;
    id: number;
    imagePath: string;
    phoneNumber: string;
    preferLanguage: string;
    statusCode: number;
    userType: {
      code: string;
      id: number;
      nameAr: string;
      nameEn: string;
      nameFr: string;
      statusCode: number;
    };
  };
}
