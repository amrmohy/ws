export class Service {
    id: number;
    nameAr: string;
    nameEn: string;
    nameFr: string;
    imagePath: string;
    descriptionAr: string;
    descriptionEn: string;
    descriptionFr: string;
    countries : any ;
 
}
