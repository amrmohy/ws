export class User {
  id: number;
  fullName: string;
  email: string;
  expireDate: string;
  phoneNumber: string;
  password: string;
  isAccountNonExpired: boolean;
  isAccountNonLocked: boolean;
  isEnabled: boolean;
  token: string;
  refreshToken: string;
  preferLanguage: string;
  mobileRole: any;
  mobileRoleId : Number ;
  userRoles: number[];
}
