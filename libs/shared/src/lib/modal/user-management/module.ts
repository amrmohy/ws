export class Module {
  id: number;
  nameAr: string;
  nameEn: string;
  nameFr: string;
  fontIcon: string;
  description: string;
  sortRanking: number;
  value?: any;
}
