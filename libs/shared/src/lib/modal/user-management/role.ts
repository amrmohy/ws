export class Role {
  id: number;
  nameAr: string;
  nameEn: string;
  nameFr: string;
  description: string;
}
