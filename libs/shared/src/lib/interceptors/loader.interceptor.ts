import {Injectable, Injector} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import {LoaderService} from '../components/loader/loader.service';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    const started = Date.now();
    const isContinue = req.url.indexOf('continueLoader=true') > -1 ? true : false;
    const loaderService = this.injector.get(LoaderService);

if (!req.url.includes('refresh-access-token')){
    loaderService.show();
}
    return next.handle(req).pipe(
      catchError((error :HttpErrorResponse) => {
        loaderService.hide();
        return throwError(error);
      }),
      finalize(() => {
        if (isContinue) {
          return;
        }
        const elapsed = Date.now() - started;
        /* check time taken by request to delay in case of short time to show spinner  */
        if (elapsed < 200) {
            setTimeout(() => {
                loaderService.hide();
            }, 300);
        } else {
            loaderService.hide();
        }
        })
    );
  }


}
