import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmMessageComponent} from '../components/confirm-message/confirm-message.component';

@Injectable({
    providedIn: 'root'
})
export class ConfirmMessageService {

    constructor(public dialog: MatDialog,private translate:TranslateService
    ) { }

    public  showCanDeactivateConfirmation(): Promise<boolean | any> {
     return  new Promise((resolve, reject) => {
        const modal =  this.dialog.open(ConfirmMessageComponent, {
            direction: this.translate.currentLang === 'ar' ? 'rtl' : 'ltr',
            panelClass: ['animate__animated', 'animate__slideInDown'],
            width: '400px'
        })
         modal.afterClosed().subscribe(confirm=>{
            resolve(confirm)
           return confirm
        })
    })
}

}
