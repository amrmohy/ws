import { Injectable } from '@angular/core';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  IsEdit: any;
  EditArticle: any;
  constructor(private  _base : BaseService) {

  }
  getArticles(){
    return  this._base.get('api/v1/article')
  }

  getArticleDetails(articleId) {
    return  this._base.get(`api/v1/article/${articleId}`)
  }

  deleteArticle(articleId) {
   return  this._base.deleteById(`api/v1/article?id=${articleId}`)
  }

  createArticle(article, hasUpload: boolean): Observable<any>{
    let serviceUrl = String(`api/v1/article`).concat(hasUpload ? '?continueLoader=true' : '');
   return  this._base.post(article,serviceUrl)
  }
  updateArticle(article, hasUpload: boolean): Observable<any>{
    let serviceUrl = String(`api/v1/article`).concat(hasUpload ? '?continueLoader=true' : '');
    return  this._base.put(article,serviceUrl)
  }

  callGoogleTranslateApi(text, source, target) :any{
    return this._base.callGoogleTranslateApi(text, source,target);
  }


  getIsEdit() {
    return this.IsEdit;
  }
  setIsEdit(IsEdit: boolean) {
    this.IsEdit = IsEdit;
  }
  getArticle() {
    return this.EditArticle;
  }
  setArticle(article) {
    this.EditArticle = article;
  }
}
