import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({providedIn:'root'})
export class QueryParamsHelperService {

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  public changeUrlParams(params: any, replaceUrl = false) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: QueryParamsHelperService.makeUrlParamsOf(params),
      replaceUrl
    });
  }

  public static makeUrlParamsOf(params: { [key: string]: string | number | boolean | any }): { [key: string]: string } {
    const keys = Object.keys(params);
    let newParams: { [key: string]: string } = {};
    for (let key of keys) {
      if (params[key] || (typeof params[key] === 'number' && params[key] === 0)) {
        if (typeof params[key] === 'object') {
          if (params[key] instanceof Array && params[key].length === 0) {
            continue;
          }
          newParams[key] = JSON.stringify(params[key]);
        } else {
          newParams[key] = params[key].toString();
        }
      }
    }
    return newParams;
  }
}
