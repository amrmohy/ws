import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import { BaseService } from './base.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpdateBranchService {
  // branchConfig = JSON.parse(localStorage.getItem('BRANCH_CONFIG'));
  public subject = new BehaviorSubject<any>([]);

  constructor(private _base: BaseService) {
  }


  public setBranch(newBranch) {
    return this.subject.next(newBranch);
  }

  getConfigBranches(): Observable<any> {
    return this._base.get(`api/v1/customer-branch/config`);
  }
}
